﻿using Jfdi.ProjectManagement.Api.Models;

namespace Jfdi.ProjectManagement.Api.Services.Notifications
{
    public interface INotificationsService
    {
        void AddNotifications(TeamMember teamMember);
    }
}
