﻿using System;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Notifications;

namespace Jfdi.ProjectManagement.Api.Services.Notifications
{
    public class NotificationsService : INotificationsService
    {
        private readonly JfdiDbContext dbContext;

        public NotificationsService(JfdiDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void AddNotifications(TeamMember teamMember)
        {
            foreach (var notificationType in Enum.GetValues<NotificationType>())
            {
                var notificationOption = new NotificationOption(Guid.NewGuid())
                {
                    IsOn = true,
                    NotificationType = notificationType,
                    TeamMember = teamMember
                };
                dbContext.NotificationOptions.Add(notificationOption);
            }
        }
    }
}
