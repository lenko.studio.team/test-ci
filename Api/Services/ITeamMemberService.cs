﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Models;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface ITeamMemberService
    {
        Task<IEnumerable<TeamMember>> GetUsers(IEnumerable<Guid> userIds);

        Task<IEnumerable<TeamMember>> GetSupervisors(IEnumerable<Guid> supervisorIds);
    }
}
