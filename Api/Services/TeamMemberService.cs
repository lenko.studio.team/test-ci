﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Services
{
    public class TeamMemberService : ITeamMemberService
    {
        private readonly JfdiDbContext dbContext;

        public TeamMemberService(JfdiDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IEnumerable<TeamMember>> GetSupervisors(IEnumerable<Guid> supervisorIds)
        {
            if (supervisorIds == null)
            {
                return new List<TeamMember>();
            }

            var users = await dbContext.TeamMembers
                .Where(a => supervisorIds.Contains(a.Id))
                .ToListAsync();

            var hasNotContainingSupervisors = supervisorIds.Except(
                users.Select(s => s.Id)).Any();

            if (hasNotContainingSupervisors)
            {
                throw new EntityNotFoundException("Supervisors");
            }

            return users;
        }

        public async Task<IEnumerable<TeamMember>> GetUsers(IEnumerable<Guid> userIds)
        {
            if (userIds == null)
            {
                return new List<TeamMember>();
            }

            var users = await dbContext.TeamMembers
                .Where(a => userIds.Contains(a.Id))
                .ToListAsync();

            var hasNotContainingUsers = userIds.Except(
                users.Select(s => s.Id)).Any();

            if (hasNotContainingUsers)
            {
                throw new EntityNotFoundException(typeof(TeamMember));
            }

            return users;
        }
    }
}
