﻿using System;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Models;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface IBlockerService
    {
        Task DeleteBlocker(TaskEvent task, Guid blockerId);

        Task<Blocker> AddBlocker(TaskEvent task, Blocker newBlocker);
    }
}
