﻿using System;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Models;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services.Blockers
{
    public class BlockerService : IBlockerService
    {
        private readonly JfdiDbContext dbContext;

        private readonly ITaskStateService taskStateService;

        public BlockerService(
            JfdiDbContext dbContext,
            ITaskStateService taskStateService)
        {
            this.dbContext = dbContext;
            this.taskStateService = taskStateService;
        }

        public async Task<Blocker> AddBlocker(TaskEvent task, Blocker newBlocker)
        {
            dbContext.Blockers.Add(newBlocker);
            await dbContext.SaveChangesAsync();
            await taskStateService.BlockTask(task);
            return newBlocker;
        }

        public async Task DeleteBlocker(TaskEvent task, Guid blockerId)
        {
            var blocker = await dbContext.Blockers
                .FirstOrFail(b => b.Id == blockerId && b.Task.Id == task.Id);

            dbContext.Blockers.Remove(blocker);
            await dbContext.SaveChangesAsync();
            await taskStateService.BlockTask(task);
        }
    }
}
