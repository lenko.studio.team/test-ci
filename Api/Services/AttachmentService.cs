﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Attachments;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Services
{
    public class AttachmentService : IAttachmentService
    {
        private readonly JfdiDbContext dbContext;

        public AttachmentService(
               JfdiDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Attachment Clone(Attachment oldAttachment)
        {
            Attachment attachment;
            switch (oldAttachment)
            {
                case DirectAttachment directAttachment:
                    attachment = new DirectAttachment(
                        Guid.NewGuid(),
                        DateTime.UtcNow,
                        directAttachment.CreatedById,
                        directAttachment.Data)
                    {
                        Name = directAttachment.Name
                    };
                    break;

                case GDriveAttachment gdriveAttachment:
                    attachment = new GDriveAttachment(
                        Guid.NewGuid(),
                        DateTime.UtcNow,
                        gdriveAttachment.CreatedById,
                        gdriveAttachment.Link)
                    {
                        Name = gdriveAttachment.Name
                    };
                    break;
                default:
                    throw new NotImplementedException($"Unknown type: {oldAttachment.GetType().Name}");
            }

            dbContext.Attachments.Add(attachment);
            return attachment;
        }

        public async Task<IEnumerable<Attachment>> FindAttachment(IEnumerable<Guid>? attachmentIds)
        {
            if (attachmentIds == null)
            {
                return new List<Attachment>();
            }

            var attachments = await dbContext.Attachments
                .Where(a => attachmentIds.Contains(a.Id))
                .ToListAsync();

            var hasNotContainingTypes = attachmentIds.Except(
                attachments.Select(s => s.Id)).Any();

            if (hasNotContainingTypes)
            {
                throw new EntityNotFoundException(typeof(Attachment));
            }

            return attachments;
        }
    }
}
