﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface IServiceInstanceService
    {
        Task<ServiceInstance> CreateServiceInstanceFrom(
            Guid serviceId, Client client);

        Task RemoveService(ServiceInstance service);

        IQueryable<Service> CreateOrderQuery(IQueryable<Service> query, OrderTypeDto orderType);
    }
}
