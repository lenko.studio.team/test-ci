﻿using System;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface ITaxClassificationService
    {
        static Guid CCorpId => Guid.Parse("a7db5845-babc-4230-ae13-ca4d865db6a6");

        static Guid SCorpId => Guid.Parse("ed17ca07-fb2e-4204-86b6-9de0d0e3e5da");

        static Guid ParnershipId => Guid.Parse("4681430b-752d-4520-8372-6f41e3918469");

        static Guid SingleMemberId => Guid.Parse("9e8940ec-4317-4a2b-a1dd-668827943a99");
    }
}
