﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface ITransactionService
    {
        Task InvokeInTransaction(RequestDelegate request, HttpContext httpContext);
    }
}
