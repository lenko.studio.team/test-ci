﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Services
{
    public class ServiceTypeService : IServiceTypeService
    {
        private readonly JfdiDbContext dbContext;

        public ServiceTypeService(JfdiDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IEnumerable<ServiceType>> FindServiceTypes(IEnumerable<Guid>? serviceTypeIds)
        {
            if (serviceTypeIds == null)
            {
                return new List<ServiceType>();
            }

            var serviceTypes = await dbContext.ServiceTypes
                .Where(a => serviceTypeIds.Contains(a.Id))
                .ToListAsync();

            var hasNotContainingTypes = serviceTypeIds.Except(
                serviceTypes.Select(s => s.Id)).Any();

            if (hasNotContainingTypes)
            {
                throw new EntityNotFoundException(typeof(ServiceType));
            }

            return serviceTypes;
        }

        public Task<ServiceType> GetServiceType(Guid serviceTypeId)
        {
            return dbContext.ServiceTypes.FindOrFailAsync(serviceTypeId);
        }
    }
}
