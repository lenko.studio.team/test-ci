﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Models;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface IServiceTypeService
    {
        static Guid BookkeepingId => Guid.Parse("a7db5845-babc-4230-ae13-ca4d865db6a6");

        static Guid MonthEndCloseId => Guid.Parse("ed17ca07-fb2e-4204-86b6-9de0d0e3e5da");

        static Guid AccountsPayablesId => Guid.Parse("4681430b-752d-4520-8372-6f41e3918469");

        static Guid AccountsReceivablesId => Guid.Parse("9e8940ec-4317-4a2b-a1dd-668827943a99");

        static Guid PayrollId => Guid.Parse("9e8940ec-4317-4a2b-a1dd-668827943a98");

        static Guid ReportingId => Guid.Parse("9e8940ec-4317-4a2b-a1dd-668827943a78");

        static Guid ComplianceId => Guid.Parse("9e8940ec-4317-4a2b-a1dd-668827943a10");

        static Guid SpecialProjectsId => Guid.Parse("9e8940ec-4317-4a2b-a1dd-668827943a43");

        Task<ServiceType> GetServiceType(Guid serviceTypeId);

        Task<IEnumerable<ServiceType>> FindServiceTypes(IEnumerable<Guid>? serviceTypeId);
    }
}
