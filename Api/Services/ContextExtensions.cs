﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace Jfdi.ProjectManagement.Api.Services
{
    public static class ContextExtensions
    {
        public static void AddTaxClassifications(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaxClassification>()
                .HasData(new TaxClassification(
                    ITaxClassificationService.CCorpId,
                    "C-Corp"));

            modelBuilder.Entity<TaxClassification>()
                .HasData(new TaxClassification(
                    ITaxClassificationService.ParnershipId,
                    "Partnership"));

            modelBuilder.Entity<TaxClassification>()
                .HasData(new TaxClassification(
                    ITaxClassificationService.SCorpId,
                    "S-Corp"));

            modelBuilder.Entity<TaxClassification>()
                .HasData(new TaxClassification(
                    ITaxClassificationService.SingleMemberId,
                    "Single member LLC"));
        }

        public static void AddServiceTypes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ServiceType>()
                .HasData(new ServiceType(
                    IServiceTypeService.BookkeepingId,
                    "Bookkeeping",
                    "bookkeeping",
                    "bookkeeping-fill",
                    "#EBBB38"));

            modelBuilder.Entity<ServiceType>()
                .HasData(new ServiceType(
                    IServiceTypeService.MonthEndCloseId,
                    "Month End Close",
                    "month-end-close",
                    "month-end-close-fill",
                    "#8F56CB"));

            modelBuilder.Entity<ServiceType>()
                .HasData(new ServiceType(
                    IServiceTypeService.AccountsPayablesId,
                    "Accounts Payables",
                    "ap",
                    "ap-fill",
                    "#65A9D9"));

            modelBuilder.Entity<ServiceType>()
                .HasData(new ServiceType(
                    IServiceTypeService.AccountsReceivablesId,
                    "Accounts Receivables",
                    "ar",
                    "ar-fill",
                    "#5DC5BE"));

            modelBuilder.Entity<ServiceType>()
                .HasData(new ServiceType(
                    IServiceTypeService.PayrollId,
                    "Payroll",
                    "payroll",
                    "payroll-fill",
                    "#DB669E"));

            modelBuilder.Entity<ServiceType>()
                .HasData(new ServiceType(
                    IServiceTypeService.ReportingId,
                    "Reporting",
                    "reporting",
                    "reporting-fill",
                    "#8BC465"));

            modelBuilder.Entity<ServiceType>()
                .HasData(new ServiceType(
                    IServiceTypeService.ComplianceId,
                    "Compliance",
                    "compliance",
                    "compliance-fill",
                    "#E26C35"));

            modelBuilder.Entity<ServiceType>()
                .HasData(new ServiceType(
                    IServiceTypeService.SpecialProjectsId,
                    "Special Projects",
                    "project",
                    "project-fill",
                    "#707070"));
        }

        public static void AddTaskStates(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskState>()
                .HasData(new TaskState(
                    "TO-DO",
                    ITaskStateService.TodoStateId));

            modelBuilder.Entity<TaskState>()
                .HasData(new TaskState(
                    "In progress",
                    ITaskStateService.InProgressStateId));

            modelBuilder.Entity<TaskState>()
                .HasData(new TaskState(
                    "Blocked",
                    ITaskStateService.BlockedStateId));

            modelBuilder.Entity<TaskState>()
                .HasData(new TaskState(
                    "For review",
                    ITaskStateService.ForReviewStateId));

            modelBuilder.Entity<TaskState>()
                .HasData(new TaskState(
                    "Done",
                    ITaskStateService.DoneStateId));
        }

        public static void AddSoftArchive(this ModelBuilder modelBuilder)
        {
            var softArchiveEntries = modelBuilder.Model
                .GetEntityTypes()
                .Where(x => typeof(ISoftArchive).IsAssignableFrom(x.ClrType));

            foreach (var mutableEntityType in softArchiveEntries)
            {
                var entityTypeBuilder = modelBuilder.Entity(mutableEntityType.ClrType);

                entityTypeBuilder.HasQueryFilter(
                    ConvertFilterExpression<ISoftArchive>(e => e.ArchiveInfo == null, mutableEntityType.ClrType));
            }
        }

        private static LambdaExpression ConvertFilterExpression<TInterface>(
            Expression<Func<TInterface, bool>> filterExpression,
            Type entityType)
        {
            var newParam = Expression.Parameter(entityType);
            var newBody = ReplacingExpressionVisitor.Replace(
                filterExpression.Parameters.Single(),
                newParam,
                filterExpression.Body);

            return Expression.Lambda(newBody, newParam);
        }
    }
}
