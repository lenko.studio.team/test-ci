﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Configuration;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Keycloak;
using Jfdi.ProjectManagement.Api.Dtos.Users;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Users;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services
{
    public class KeycloakUserService : IUserService
    {
        private readonly HttpClient httpClient;
        private readonly JfdiDbContext dbContext;
        private readonly KeycloakConfig keycloakConfig;

        public KeycloakUserService(
            HttpClient httpClient,
            IOptions<KeycloakConfig> keycloakOptions,
            JfdiDbContext dbContext)
        {
            this.httpClient = httpClient;
            keycloakConfig = keycloakOptions.Value;
            this.dbContext = dbContext;
        }

        public async Task<IEnumerable<UserDto>> GetUsers(UserFilterDto filter)
        {
            var token = await GetToken();
            var uri = $"auth/admin/realms/{keycloakConfig.Realm}/users";
            uri += FilterToString(filter);
            var request = new HttpRequestMessage(
                HttpMethod.Get,
                uri);
            request.Headers.Authorization = new AuthenticationHeaderValue(
                "Bearer", token);
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadAsStringAsync();
            var users = JsonSerializer.Deserialize<IEnumerable<UserDto>>(result);
            return users ?? new List<UserDto>();
        }

        public async Task<User> CreateUser(Guid userId, CreateTeamMemberDto newTeamMember)
        {
            var token = await GetToken();
            var uri = $"auth/admin/realms/{keycloakConfig.Realm}/users";
            var userName = $"{newTeamMember.FirstName} {newTeamMember.LastName}";
            var group = GetGroupName(newTeamMember.Permission);
            var payload = new CreateUserDto(
                userName,
                newTeamMember.Email)
            {
                FirstName = newTeamMember.FirstName,
                LastName = newTeamMember.LastName
            };
            var json = JsonSerializer.Serialize(payload);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(
                HttpMethod.Post,
                uri);
            request.Content = content;
            request.Headers.Authorization = new AuthenticationHeaderValue(
                "Bearer", token);
            var response = await httpClient.SendAsync(request);
            await CheckResponse(response);

            var location = response.Headers.Location;

            if (location == null
                || !Guid.TryParse(location.Segments.Last(), out var userKeycloakId))
            {
                throw new ClientErrorException("Can't get user id");
            }

            try
            {
                await AddUserToGroup(userKeycloakId, group);
                await SendVerificationEmail(userId, userKeycloakId);
                return new User(userKeycloakId, newTeamMember.Email, DateTime.UtcNow, newTeamMember.Permission);
            }
            catch (Exception)
            {
                await DeleteUser(userKeycloakId);
                throw;
            }
        }

        public async Task UpdateUserCredentials(Guid userId, AuthTeamMemberDto authTeamMemberDto)
        {
            var token = await GetToken();
            var uri = $"auth/admin/realms/{keycloakConfig.Realm}/users/{userId}";
            var payload = new UpdateUserDto(
                authTeamMemberDto.UserName,
                authTeamMemberDto.Password);
            var json = JsonSerializer.Serialize(payload);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(
                HttpMethod.Put,
                uri);
            request.Content = content;
            request.Headers.Authorization = new AuthenticationHeaderValue(
                "Bearer", token);
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteUser(Guid userKeycloakId)
        {
            var token = await GetToken();
            var uri = $"auth/admin/realms/{keycloakConfig.Realm}/users/{userKeycloakId}";
            var request = new HttpRequestMessage(
                HttpMethod.Delete,
                uri);
            request.Headers.Authorization = new AuthenticationHeaderValue(
                "Bearer", token);
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
        }

        public async Task<UserPermissions> GetPermission(Guid userId)
        {
            var token = await GetToken();
            var uri = $"auth/admin/realms/{keycloakConfig.Realm}/users/{userId}/groups";
            var request = new HttpRequestMessage(
                HttpMethod.Get,
                uri);
            request.Headers.Authorization = new AuthenticationHeaderValue(
                "Bearer", token);
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var groups = JsonSerializer.Deserialize<IEnumerable<UserGroupDto>>(content)
                ?? throw new NullReferenceException("Not find any groups");
            return TryGetPermission(groups);
        }

        public async Task ChangePassword(User user, ChangePasswordDto changePasswordDto)
        {
            var keycloakUser = await GetUser(user.Id);
            var userName = keycloakUser.UserName ?? throw new EntityNotFoundException(typeof(User));
            var _ = await GetToken(userName, changePasswordDto.OldPassword);
            var token = await GetToken();
            var payload = new
            {
                temporary = false,
                type = "password",
                value = changePasswordDto.NewPassword
            };
            var json = JsonSerializer.Serialize(payload);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var uri = $"auth/admin/realms/{keycloakConfig.Realm}/users/{user.Id}/reset-password";
            var request = new HttpRequestMessage(
                HttpMethod.Put,
                uri);
            request.Content = content;
            request.Headers.Authorization = new AuthenticationHeaderValue(
                "Bearer", token);
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
        }

        private async Task CheckResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                return;
            }

            var content = await response.Content.ReadAsStringAsync();
            var messageDto = JsonSerializer.Deserialize<ErrorMessageDto>(content);
            var message = messageDto?.ErrorMessage ?? "Problem with users";
            throw new ClientErrorException(message);
        }

        private UserPermissions TryGetPermission(IEnumerable<UserGroupDto> groups)
        {
            foreach (var group in groups)
            {
                if (string.IsNullOrEmpty(group.Name))
                {
                    continue;
                }

                if (UserPermission.TryParsePermission(group.Name, out var permission))
                {
                    return permission;
                }
            }

            throw new NotSupportedException("Not find expect group");
        }

        private async Task<string> GetToken()
        {
            var uri = $"auth/realms/{keycloakConfig.Realm}/protocol/openid-connect/token";
            var request = new HttpRequestMessage(
                   HttpMethod.Post,
                   uri);
            request.Content = new StringContent(
                $"client_secret={keycloakConfig.ClientSecret}&client_id={keycloakConfig.ClientId}&grant_type=client_credentials",
                Encoding.UTF8,
                "application/x-www-form-urlencoded");
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();
            var token = JObject.Parse(json);

            return (string)token["access_token"];
        }

        private async Task<string> GetToken(string userName, string password)
        {
            var uri = $"auth/realms/{keycloakConfig.Realm}/protocol/openid-connect/token";
            var request = new HttpRequestMessage(
                   HttpMethod.Post,
                   uri);
            var data = new List<KeyValuePair<string?, string?>>();
            data.Add(new KeyValuePair<string?, string?>("client_id", keycloakConfig.ClientId));
            data.Add(new KeyValuePair<string?, string?>("client_secret", keycloakConfig.ClientSecret));
            data.Add(new KeyValuePair<string?, string?>("username", userName));
            data.Add(new KeyValuePair<string?, string?>("password", password));
            data.Add(new KeyValuePair<string?, string?>("grant_type", "password"));
            request.Content = new FormUrlEncodedContent(data);
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();
            var token = JObject.Parse(json);

            return (string)token["access_token"];
        }

        private string FilterToString(UserFilterDto filter)
        {
            var builder = new StringBuilder();
            builder.Append("?");

            if (!string.IsNullOrEmpty(filter.UserName))
            {
                builder.Append($"username={filter.UserName}&");
            }

            builder.Append($"max={filter.Limit}&");
            builder.Append($"first={filter.Offset}");

            return builder.ToString();
        }

        private async Task SendVerificationEmail(Guid userId, Guid userKeycloakId)
        {
            var redirectLink = keycloakConfig.RedirectUrl!.Replace("USER_ID", userId.ToString());
            var lifespan = 48 * 60 * 60;
            var token = await GetToken();
            var uri = $"auth/admin/realms/{keycloakConfig.Realm}/users/{userKeycloakId}/execute-actions-email?redirect_uri={redirectLink}&client_id={keycloakConfig.ClientId}&lifespan={lifespan}";
            var payload = new[] { "VERIFY_EMAIL" };
            var json = JsonSerializer.Serialize(payload);

            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(
                   HttpMethod.Put,
                   uri);
            request.Content = content;
            request.Headers.Authorization = new AuthenticationHeaderValue(
                "Bearer", token);
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
        }

        private string GetGroupName(UserPermissions permissions)
        {
            switch (permissions)
            {
                case UserPermissions.Owner:
                    return keycloakConfig.OwnerGroup!;
                case UserPermissions.Admin:
                    return keycloakConfig.AdminGroup!;
                case UserPermissions.Member:
                    return keycloakConfig.MemberGroup!;
            }

            throw new NotSupportedException($"Unknown role : {permissions}");
        }

        private async Task AddUserToGroup(Guid userId, string groupName)
        {
            var groupId = await FindGroupIdByName(groupName);
            var token = await GetToken();
            var uri = $"auth/admin/realms/{keycloakConfig.Realm}/users/{userId}/groups/{groupId}";
            var request = new HttpRequestMessage(HttpMethod.Put, uri);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
        }

        private async Task<Guid> FindGroupIdByName(string groupName)
        {
            var token = await GetToken();
            var uri = $"auth/admin/realms/{keycloakConfig.Realm}/groups?search={groupName}";
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var groups = JsonSerializer.Deserialize<IEnumerable<UserGroupDto>>(content)
                ?? throw new NullReferenceException("Failed to get groups");
            return groups.First(group => group.Name == groupName).Id;
        }

        private async Task<UserDto> GetUser(Guid userId)
        {
            var token = await GetToken();
            var uri = $"auth/admin/realms/{keycloakConfig.Realm}/users/{userId}";
            var request = new HttpRequestMessage(
                HttpMethod.Get,
                uri);
            request.Headers.Authorization = new AuthenticationHeaderValue(
                "Bearer", token);
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<UserDto>(result)
                ?? throw new EntityNotFoundException(typeof(User));
        }
    }
}
