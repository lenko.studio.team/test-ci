﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Services
{
    public class SoftwareService : ISoftwareService
    {
        private readonly JfdiDbContext dbContext;
        private readonly IMapper mapper;
        private readonly ICurrentUserProvider currentUser;
        private readonly IAttachmentService attachmentService;

        public SoftwareService(
            JfdiDbContext dbContext,
            IMapper mapper,
            ICurrentUserProvider currentUser,
            IAttachmentService attachmentService)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.currentUser = currentUser;
            this.attachmentService = attachmentService;
        }

        public async Task<Software> CreateSoftware(CreateSoftwareDto createSoftwareDto)
        {
            var software = new Software(
                createSoftwareDto.Name,
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId);
            mapper.Map(createSoftwareDto, software);
            var attachments = await attachmentService.FindAttachment(createSoftwareDto.AttachmentIds);
            attachments.ToList().ForEach(software.Attachments.Add);
            dbContext.Softwares.Add(software);
            return software;
        }

        public async Task<IEnumerable<Software>> FindSoftwares(IEnumerable<Guid>? softwareIds)
        {
            if (softwareIds == null)
            {
                return new List<Software>();
            }

            var softwares = await dbContext.Softwares
                .Where(a => softwareIds.Contains(a.Id))
                .ToListAsync();

            var hasNotSoftwareTypes = softwareIds.Except(
                softwares.Select(s => s.Id)).Any();

            if (hasNotSoftwareTypes)
            {
                throw new EntityNotFoundException(typeof(Software));
            }

            return softwares;
        }
    }
}
