﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services
{
    public class ServiceInstanceService : IServiceInstanceService
    {
        private readonly JfdiDbContext dbContext;
        private readonly ICurrentUserProvider currentUser;
        private readonly ISoftwareService softwareService;
        private readonly IAttachmentService attachmentService;
        private readonly ITasksService tasksService;

        public ServiceInstanceService(
            JfdiDbContext dbContext,
            ICurrentUserProvider currentUser,
            ISoftwareService softwareService,
            IAttachmentService attachmentService,
            ITasksService tasksService)
        {
            this.dbContext = dbContext;
            this.currentUser = currentUser;
            this.softwareService = softwareService;
            this.attachmentService = attachmentService;
            this.tasksService = tasksService;
        }

        public IQueryable<Service> CreateOrderQuery(IQueryable<Service> query, OrderTypeDto orderType)
        {
            switch (orderType.Field)
            {
                case OrderFields.Name:
                    return orderType.Desc
                        ? query.OrderByDescending(s => s.Name.ToLower())
                        : query.OrderBy(s => s.Name);

                case OrderFields.MostClient:
                    return orderType.Desc
                        ? query.OrderByDescending(s => s.Instancies.Count())
                        : query.OrderBy(s => s.Instancies.Count());

                case OrderFields.Newly:
                    return orderType.Desc
                        ? query.OrderBy(s => s.CreatedAtUtc)
                        : query.OrderByDescending(s => s.CreatedAtUtc);
            }

            throw new NotImplementedException();
        }

        public async Task<ServiceInstance> CreateServiceInstanceFrom(Guid serviceId, Client client)
        {
            var service = await dbContext.Services
                .Include(s => s.Softwares)
                .Include(s => s.ServiceType)
                .Include(s => s.Tasks)
                .ThenInclude(t => t.Children)
                .Include(s => s.Attachments)
                .FirstOrFail(s => s.Id == serviceId);
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                service.Name,
                service.StartDateUtc,
                service.EndDateUtc)
            {
                RecurrenceRule = service.RecurrenceRule,
                Calendar = client.Calendar
            };

            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                service.Name)
            {
                Template = service,
                Description = service.Description,
                IsHighPriority = service.IsHighPriority,
                Links = service.Links,
                Event = calendarEvent,
                Client = client
            };

            foreach (var software in service.Softwares)
            {
                serviceInstance.Softwares.Add(software);
            }

            foreach (var task in service.Tasks)
            {
                var taskInstance = await tasksService.CreateInstance(task, serviceInstance);
                serviceInstance.Tasks.Add(taskInstance);
            }

            service.Attachments
                .Select(attachmentService.Clone)
                .ToList()
                .ForEach(serviceInstance.Attachments.Add);
            dbContext.ServiceInstances.Add(serviceInstance);
            return serviceInstance;
        }

        public async Task RemoveService(ServiceInstance service)
        {
            var tasks = await dbContext.TaskInstances
                .Include(ti => ti.Event)
                .Include(ti => ti.Children)
                .Include(ti => ti.TaskEvents)
                .Where(ti => ti.Service!.Id == service.Id)
                .ToListAsync();

            foreach (var task in tasks)
            {
                await tasksService.RemoveTask(task);
            }

            dbContext.RemoveRange(service.Attachments);
            dbContext.CalendarEvents.Remove(service.Event);
            dbContext.Remove(service);
        }
    }
}
