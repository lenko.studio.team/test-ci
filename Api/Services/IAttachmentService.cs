﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Models;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface IAttachmentService
    {
        Attachment Clone(Attachment oldAttachment);

        Task<IEnumerable<Attachment>> FindAttachment(IEnumerable<Guid>? attachmentIds);
    }
}
