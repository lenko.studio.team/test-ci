﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface ISoftwareService
    {
        Task<Software> CreateSoftware(CreateSoftwareDto createSoftwareDto);

        Task<IEnumerable<Software>> FindSoftwares(IEnumerable<Guid>? softwareIds);
    }
}
