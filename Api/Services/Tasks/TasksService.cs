﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Tasks;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Services.Tasks;
using Microsoft.EntityFrameworkCore;
using JFDITask = Jfdi.ProjectManagement.Api.Models.Task;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services
{
    public class TasksService : ITasksService
    {
        private readonly ICurrentUserProvider currentUser;
        private readonly JfdiDbContext dbContext;
        private readonly IMapper mapper;
        private readonly ITaskEventService taskEventService;

        public TasksService(
            ICurrentUserProvider currentUser,
            JfdiDbContext dbContext,
            IMapper mapper,
            ITaskEventService taskEventService)
        {
            this.currentUser = currentUser;
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.taskEventService = taskEventService;
        }

        public async Task<TaskInstance> CreateInstance(
            JFDITask task,
            ServiceInstance serviceInstance)
        {
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                task.Title,
                task.StartDateUtc,
                task.EndDateUtc)
            {
                RecurrenceRule = task.RecurrenceRule,
                Calendar = serviceInstance.Client.Calendar
            };

            var taskInstance = new TaskInstance(
                   Guid.NewGuid(),
                   DateTime.UtcNow,
                   currentUser.CurrentUserId,
                   task.Title)
            {
                Service = serviceInstance,
                Template = task,
                Event = calendarEvent,
                IsHighPriority = false,
                Info = task.Info
            };
            var taskEvent = await taskEventService.Create(taskInstance, task.StartDateUtc);
            if (taskEvent == null)
            {
                throw new ClientErrorException("Can't create task event");
            }

            taskInstance.TaskEvents.Add(taskEvent);
            foreach (var child in task.Children)
            {
                var childInstance = await CreateInstance(child, serviceInstance);
                taskInstance.Children.Add(childInstance);
            }

            dbContext.CalendarEvents.Add(calendarEvent);
            dbContext.TaskInstances.Add(taskInstance);
            return taskInstance;
        }

        public JFDITask CreateSubTask(CreateSubTaskDto createSubTaskDto, JFDITask parentTask)
        {
            var subTask = new JFDITask(
                   createSubTaskDto.Title,
                   Guid.NewGuid(),
                   DateTime.UtcNow,
                   currentUser.CurrentUserId);

            subTask.RecurrenceRule = createSubTaskDto.RecurrenceRule;
            subTask.StartDateUtc = createSubTaskDto.StartUtc.Date;
            subTask.EndDateUtc = subTask.StartDateUtc.Date.AddDays(1).AddTicks(-1);
            subTask.Parent = parentTask;
            parentTask.Children.Add(subTask);

            dbContext.Tasks.Add(subTask);
            return subTask;
        }

        public JFDITask CreateTask(CreateTaskDto createTaskDto)
        {
            var task = new JFDITask(
                createTaskDto.Title,
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId);
            mapper.Map(createTaskDto, task);
            task.StartDateUtc = createTaskDto.StartUtc.Date;
            task.EndDateUtc = task.StartDateUtc.Date.AddDays(1).AddTicks(-1);
            createTaskDto.Children.ToList().ForEach(st => CreateSubTask(st, task));
            dbContext.Tasks.Add(task);
            return task;
        }

        public async Task RemoveTaskFromService(JFDITask task, Service service)
        {
            service.Tasks.Remove(task);
            var services = await dbContext.ServiceInstances
                .Include(ti => ti.Tasks)
                .ThenInclude(ti => ti.Template)
                .Include(ti => ti.Tasks)
                .ThenInclude(ti => ti.Event)
                .Include(ti => ti.Tasks)
                .ThenInclude(ti => ti.Children)
                .Include(ti => ti.Tasks)
                .ThenInclude(ti => ti.TaskEvents)
                .Where(si => si.Template.Id == service.Id && si.Tasks.Any(t => t.Template!.Id == task.Id))
                .ToListAsync();

            foreach (var serviceInstance in services)
            {
                var taskInstance = serviceInstance.Tasks.FirstOrFail(ti => ti.Template!.Id == task.Id);
                serviceInstance.Tasks.Remove(taskInstance);
                await RemoveTask(taskInstance);
            }
        }

        public async Task RemoveTask(TaskInstance task)
        {
            await dbContext.Entry(task)
                .Reference(t => t.Event)
                .LoadAsync();
            if (task.Event != default)
            {
                dbContext.CalendarEvents.Remove(task.Event);
            }

            await dbContext.Entry(task)
                .Collection(t => t.TaskEvents)
                .LoadAsync();
            foreach (var taskEvent in task.TaskEvents)
            {
                await taskEventService.RemoveEvent(taskEvent);
            }

            await dbContext.Entry(task)
                .Collection(t => t.Children)
                .LoadAsync();
            foreach (var subTask in task.Children)
            {
                await RemoveTask(subTask);
            }

            dbContext.TaskInstances.Remove(task);
        }

        public IQueryable<TaskEvent> CreateOrderQuery(IQueryable<TaskEvent> query, OrderTypeDto orderType)
        {
            switch (orderType.Field)
            {
                case OrderFields.Name:
                    return orderType.Desc
                        ? query.OrderByDescending(s => s.Instance.Name.ToLower())
                        : query.OrderBy(s => s.Instance.Name.ToLower());

                case OrderFields.StartDate:
                    return orderType.Desc
                        ? query.OrderByDescending(s => s.StartDateUtc)
                        : query.OrderBy(s => s.StartDateUtc);

                case OrderFields.EndDate:
                    return orderType.Desc
                        ? query.OrderByDescending(s => s.EndDateUtc)
                        : query.OrderBy(s => s.EndDateUtc);

                case OrderFields.Newly:
                    return orderType.Desc
                        ? query.OrderBy(s => s.Instance.CreatedAtUtc)
                        : query.OrderByDescending(s => s.Instance.CreatedAtUtc);
            }

            throw new NotImplementedException();
        }
    }
}
