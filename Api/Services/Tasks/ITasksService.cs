﻿using System.Linq;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Tasks;
using Jfdi.ProjectManagement.Api.Models;
using JFDITask = Jfdi.ProjectManagement.Api.Models.Task;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface ITasksService
    {
        JFDITask CreateTask(CreateTaskDto createTaskDto);

        JFDITask CreateSubTask(CreateSubTaskDto createSubTaskDto, JFDITask parentTask);

        Task<TaskInstance> CreateInstance(
                    JFDITask task,
                    ServiceInstance serviceInstance);

        Task RemoveTaskFromService(JFDITask task, Service service);

        Task RemoveTask(TaskInstance task);

        IQueryable<TaskEvent> CreateOrderQuery(IQueryable<TaskEvent> query, OrderTypeDto orderType);
    }
}
