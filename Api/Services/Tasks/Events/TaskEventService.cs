﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ical.Net.DataTypes;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services.Tasks.Events
{
    public class TaskEventService : ITaskEventService
    {
        private readonly JfdiDbContext dbContext;
        private readonly IBlockerService blockerService;
        private readonly ITaskStateService taskStateService;
        private readonly IMapper mapper;

        public TaskEventService(
            JfdiDbContext dbContext,
            IBlockerService blockerService,
            ITaskStateService taskStateService,
            IMapper mapper)
        {
            this.dbContext = dbContext;
            this.blockerService = blockerService;
            this.taskStateService = taskStateService;
            this.mapper = mapper;
        }

        public async Task<TaskEvent?> Create(TaskInstance instance, DateTime currentDate)
        {
            var occurence = GetOccurrence(instance.Event, currentDate);
            if (occurence == null)
            {
                return null;
            }

            var taskEvent = new TaskEvent(
                Guid.NewGuid());
            taskEvent.Instance = instance;
            taskEvent.State = await taskStateService.GetInitialState();
            taskEvent.StartDateUtc = occurence.Period.StartTime.AsUtc;
            taskEvent.EndDateUtc = occurence.Period.EndTime.AsUtc;
            dbContext.TaskEvents.Add(taskEvent);
            return taskEvent;
        }

        public async Task RemoveEvent(TaskEvent taskEvent)
        {
            var allTaskData = await dbContext.TaskEvents
                .Include(te => te.Blockers)
                .Include(te => te.Children)
                .Include(te => te.Comments)
                .FirstOrFail(te => te.Id == taskEvent.Id);

            foreach (var blocker in allTaskData.Blockers)
            {
                await blockerService.DeleteBlocker(allTaskData, blocker.Id);
            }

            allTaskData.Comments.ToList().ForEach(c => dbContext.Comments.Remove(c));

            foreach (var subTask in allTaskData.Children)
            {
                await RemoveEvent(subTask);
            }

            dbContext.TaskEvents.Remove(allTaskData);
        }

        public async Task<IEnumerable<TaskEventDto>> CalculateRecurringEvents(IEnumerable<TaskEvent> events, DateTime startDate, DateTime endDate)
        {
            bool EventExists(Occurrence occurrence, TaskInstance taskInstance)
            {
                return events
                    .Any(x => x.Instance == taskInstance &&
                        x.StartDateUtc == occurrence.Period.StartTime.AsUtc &&
                        x.EndDateUtc == occurrence.Period.EndTime.AsUtc);
            }

            var state = await taskStateService.GetInitialState();

            var dynamicTaskEvents = events
                .Select(evt => evt.Instance)
                .Distinct()
                .Where(ti => !string.IsNullOrEmpty(ti.Event.RecurrenceRule))
                .SelectMany(ti => GetOccurrences(ti.Event, startDate, endDate)
                    .Where(occ => occ.Period.EndTime.AsUtc < endDate && !EventExists(occ, ti))
                    .Select(occ => mapper.Map<TaskEventDto>(
                        new TaskEvent(Guid.NewGuid())
                    {
                        StartDateUtc = occ.Period.StartTime.AsUtc,
                        EndDateUtc = occ.Period.EndTime.AsUtc,
                        Instance = ti,
                        State = state,
                    }, opts => opts.AfterMap((src, dst) => dst.IsStored = false))))
                .ToList();

            return events
                .Where(e => e.EndDateUtc >= startDate && e.EndDateUtc <= endDate)
                .Select(mapper.Map<TaskEventDto>)
                .Concat(dynamicTaskEvents)
                .OrderBy(e => e.StartDateUtc);
        }

        private Occurrence? GetOccurrence(CalendarEvent calendarEvent, DateTime startDate)
        {
            return GetOccurrences(calendarEvent, startDate, DateTime.UtcNow.AddYears(1)).FirstOrDefault();
        }

        private IEnumerable<Occurrence> GetOccurrences(CalendarEvent calendarEvent, DateTime startDate, DateTime endDate)
        {
            var vEvent = new Ical.Net.CalendarComponents.CalendarEvent
            {
                Start = new CalDateTime(calendarEvent.StartUtc),
                End = new CalDateTime(calendarEvent.EndUtc),
            };

            if (!string.IsNullOrEmpty(calendarEvent.RecurrenceRule))
            {
                var rrule = new RecurrencePattern(calendarEvent.RecurrenceRule);
                vEvent.RecurrenceRules = new List<RecurrencePattern>
                {
                    rrule,
                };
            }

            var calendar = new Ical.Net.Calendar();
            calendar.Events.Add(vEvent);

            return calendar.GetOccurrences(startDate, endDate);
        }
    }
}
