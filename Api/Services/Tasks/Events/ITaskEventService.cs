﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services.Tasks
{
    public interface ITaskEventService
    {
        Task<TaskEvent?> Create(TaskInstance instance, DateTime currentDate);

        Task<IEnumerable<TaskEventDto>> CalculateRecurringEvents(IEnumerable<TaskEvent> events, DateTime startDate, DateTime endDate);

        Task RemoveEvent(TaskEvent taskEvent);
    }
}
