﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos.Mediator;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services
{
    public class TaskStateService : ITaskStateService
    {
        private readonly JfdiDbContext dbContext;
        private readonly IBus bus;

        public TaskStateService(
            JfdiDbContext dbContext,
            IBus bus)
        {
            this.dbContext = dbContext;
            this.bus = bus;
        }

        public async Task BlockTask(TaskEvent task)
        {
            var blockers = await dbContext.Blockers
                .Include(b => b.Task)
                .Where(ti => ti.Task.Id == task.Id)
                .ToListAsync();
            task.State = await GetStateWhenBlocked(blockers, task);

            await bus.Publish<TaskStateNotification>(new
            {
                TaskId = task.Id,
                NewStateId = task.State.Id
            });
            await dbContext.SaveChangesAsync();
        }

        public async Task UpdateState(TaskEvent task, Guid taskStateId)
        {
            Assert.NotEqual(ITaskStateService.BlockedStateId, taskStateId);
            Assert.NotEqual(ITaskStateService.BlockedStateId, task.State.Id);

            var state = await dbContext.TaskStates.FindOrFailAsync(taskStateId);
            if (taskStateId == ITaskStateService.ForReviewStateId && !task.Instance.ForReview)
            {
                throw new ClientErrorException("Task should be for review");
            }

            if (task.State.Id == ITaskStateService.DoneStateId && taskStateId != ITaskStateService.DoneStateId)
            {
                throw new ClientErrorException("If task is done, we can change it");
            }

            task.State = state;

            await bus.Publish<TaskStateNotification>(new
            {
                TaskId = task.Id,
                NewStateId = taskStateId
            });
        }

        public Task<TaskState> GetInitialState()
        {
            return dbContext.TaskStates.FindOrFailAsync(ITaskStateService.TodoStateId);
        }

        private Task<TaskState> GetStateWhenBlocked(List<Blocker> blockers, TaskEvent task)
        {
            if (blockers.Count != 0)
            {
                return dbContext.TaskStates.FindOrFailAsync(ITaskStateService.BlockedStateId);
            }

            if (task.State.Id == ITaskStateService.BlockedStateId)
            {
                return dbContext.TaskStates.FindOrFailAsync(ITaskStateService.TodoStateId);
            }

            return Task.FromResult(task.State);
        }
    }
}
