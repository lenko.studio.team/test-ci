﻿using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos.Mediator;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Jfdi.ProjectManagement.Api.Services.Tasks.TaskStates
{
    public class TaskStateNotificationConsumer : IConsumer<TaskStateNotification>
    {
        private readonly ILogger logger;

        public TaskStateNotificationConsumer(ILogger<TaskStateNotificationConsumer> logger)
        {
            this.logger = logger;
        }

        public Task Consume(ConsumeContext<TaskStateNotification> context)
        {
            logger.LogDebug($"Receive notification: {context.Message}");
            return Task.CompletedTask;
        }
    }
}
