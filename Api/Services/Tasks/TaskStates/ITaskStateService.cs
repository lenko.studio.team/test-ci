﻿using System;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Models;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface ITaskStateService
    {
        static Guid TodoStateId => Guid.Parse("a7db5845-babc-4230-ae13-ca4d865db6a6");

        static Guid InProgressStateId => Guid.Parse("b62cb356-de5a-444b-85d4-4c5ca05793b6");

        static Guid BlockedStateId => Guid.Parse("5994906d-4759-4a07-b6eb-62ba7348ea5e");

        static Guid ForReviewStateId => Guid.Parse("0366dac4-b435-475d-ba85-96ba8a112981");

        static Guid DoneStateId => Guid.Parse("f6550273-0245-4180-b5e6-425c88d892bd");

        Task UpdateState(TaskEvent task, Guid taskStateId);

        Task BlockTask(TaskEvent task);

        Task<TaskState> GetInitialState();
    }
}
