﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Jfdi.ProjectManagement.Api.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly JfdiDbContext dbContext;

        public TransactionService(JfdiDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task InvokeInTransaction(RequestDelegate request, HttpContext httpContext)
        {
            if (dbContext.Database.CurrentTransaction == null)
            {
                using var transaction = await dbContext.Database.BeginTransactionAsync();

                try
                {
                    await request(httpContext);
                    await transaction.CommitAsync();
                    return;
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }

            await request.Invoke(httpContext);
        }
    }
}
