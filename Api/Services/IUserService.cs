﻿using System;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Users;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Users;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Services
{
    public interface IUserService
    {
        Task<User> CreateUser(Guid userId, CreateTeamMemberDto newTeamMember);

        Task UpdateUserCredentials(Guid userId, AuthTeamMemberDto authTeamMemberDto);

        Task DeleteUser(Guid userKeycloakId);

        Task<UserPermissions> GetPermission(Guid id);

        Task ChangePassword(User user, ChangePasswordDto changePasswordDto);
    }
}
