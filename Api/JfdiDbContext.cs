﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text.Json;
using Jfdi.ProjectManagement.Api.Configuration;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Attachments;
using Jfdi.ProjectManagement.Api.Models.Notifications;
using Jfdi.ProjectManagement.Api.Models.Users;
using Jfdi.ProjectManagement.Api.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Npgsql;

namespace Jfdi.ProjectManagement.Api
{
    public class JfdiDbContext : DbContext
    {
        private readonly ILoggerFactory loggerFactory;

        public JfdiDbContext(DbContextOptions<JfdiDbContext> options, ILoggerFactory loggerFactory)
            : base(options)
        {
            this.loggerFactory = loggerFactory;
        }

        public DbSet<User> Users => Set<User>();

        public DbSet<Client> Clients => Set<Client>();

        public DbSet<Calendar> Calendars => Set<Calendar>();

        public DbSet<Task> Tasks => Set<Task>();

        public DbSet<Service> Services => Set<Service>();

        public DbSet<CalendarEvent> CalendarEvents => Set<CalendarEvent>();

        public DbSet<ServiceInstance> ServiceInstances => Set<ServiceInstance>();

        public DbSet<TaskInstance> TaskInstances => Set<TaskInstance>();

        public DbSet<TaskEvent> TaskEvents => Set<TaskEvent>();

        public DbSet<Blocker> Blockers => Set<Blocker>();

        public DbSet<TaskState> TaskStates => Set<TaskState>();

        public DbSet<TeamMember> TeamMembers => Set<TeamMember>();

        public DbSet<TaxClassification> TaxClassifications => Set<TaxClassification>();

        public DbSet<ServiceType> ServiceTypes => Set<ServiceType>();

        public DbSet<Software> Softwares => Set<Software>();

        public DbSet<Attachment> Attachments => Set<Attachment>();

        public DbSet<Comment> Comments => Set<Comment>();

        public DbSet<NotificationOption> NotificationOptions => Set<NotificationOption>();

        public static string ConnectionStringFromConfig(DatabaseConfig dbConfig)
        {
            var builder = new NpgsqlConnectionStringBuilder()
            {
                Host = dbConfig.Host,
                Port = dbConfig.Port ?? 0,
                Database = dbConfig.InitialCatalog,
                Username = dbConfig.User,
                Password = dbConfig.Password,
                ConnectionIdleLifetime = 60,
            };

            return builder.ConnectionString;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyUtcDateTimeConverter();
            modelBuilder
                .Entity<Client>()
                .UseXminAsConcurrencyToken()
                .Property(c => c.ClientJfdiEmails)
                .HasConversion(
                    p => JsonSerializer.Serialize(p, default),
                    p => JsonSerializer.Deserialize<List<string>>(p, default));

            modelBuilder
                .Entity<Client>()
                .Property(c => c.Software)
                .HasConversion(
                    p => JsonSerializer.Serialize(p, default),
                    p => JsonSerializer.Deserialize<List<string>>(p, default));

            modelBuilder
                .Entity<Software>()
                .Property(c => c.Links)
                .HasConversion(
                    p => JsonSerializer.Serialize(p, default),
                    p => JsonSerializer.Deserialize<List<string>>(p, default));

            modelBuilder
                .Entity<Calendar>()
                .UseXminAsConcurrencyToken();

            modelBuilder
                .Entity<CalendarEvent>()
                .UseXminAsConcurrencyToken();

            modelBuilder
                .Entity<Task>()
                .UseXminAsConcurrencyToken()
                .HasMany(tm => tm.Children)
                .WithOne(t => t.Parent!);

            modelBuilder
                .Entity<Service>()
                .UseXminAsConcurrencyToken()
                .Property(s => s.Links)
                .HasConversion(
                    p => JsonSerializer.Serialize(p, default),
                    p => JsonSerializer.Deserialize<List<string>>(p, default));

            modelBuilder
                .Entity<ServiceInstance>()
                .UseXminAsConcurrencyToken()
                .Property(c => c.Links)
                .HasConversion(
                    p => JsonSerializer.Serialize(p, default),
                    p => JsonSerializer.Deserialize<List<string>>(p, default));

            modelBuilder
                .Entity<TeamMember>()
                .UseXminAsConcurrencyToken()
                .Property(c => c.Role)
                .HasConversion(
                    p => p.ToString(),
                    p => Enum.Parse<TeamMemberRole>(p));

            modelBuilder
                .Entity<Attachment>()
                .HasDiscriminator(a => a.Type)
                .HasValue<Attachment>(AttachmentType.None)
                .HasValue<DirectAttachment>(AttachmentType.Direct)
                .HasValue<GDriveAttachment>(AttachmentType.GoogleDrive);

            modelBuilder
                .Entity<Attachment>()
                .Property(c => c.Type)
                .HasConversion(
                    p => p.ToString(),
                    p => Enum.Parse<AttachmentType>(p));

            modelBuilder
                .Entity<TaskInstance>()
                .Property(c => c.Links)
                .HasConversion(
                    p => JsonSerializer.Serialize(p, default),
                    p => JsonSerializer.Deserialize<List<string>>(p, default));

            modelBuilder.Entity<TaskInstance>()
                .HasMany(ti => ti.Assignees)
                .WithMany(tm => tm.AssignesTasks)
                .UsingEntity(titm => titm.ToTable("AssigneeTasks"));

            modelBuilder.Entity<TaskInstance>()
                .HasMany(ti => ti.Supervisors)
                .WithMany(tm => tm.SupervisorTasks)
                .UsingEntity(titm => titm.ToTable("SupervisorTasks"));

            modelBuilder
                .Entity<User>()
                .UseXminAsConcurrencyToken()
                .Property(c => c.Permission)
                .HasConversion(
                    p => p.ToString(),
                    p => Enum.Parse<UserPermissions>(p));
            modelBuilder
                .Entity<NotificationOption>()
                .UseXminAsConcurrencyToken()
                .Property(c => c.NotificationType)
                .HasConversion(
                    p => p.ToString(),
                    p => Enum.Parse<NotificationType>(p));

            modelBuilder
                .Entity<TeamMember>()
                .HasOne(tm => tm.User)
                .WithOne(u => u.TeamMember!)
                .HasForeignKey<TeamMember>(x => x.UserId);

            modelBuilder
                .Entity<User>()
                .HasOne(c => c.TeamMember)
                .WithOne(tm => tm!.User)
                .IsRequired(false);

            modelBuilder.AddSoftArchive();
            modelBuilder.AddTaxClassifications();
            modelBuilder.AddServiceTypes();
            modelBuilder.AddTaskStates();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(loggerFactory);
        }
    }

    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:File may only contain a single type", Justification = "Design time")]
    public class JfdiDbContextFactory : IDesignTimeDbContextFactory<JfdiDbContext>
    {
        public JfdiDbContextFactory()
        {
            // Required by dotnet-ef, do not remove
        }

        public JfdiDbContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(Directory.GetCurrentDirectory() + "/appsettings.json")
                .AddJsonFile(Directory.GetCurrentDirectory() + "/appsettings.Development.json", optional: true)
                .AddJsonFile(Directory.GetCurrentDirectory() + "/appsettings.Production.json", optional: true)
                .Build();

            var dbConfig = new DatabaseConfig();
            configuration.Bind("Database", dbConfig);
            var connectionString = JfdiDbContext.ConnectionStringFromConfig(dbConfig);

            var optionsBuilder = new DbContextOptionsBuilder<JfdiDbContext>();
            optionsBuilder.UseNpgsql(connectionString);

            return new JfdiDbContext(optionsBuilder.Options, new LoggerFactory());
        }
    }
}
