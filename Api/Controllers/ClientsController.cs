﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Attributes;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Clients;
using Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Jfdi.ProjectManagement.Api.Services;
using Jfdi.ProjectManagement.Api.Services.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ClientsController : BaseController
    {
        private readonly JfdiDbContext database;
        private readonly IMapper mapper;
        private readonly ICurrentUserProvider currentUser;
        private readonly ITasksService taskService;
        private readonly ITaskStateService taskStateService;
        private readonly IServiceInstanceService serviceService;
        private readonly IServiceTypeService serviceTypeService;
        private readonly ITeamMemberService teamMemberService;
        private readonly ITaskEventService taskEventService;

        public ClientsController(
            JfdiDbContext database,
            IMapper mapper,
            ICurrentUserProvider currentUser,
            ITasksService taskService,
            ITaskStateService taskStateService,
            IServiceInstanceService serviceService,
            IServiceTypeService serviceTypeService,
            ITeamMemberService teamMemberService,
            ITaskEventService taskEventService)
        {
            this.database = database;
            this.mapper = mapper;
            this.currentUser = currentUser;
            this.taskService = taskService;
            this.taskStateService = taskStateService;
            this.serviceService = serviceService;
            this.serviceTypeService = serviceTypeService;
            this.teamMemberService = teamMemberService;
            this.taskEventService = taskEventService;
        }

        [HttpGet]
        public async Task<PaginationDto<ClientDto>> GetClients([FromQuery] ClientsFilterDto filterDto)
        {
            var query = database.Clients
                .Include(c => c.Calendar)
                .Include(c => c.Services)
                .ThenInclude(c => c.Template)
                .ThenInclude(c => c.ServiceType)
                .Include(c => c.TeamMemberAssignments)
                .AsNoTracking();

            if (filterDto.Title != null)
            {
                query = query
                    .Where(s => EF.Functions.Like(s.CompanyName.ToLower(), $"%{filterDto.Title.ToLower()}%")
                        || EF.Functions.Like((s.Tresurer ?? string.Empty).ToLower(), $"%{filterDto.Title.ToLower()}%"));
            }

            if (filterDto.WithoutAssignedMembers.HasValue)
            {
                query = query.Where(c => (c.TeamMemberAssignments.Count == 0) == filterDto.WithoutAssignedMembers.Value);
            }

            if (filterDto.Order != null)
            {
                query = OrderQuery(query, filterDto.Order);
            }

            var totalCount = await query.CountAsync();
            query = query
                .Skip(filterDto.Offset)
                .Take(filterDto.Limit);
            var data = await mapper
                .ProjectTo<ClientDto>(query)
                .ToListAsync();

            return new PaginationDto<ClientDto>
            {
                Data = data,
                TotalCount = totalCount
            };
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ClientDto> GetClient(Guid id)
        {
            var client = database.Clients.AsQueryable();
            return await mapper.ProjectTo<ClientDto>(client).FirstOrFail(c => c.Id == id);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task PutClient(Guid id, UpdateClientViewModel model)
        {
            var client = await database.Clients
                .Include(c => c.TeamMemberAssignments)
                .Include(c => c.TaxClassification)
                .FirstOrFail(c => c.Id == id);

            mapper.Map(model, client);

            database.Entry(client).State = EntityState.Modified;

            client.TaxClassification = model.TaxClassificationId.HasValue
                ? await database.TaxClassifications.FindOrFailAsync(model.TaxClassificationId)
                : null;
            await database.SaveChangesAsync();
        }

        [HttpPost]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<ClientDto>> PostClient(CreateClientViewModel model)
        {
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                model.CompanyName)
            {
                CityRegistration = model.CityRegistration,
                ClientJfdiEmails = model.ClientJfdiEmails,
                ClientTag = model.ClientTag,
                CompanyEin = model.CompanyEin,
                DateFormed = model.DateFormed,
                Duns = model.Duns,
                InitialTransactionProcessingDate = model.InitialTransactionProcessingDate,
                MailingAddress = model.MailingAddress,
                OnePasswordClientAcronoym = model.OnePasswordClientAcronoym,
                Phone = model.Phone,
                PhysicalAddress = model.PhysicalAddress,
                President = model.President,
                RegisteredAgent = model.RegisteredAgent,
                ServiceStartDate = model.ServiceStartDate,
                Software = model.Software,
                StateRegistration = model.StateRegistration,
                Tresurer = model.Tresurer,
                YearEndDate = model.YearEndDate,
            };

            database.Clients.Add(client);

            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId);

            client.Calendar = calendar;

            foreach (var serviceId in model.ServiceIds)
            {
                var serviceInstance = await serviceService.CreateServiceInstanceFrom(serviceId, client);
                serviceInstance.Event.Calendar = calendar;
                serviceInstance.Tasks.ToList().ForEach(ti => ti.Event.Calendar = calendar);
                serviceInstance.Tasks.SelectMany(t => t.Children).ToList().ForEach(ti => ti.Event.Calendar = calendar);
            }

            if (model.TaxClassificationId.HasValue)
            {
                client.TaxClassification = await database.TaxClassifications.FindOrFailAsync(model.TaxClassificationId);
            }

            var assinees = await teamMemberService.GetUsers(model.TeamMemberAssignmentIds);
            assinees.ToList().ForEach(client.TeamMemberAssignments.Add);

            await database.SaveChangesAsync();

            return CreatedAtAction(nameof(GetClient), new { id = client.Id }, mapper.Map<ClientDto>(client));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task DeleteClient(Guid id)
        {
            var client = await database.Clients
                .Include(c => c.Calendar)
                .Include(c => c.Services)
                .ThenInclude(si => si.Softwares)
                .ThenInclude(s => s.Attachments)
                .Include(c => c.Services)
                .ThenInclude(si => si.Attachments)
                .Include(c => c.Services)
                .ThenInclude(si => si.Event)
                .FirstOrFail(c => c.Id == id);

            foreach (var service in client.Services)
            {
                await serviceService.RemoveService(service);
            }

            database.Calendars.Remove(client.Calendar);
            database.Clients.Remove(client);

            await database.SaveChangesAsync();
        }

        /// <summary>
        /// Add task to client.
        /// </summary>
        /// <param name="clientId">Id client.</param>
        /// <param name="taskInstanceDto">Task instance data.</param>
        /// <remarks>
        /// Recurring can be: None: null,
        /// SemiWeekly: RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO;BYDAY=TU,FR,
        /// Weekly: RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO,
        /// SemiMonthly: RRULE:FREQ=WEEKLY;INTERVAL=2;WKST=MO,
        /// Quartely: RRULE:FREQ=MONTHLY;INTERVAL=3;WKST=MO,
        /// Annually: RRULE:FREQ=YEARLY;INTERVAL=1;WKST=MO.
        /// </remarks>
        /// <returns>Task.</returns>
        [HttpPost("{clientId}/tasks/instances")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<TaskEventDto> AddTask(
            Guid clientId,
            [FromBody] CreateTaskInstanceDto taskInstanceDto)
        {
            var serviceInstance = await database.ServiceInstances
                .Include(si => si.Template)
                .ThenInclude(s => s.Tasks)
                .Include(si => si.Client)
                .Include(c => c.Client.Calendar)
                .Include(c => c.Client.TeamMemberAssignments)
                .FirstOrFail(c => c.Id == taskInstanceDto.ServiceId && c.Client.Id == clientId);

            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                taskInstanceDto.Title,
                taskInstanceDto.StartUtc.Date,
                taskInstanceDto.StartUtc.Date.AddDays(1).AddTicks(-1),
                taskInstanceDto.RecurrenceRule)
            {
                CalendarId = serviceInstance.Client.Calendar.Id
            };

            var parentTaskEvent = taskInstanceDto.ParentId == null ? null :
                await database.TaskEvents
                    .Include(t => t.Instance)
                    .FirstOrFail(t => t.Id == taskInstanceDto.ParentId);

            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                taskInstanceDto.Title)
            {
                Service = serviceInstance,
                Event = calendarEvent,
                Info = taskInstanceDto.Info,
                IsHighPriority = taskInstanceDto.IsHighPriority,
                ForReview = taskInstanceDto.ForReview,
                Parent = parentTaskEvent?.Instance,
                Links = taskInstanceDto.Links
            };
            var taskEvent = await taskEventService.Create(taskInstance, calendarEvent.StartUtc);
            if (taskEvent == null)
            {
                throw new ClientErrorException("Can't create task event");
            }

            taskEvent.Parent = parentTaskEvent;
            taskInstance.TaskEvents.Add(taskEvent);
            foreach (var teamMemberId in taskInstanceDto.AssignedTo)
            {
                var teamMember = serviceInstance.Client.TeamMemberAssignments.FirstOrFail(tm => tm.Id == teamMemberId);
                taskInstance.Assignees.Add(teamMember);
            }

            if (!taskInstanceDto.ForReview && taskInstanceDto.SupervisorIds.Any())
            {
                throw new ClientErrorException("Not reviewing task should have not supervisor");
            }

            foreach (var teamMemberId in taskInstanceDto.SupervisorIds)
            {
                var teamMember = serviceInstance.Client.TeamMemberAssignments.FirstOrFail(tm => tm.Id == teamMemberId);
                taskInstance.Supervisors.Add(teamMember);
            }

            if (taskInstanceDto.SoftwareId.HasValue)
            {
                var software = await database.Softwares.FindOrFailAsync(taskInstanceDto.SoftwareId);
                taskInstance.Software = software;
            }

            serviceInstance.Client.Calendar.CalendarEvents.Add(calendarEvent);
            database.TaskInstances.Add(taskInstance);
            serviceInstance.Tasks.Add(taskInstance);
            await database.SaveChangesAsync();
            return mapper.Map<TaskEventDto>(taskEvent);
        }

        /// <summary>
        /// Instantiate an upcoming task instance for a recurring task.
        /// </summary>
        /// <param name="clientId">Client Id.</param>
        /// <param name="taskInstanceId">Base task instance Id.</param>
        /// <param name="taskInstanceDto">Task instance occurrence data.</param>
        /// <remarks>
        /// Recurring can be: None: null,
        /// SemiWeekly: RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO;BYDAY=TU,FR,
        /// Weekly: RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO,
        /// SemiMonthly: RRULE:FREQ=WEEKLY;INTERVAL=2;WKST=MO,
        /// Quartely: RRULE:FREQ=MONTHLY;INTERVAL=3;WKST=MO,
        /// Annually: RRULE:FREQ=YEARLY;INTERVAL=1;WKST=MO.
        /// </remarks>
        /// <returns>Task.</returns>
        [HttpPost("{clientId}/tasks/instances/instantiate/{taskInstanceId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<TaskEventDto> InstantiateInstanceForDate(
            Guid clientId,
            Guid taskInstanceId,
            [FromBody] InstantiateTaskInstanceEventDto taskInstanceDto)
        {
            var taskInstance = await database.TaskInstances
                .Include(ti => ti.Event)
                .FirstOrFail(ti => ti.Service!.Client.Id == clientId && ti.Id == taskInstanceId);

            var taskEvent = await taskEventService.Create(taskInstance, taskInstanceDto.DateOfOccurrence);
            if (taskEvent == null)
            {
                throw new ClientErrorException("Can't create task event");
            }
            var existingEvent = await database.TaskEvents
                .Where(te => te.StartDateUtc == taskEvent.StartDateUtc && te.EndDateUtc == taskEvent.EndDateUtc)
                .FirstOrDefaultAsync();

            if (existingEvent != null)
            {
                throw new ClientErrorException("Task event already exists on specified date");
            }
            taskInstance.TaskEvents.Add(taskEvent);
            database.TaskEvents.Add(taskEvent);
            await database.SaveChangesAsync();
            return mapper.Map<TaskEventDto>(taskEvent);
        }

        /// <summary>
        /// Update task instance.
        /// </summary>
        /// <param name="clientId">Client id.</param>
        /// <param name="taskId">Task id.</param>
        /// <param name="taskDto">Task update data.</param>
        /// <returns>Task.</returns>
        [HttpPut("{clientId}/tasks/instances/{taskId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task UpdateTaskInstance(
            Guid clientId,
            Guid taskId,
            [FromBody] UpdateTaskInstanceDto taskDto)
        {
            var taskEvent = await database.TaskEvents
                .Include(te => te.Instance)
                .ThenInclude(ti => ti.Event)
                .FirstOrFail(t => t.Id == taskId && t.Instance.Service!.Client.Id == clientId);

            if (taskDto.ServiceId != null)
            {
                var service = await database.ServiceInstances
                    .FirstOrFail(s => s.Id == taskDto.ServiceId && s.Client.Id == clientId);
                taskEvent.Instance.Service = service;
            }

            if (taskDto.SoftwareId != null)
            {
                var software = await database.Softwares
                    .FirstOrFail(s => s.Id == taskDto.SoftwareId);
                taskEvent.Instance.Software = software;
            }
            else
            {
                taskEvent.Instance.Software = null;
            }

            taskEvent.StartDateUtc = taskDto.StartUtc?.Date ?? taskEvent.StartDateUtc;
            taskEvent.EndDateUtc = taskEvent.StartDateUtc.Date.AddDays(1).AddTicks(-1);

            taskEvent.Instance.Name = taskDto.Title ?? taskEvent.Instance.Name;
            taskEvent.Instance.Info = taskDto.Info ?? taskEvent.Instance.Info;
            taskEvent.Instance.ForReview = taskDto.ForReview ?? taskEvent.Instance.ForReview;
            taskEvent.Instance.IsHighPriority = taskDto.IsHighPriority ?? taskEvent.Instance.IsHighPriority;
            taskEvent.Instance.Links = taskDto.Links ?? taskEvent.Instance.Links;

            await database.SaveChangesAsync();
        }

        [HttpDelete("{clientId}/tasks/events/{taskId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task RemoveTask(Guid clientId, Guid taskId)
        {
            var taskEvent = await database.TaskEvents
                .Include(te => te.Instance)
                .ThenInclude(ti => ti.Event)
                .FirstOrFail(t => t.Id == taskId && t.Instance.Service!.Client.Id == clientId);
            await taskEventService.RemoveEvent(taskEvent);
            var newTaskEvent = await taskEventService.Create(taskEvent.Instance, taskEvent.EndDateUtc);
            if (newTaskEvent != null)
            {
                database.TaskEvents.Add(newTaskEvent);
            }

            await database.SaveChangesAsync();
        }

        [HttpDelete("{clientId}/tasks/instances/{taskId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task RemoveTaskInstance(Guid clientId, Guid taskId)
        {
            var taskEvent = await database.TaskEvents
                .Include(te => te.Instance.Event)
                .Include(te => te.Instance.TaskEvents)
                .Include(te => te.Instance.Children)
                .ThenInclude(te => te.TaskEvents)
                .Include(te => te.Instance.Children)
                .ThenInclude(te => te.Event)
                .Include(te => te.Children)
                .FirstOrFail(t => t.Id == taskId && t.Instance.Service!.Client.Id == clientId);

            await taskService.RemoveTask(taskEvent.Instance);

            await database.SaveChangesAsync();
        }

        [HttpGet("{clientId}/tasks/instances/{taskId}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<TaskEventDto> GetTask(
            Guid clientId,
            Guid taskId,
            bool includeBlockers = false,
            bool includeSubtasks = false)
        {
            var query = database.TaskEvents
                .Include(ti => ti.Instance.Event)
                .Include(ti => ti.State)
                .Include(ti => ti.Instance.Assignees)
                .Include(ti => ti.Instance.Supervisors)
                .Include(ti => ti.Instance.Template)
                .Include(t => t.Instance.Service!.Client)
                .AsNoTracking();

            if (includeBlockers)
            {
                query = query
                    .Include(ti => ti.Blockers)
                    .ThenInclude(b => b.CalendarEvent);
            }

            if (includeSubtasks)
            {
                query = query
                    .Include(ti => ti.Children)
                    .ThenInclude(ti => ti.Instance.Event);
            }

            var entity = await query.FirstOrFail(t => t.Id == taskId);
            var taskEvent = mapper.Map<TaskEventDto>(entity);
            return taskEvent;
        }

        [HttpGet("{clientId}/tasks")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<TaskEventShortDto>> GetTasks(
            Guid clientId)
        {
            var query = database.TaskEvents
                .Include(ti => ti.Instance.Event)
                .Include(ti => ti.State)
                .Include(ti => ti.Instance.Assignees)
                .Include(ti => ti.Instance.Template)
                .Include(t => t.Instance.Service!.Client)
                .Include(t => t.Instance.Service!.Event)
                .AsNoTracking()
                .Where(ti => ti.Instance.Service!.Client.Id == clientId);

            var data = await query.ToListAsync();
            return data.Select(mapper.Map<TaskEventShortDto>);
        }

        [HttpGet("{clientId}/services")]
        public Task<List<ServiceInstanceDto>> GetServices(Guid clientId, [FromQuery] ClientServiceInstanceFilterDto filterDto)
        {
            var query = database.ServiceInstances
                .Include(c => c.Client)
                .Include(c => c.Template)
                .Include(si => si.Attachments)
                .Where(si => si.Client.Id == clientId);

            query = query
                .Skip(filterDto.Offset)
                .Take(filterDto.Limit)
                .AsNoTracking();

            return mapper
                .ProjectTo<ServiceInstanceDto>(query)
                .ToListAsync();
        }

        [HttpPost("{clientId}/services/{serviceTemplateId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ServiceInstanceDto> AddService(Guid clientId, Guid serviceTemplateId)
        {
            var client = await database.Clients
                .Include(c => c.Calendar)
                .Include(c => c.Services.Where(s => s.Template!.Id == serviceTemplateId))
                .ThenInclude(si => si.Template)
                .ThenInclude(si => si.ServiceType)
                .FirstOrFail(c => c.Id == clientId);

            var template = client.Services.FirstOrDefault(s => s.Template!.Id == serviceTemplateId);

            if (template != null)
            {
                throw new ClientErrorException("Duplicate service");
            }

            var serviceInstance = await serviceService.CreateServiceInstanceFrom(serviceTemplateId, client);
            client.Services.Add(serviceInstance);

            serviceInstance.Client = client;
            database.ServiceInstances.Add(serviceInstance);

            await database.SaveChangesAsync();
            return mapper.Map<ServiceInstanceDto>(serviceInstance);
        }

        [HttpGet("{clientId}/services/{serviceId}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ServiceInstanceDto> GetService(Guid clientId, Guid serviceId)
        {
            var serviceInstances = await database.ServiceInstances
                .Include(si => si.Template)
                .Include(si => si.Attachments)
                .Include(si => si.Event)
                .Include(si => si.Softwares)
                .AsNoTracking()
                .FirstOrFail(t => t.Id == serviceId && t.Client!.Id == clientId);

            return mapper.Map<ServiceInstanceDto>(serviceInstances);
        }

        [HttpDelete("{clientId}/services/{serviceId}")]
        [Authorize(Roles = UserPermission.Owner)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task RemoveService(Guid clientId, Guid serviceId)
        {
            var serviceInstance = await database.ServiceInstances
                .Include(si => si.Attachments)
                .Include(si => si.Event)
                .Include(si => si.Softwares)
                .Include(si => si.Tasks)
                .IgnoreQueryFilters()
                .FirstOrFail(si => si.Id == serviceId && si.Client.Id == clientId);

            await serviceService.RemoveService(serviceInstance);

            await database.SaveChangesAsync();
        }

        [HttpPut("{clientId}/tasks/{tasksId}/states/{stateId}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task UpdateTaskState(Guid clientId, Guid tasksId, Guid stateId)
        {
            var task = await database.TaskEvents
                .Include(te => te.State)
                .Include(te => te.Instance)
                .ThenInclude(ti => ti.Event)
                .FirstOrFail(te => te.Id == tasksId && te.Instance.Service!.Client.Id == clientId);

            await taskStateService.UpdateState(task, stateId);

            if (stateId == ITaskStateService.DoneStateId)
            {
                var newTask = await taskEventService.Create(task.Instance, task.EndDateUtc);
                if (newTask != null)
                {
                    database.TaskEvents.Add(newTask);
                }
            }

            await database.SaveChangesAsync();
        }

        [HttpPut("{clientId}/tasks/{tasksId}/assign/{teamMemberId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task AssigneeTask(Guid clientId, Guid tasksId, Guid teamMemberId)
        {
            var task = await database.TaskEvents
                .Include(te => te.Instance.Assignees)
                .FirstOrFail(te => te.Id == tasksId && te.Instance.Service!.Client.Id == clientId);

            var alreadyContain = task.Instance.Assignees.Any(a => a.Id == teamMemberId);
            if (alreadyContain)
            {
                throw new ClientErrorException("User already exists");
            }

            var teamMember = await database.TeamMembers
                .FirstOrFail(tm => tm.Id == teamMemberId && tm.Clients.Any(c => c.Id == clientId));

            task.Instance.Assignees.Add(teamMember);

            await database.SaveChangesAsync();
        }

        [HttpDelete("{clientId}/tasks/{tasksId}/deassign/{teamMemberId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task DeassignTask(Guid clientId, Guid tasksId, Guid teamMemberId)
        {
            var task = await database.TaskEvents
                .Include(te => te.Instance.Assignees)
                .FirstOrFail(te => te.Id == tasksId && te.Instance.Service!.Client.Id == clientId);

            var teamMember = task.Instance.Assignees.FirstOrFail(ti => ti.Id == teamMemberId);

            task.Instance.Assignees.Remove(teamMember);

            await database.SaveChangesAsync();
        }

        [HttpPut("{clientId}/tasks/{tasksId}/supervise/{teamMemberId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task SuperviseTask(Guid clientId, Guid tasksId, Guid teamMemberId)
        {
            var task = await database.TaskEvents
                .Include(te => te.Instance.Supervisors)
                .FirstOrFail(te => te.Id == tasksId && te.Instance.Service!.Client.Id == clientId);

            var alreadyContain = task.Instance.Supervisors.Any(a => a.Id == teamMemberId);
            if (alreadyContain)
            {
                throw new ClientErrorException("User already exists");
            }

            var teamMember = await database.TeamMembers
                .FirstOrFail(tm => tm.Id == teamMemberId && tm.Clients.Any(c => c.Id == clientId));

            task.Instance.Supervisors.Add(teamMember);
            task.Instance.ForReview = true;
            await database.SaveChangesAsync();
        }

        [HttpDelete("{clientId}/tasks/{tasksId}/supervise/{teamMemberId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task UnsuperviseTask(Guid clientId, Guid tasksId, Guid teamMemberId)
        {
            var task = await database.TaskEvents
                .Include(te => te.Instance.Supervisors)
                .FirstOrFail(te => te.Id == tasksId && te.Instance.Service!.Client.Id == clientId);

            var teamMember = task.Instance.Supervisors.FirstOrFail(ti => ti.Id == teamMemberId);

            task.Instance.Supervisors.Remove(teamMember);
            task.Instance.ForReview = task.Instance.Supervisors.Any();

            await database.SaveChangesAsync();
        }

        [HttpDelete("{clientId}/team/{teamMemberId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task RemoveTeamMember(Guid clientId, Guid teamMemberId)
        {
            var client = await database.Clients
                .Include(c => c.TeamMemberAssignments.Where(tm => tm.Id == teamMemberId))
                .FirstOrFail(t => t.Id == clientId);
            var teamMember = client.TeamMemberAssignments.FirstOrFail(s => s.Id == teamMemberId);
            client.TeamMemberAssignments.Remove(teamMember);

            var tasks = await database.TaskInstances
                .Include(ti => ti.Assignees)
                .Include(ti => ti.Supervisors)
                .Where(ti => ti.Service!.Client.Id == clientId &&
                    (ti.Assignees.Any(a => a.Id == teamMemberId) || ti.Supervisors.Any(a => a.Id == teamMemberId)))
                .ToListAsync();
            foreach (var task in tasks)
            {
                var assignee = task.Assignees.FirstOrFail(a => a.Id == teamMemberId);
                task.Assignees.Remove(assignee);

                var supervisor = task.Supervisors.FirstOrFail(a => a.Id == teamMemberId);
                task.Supervisors.Remove(assignee);
            }

            await database.SaveChangesAsync();
        }

        [HttpPut("{clientId}/team/{teamMemberId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task AddTeamMember(Guid clientId, Guid teamMemberId)
        {
            var client = await database.Clients
                .Include(c => c.TeamMemberAssignments.Where(tm => tm.Id == teamMemberId))
                .FirstOrFail(t => t.Id == clientId);

            if (client.TeamMemberAssignments.Any(tm => tm.Id == teamMemberId))
            {
                throw new ClientErrorException("Team member already exist");
            }

            var teamMember = await database.TeamMembers.FirstOrFail(tm => tm.Id == teamMemberId && tm.EmailConfirmed);
            client.TeamMemberAssignments.Add(teamMember);
            await database.SaveChangesAsync();
        }

        [HttpGet("archive/count")]
        public async Task<ArchiveCountDto> GetArchiveCount()
        {
            var query = database.Clients
                .IgnoreQueryFilters()
                .Where(c => c.ArchiveInfo != null);
            var count = await query.CountAsync();
            return new ArchiveCountDto { Count = count };
        }

        [HttpPut("archive/{clientId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        public async Task ArchiveClient(Guid clientId)
        {
            var data = await database.Clients
                .Select(c => new
                {
                    Client = c,
                    NotDoneContain = c.Services
                        .SelectMany(s => s.Tasks)
                        .SelectMany(t => t.TaskEvents)
                        .Where(t => t.State.Id != ITaskStateService.DoneStateId).Any()
                })
                .FirstOrFail(c => c.Client.Id == clientId);

            if (data.NotDoneContain)
            {
                throw new ClientErrorException("Client has open tasks");
            }

            data.Client.ArchiveInfo = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId);

            await database.SaveChangesAsync();
        }

        [HttpDelete("archive/{clientId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        public async Task ReactiveClient(Guid clientId)
        {
            var data = await database.Clients
                .Include(c => c.ArchiveInfo)
                .IgnoreQueryFilters()
                .FirstOrFail(c => c.Id == clientId
                    && c.ArchiveInfo != null);

            database.Remove(data.ArchiveInfo);
            data.ArchiveInfo = null;

            await database.SaveChangesAsync();
        }

        [HttpGet("archive")]
        public async Task<IEnumerable<ArchiveClientDto>> GetArchiveClients([FromQuery] ArchiveClientsFilterDto filterDto)
        {
            var query = database.Clients
                .Include(c => c.Calendar)
                .Include(c => c.Services)
                .ThenInclude(c => c.Template)
                .ThenInclude(c => c.ServiceType)
                .Include(c => c.TeamMemberAssignments)
                .IgnoreQueryFilters()
                .Where(c => c.ArchiveInfo != null)
                .AsNoTracking();

            if (filterDto.Title != null)
            {
                query = query
                    .Where(s => EF.Functions.Like(s.CompanyName.ToLower(), $"%{filterDto.Title.ToLower()}%")
                        || EF.Functions.Like((s.Tresurer ?? string.Empty).ToLower(), $"%{filterDto.Title.ToLower()}%"));
            }

            if (filterDto.Order != null)
            {
                query = OrderQuery(query, filterDto.Order);
            }

            query = query
                .Skip(filterDto.Offset)
                .Take(filterDto.Limit);
            return await mapper
                .ProjectTo<ArchiveClientDto>(query)
                .ToListAsync();
        }

        private bool ClientExists(Guid id)
        {
            return database.Clients.Any(e => e.Id == id);
        }

        private IQueryable<Client> OrderQuery(IQueryable<Client> query, OrderClientsDto orderDto)
        {
            switch (orderDto.Field)
            {
                case OrderClientsDto.OrderClientField.Name:
                    return orderDto.Desc
                        ? query.OrderByDescending(s => s.CompanyName.ToLower())
                        : query.OrderBy(s => s.CompanyName.ToLower());

                case OrderClientsDto.OrderClientField.ServiceCount:
                    return orderDto.Desc
                        ? query.OrderByDescending(s => s.Services.Count)
                        : query.OrderBy(s => s.Services.Count);

                case OrderClientsDto.OrderClientField.StartDate:
                    return orderDto.Desc
                        ? query.OrderByDescending(s => s.ServiceStartDate)
                        : query.OrderBy(s => s.ServiceStartDate);
            }

            throw new NotImplementedException();
        }
    }
}
