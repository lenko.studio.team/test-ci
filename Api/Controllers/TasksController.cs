﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Jfdi.ProjectManagement.Api.Attributes;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Tasks;
using Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Jfdi.ProjectManagement.Api.Services;
using Jfdi.ProjectManagement.Api.Services.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TasksController : BaseController
    {
        private readonly JfdiDbContext database;
        private readonly IMapper mapper;
        private readonly ITasksService tasksService;
        private readonly ICurrentUserProvider currentUser;
        private readonly ITaskEventService taskEventService;

        public TasksController(
            JfdiDbContext database,
            IMapper mapper,
            ITasksService tasksService,
            ICurrentUserProvider currentUser,
            ITaskEventService taskEventService)
        {
            this.database = database;
            this.mapper = mapper;
            this.tasksService = tasksService;
            this.currentUser = currentUser;
            this.taskEventService = taskEventService;
        }

        /// <summary>
        /// Create task.
        /// </summary>
        /// <param name="model">Task creation data.</param>
        /// <remarks>
        /// Recurring can be: None: null,
        /// SemiWeekly: RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO;BYDAY=TU,FR,
        /// Weekly: RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO,
        /// SemiMonthly: RRULE:FREQ=WEEKLY;INTERVAL=2;WKST=MO,
        /// Quartely: RRULE:FREQ=MONTHLY;INTERVAL=3;WKST=MO,
        /// Annually: RRULE:FREQ=YEARLY;INTERVAL=1;WKST=MO.
        /// </remarks>
        /// <returns>Task.</returns>
        [HttpPost]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public async Task<TaskDto> PostTask(CreateTaskDto model)
        {
            var serviceTask = tasksService.CreateTask(model);
            await database.SaveChangesAsync();

            return mapper.Map<TaskDto>(serviceTask);
        }

        /// <summary>
        /// Update task.
        /// </summary>
        /// <param name="taskId">Task id.</param>
        /// <param name="taskDto">Task update data.</param>
        /// <remarks>
        /// Recurring can be: None: null,
        /// SemiWeekly: RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO;BYDAY=TU,FR,
        /// Weekly: RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO,
        /// SemiMonthly: RRULE:FREQ=WEEKLY;INTERVAL=2;WKST=MO,
        /// Quartely: RRULE:FREQ=MONTHLY;INTERVAL=3;WKST=MO,
        /// Annually: RRULE:FREQ=YEARLY;INTERVAL=1;WKST=MO.
        /// </remarks>
        /// <returns>Task.</returns>
        [HttpPut("{taskId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task UpdateTask(
            Guid taskId,
            [FromBody] CreateTaskDto taskDto)
        {
            var task = await database.Tasks
                .FirstOrFail(x => x.Id == taskId);

            mapper.Map(taskDto, task);
            task.StartDateUtc = taskDto.StartUtc.Date;
            task.EndDateUtc = task.StartDateUtc.Date.AddDays(1).AddTicks(-1);
            await database.SaveChangesAsync();
        }

        [HttpDelete("{taskId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task DeleteTask(Guid taskId)
        {
            var isContains = await database.Services.AnyAsync(service =>
                service.Tasks.Any(t => t.Id == taskId));
            Assert.False(isContains);

            var subtasks = await database.Tasks.Where(t => t.Parent!.Id == taskId).ToListAsync();
            database.RemoveRange(subtasks);

            var task = await database.Tasks.FirstOrFail(t => t.Id == taskId);
            database.Remove(task);

            await database.SaveChangesAsync();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<IEnumerable<TaskDto>> GetTasks([FromQuery] TaskFilterDto filter)
        {
            var query = database.Tasks
                .Where(t => t.Parent == null)
                .AsNoTracking();

            return await query
                .Skip(filter.Offset)
                .Take(filter.Limit)
                .ProjectTo<TaskDto>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        [HttpGet("{taskId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<TaskDto> GetTask(Guid taskId)
        {
            var query = database.Tasks.AsQueryable();
            return await mapper
                .ProjectTo<TaskDto>(query)
                .FirstOrFail(t => t.Id == taskId);
        }

        [HttpPut("{taskId}/parent")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task ChangeParent(Guid taskId, UpdateTaskParentDto parentDto)
        {
            Assert.NotEqual(taskId, parentDto.ParentId);

            var task = await database.Tasks.Include(t => t.Parent).FirstOrFail(t => t.Id == taskId);
            Assert.NotNull(task.Parent);

            var newParent = await database.Tasks.Include(t => t.Parent).FirstOrFail(t => t.Id == parentDto.ParentId);
            Assert.Null(newParent.Parent);

            var serviceExist = await database.Services
                            .AnyAsync(s => s.Tasks.Any(t => t.Id == task.Parent!.Id)
                                && s.Tasks.Any(t => t.Id == newParent.Id));

            Assert.True(serviceExist);

            task.Parent = newParent;

            await database.SaveChangesAsync();
        }

        [HttpPost("{taskId}/subtasks")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<SubTaskDto> PostSubTask(Guid taskId, [FromBody]CreateSubTaskDto model)
        {
            var parentTask = await database.Tasks
                .FirstOrFail(t => t.Id == taskId && t.Parent == null);

            var subTask = tasksService.CreateSubTask(model, parentTask);

            await database.SaveChangesAsync();

            return mapper.Map<SubTaskDto>(subTask);
        }

        [HttpGet("instances")]
        public async Task<IEnumerable<TaskEventDto>> GetTasks([FromQuery] ClientTaskInstanceFilterDto filterDto)
        {
            var query = database.TaskEvents
                .Include(si => si.Instance.Assignees)
                .Include(si => si.Instance.Supervisors)
                .Include(si => si.State)
                .Include(ti => ti.Instance.Software)
                .Include(ti => ti.Instance.Event)
                .Include(ti => ti.Instance.Service)
                .Include(ti => ti.Instance.Service!.Client)
                .Include(ti => ti.Instance.Service!.Template.ServiceType)
                .Where(ti => ti.Parent == null)
                .OrderBy(ti => ti.StartDateUtc)
                .AsQueryable();

            if (filterDto.IncludeBlockers)
            {
                query = query
                    .Include(ti => ti.Blockers)
                    .ThenInclude(b => b.CalendarEvent);
            }

            if (filterDto.IncludeSubTasks)
            {
                query = query
                    .Include(ti => ti.Children)
                    .ThenInclude(ti => ti.Blockers)
                    .Include(ti => ti.Children)
                    .ThenInclude(ti => ti.State)
                    .Include(ti => ti.Children)
                    .ThenInclude(ti => ti.Instance);
            }

            if (filterDto.StateId.HasValue)
            {
                query = query
                    .Where(ti => ti.State.Id == filterDto.StateId);
            }

            if (filterDto.AssignedId.HasValue)
            {
                query = query
                    .Where(ti => ti.Instance.Assignees.Any(a => a.Id == filterDto.AssignedId));
            }

            if (filterDto.SupervisorId.HasValue)
            {
                query = query
                    .Where(ti => ti.Instance.Supervisors.Any(a => a.Id == filterDto.SupervisorId));
            }

            if (filterDto.ClientId.HasValue)
            {
                query = query
                    .Where(ti => ti.Instance.Service!.Client.Id == filterDto.ClientId);
            }

            if (filterDto.SearchStartUtc.HasValue)
            {
                query = query
                    .Where(ti => ti.StartDateUtc >= filterDto.SearchStartUtc.Value.AddYears(-1));
            }

            if (filterDto.SearchEndUtc.HasValue)
            {
                query = query
                    .Where(ti => ti.EndDateUtc <= filterDto.SearchEndUtc.Value);
            }

            if (filterDto.IsHighPriority.HasValue)
            {
                query = query
                    .Where(ti => ti.Instance.IsHighPriority == filterDto.IsHighPriority.Value);
            }

            if (filterDto.OrderType != null)
            {
                query = tasksService.CreateOrderQuery(query, filterDto.OrderType);
            }

            var data = await query
                .ToListAsync();

            if (filterDto.StateId == ITaskStateService.TodoStateId || filterDto.StateId == null)
            {
                // calculate unstored recurring tasks
                var calculated = await taskEventService.CalculateRecurringEvents(
                    data,
                    filterDto.SearchStartUtc ?? DateTime.UtcNow.Date.AddYears(-10),
                    filterDto.SearchEndUtc ?? DateTime.UtcNow.Date.AddDays(1).AddYears(1));

                return calculated;
            }

            return data.Select(mapper.Map<TaskEventDto>);
        }

        [HttpGet("instances/archive/count")]
        public async Task<ArchiveCountDto> GetArchiveCount()
        {
            var query = database.TaskEvents
                .IgnoreQueryFilters()
                .Where(ti => ti.ArchiveInfo != null);

            if (currentUser.User.Permission == Models.Users.UserPermissions.Member)
            {
                query = query.Where(te => te.ArchiveInfo!.ArchivedById == currentUser.CurrentUserId);
            }

            var count = await query.CountAsync();
            return new ArchiveCountDto { Count = count };
        }

        [HttpPut("instances/archive/{taskId}")]
        public async Task ArchiveTask(Guid taskId)
        {
            var task = await database.TaskEvents
                .FirstOrFail(ti => ti.Id == taskId
                    && ti.State.Id == ITaskStateService.DoneStateId);
            task.ArchiveInfo = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId);
            await database.SaveChangesAsync();
        }

        [HttpDelete("instances/archive/{taskId}")]
        public async Task ReactiveTask(Guid taskId)
        {
            var data = await database.TaskEvents
                .Include(c => c.ArchiveInfo)
                .IgnoreQueryFilters()
                .FirstOrFail(te => te.Id == taskId
                    && te.ArchiveInfo != null);
            database.Remove(data.ArchiveInfo);
            data.ArchiveInfo = null;
            await database.SaveChangesAsync();
        }

        [HttpGet("instances/archive")]
        public async Task<IEnumerable<ArchivedTaskDto>> GetArchiveTasks([FromQuery]ServiceFilterDto filterDto)
        {
            var query = database.TaskEvents
                .Include(ti => ti.Instance.Service!.Client)
                .Include(ti => ti.Instance.Children)
                .Include(ti => ti.Instance.Service)
                .ThenInclude(si => si!.Template.ServiceType)
                .IgnoreQueryFilters()
                .Where(ti => ti.ArchiveInfo != null)
                .Skip(filterDto.Offset)
                .Take(filterDto.Limit);

            if (currentUser.User.Permission == Models.Users.UserPermissions.Member)
            {
                query = query.Where(te => te.ArchiveInfo!.ArchivedById == currentUser.CurrentUserId);
            }

            var data = await query.ToListAsync();
            return data.Select(mapper.Map<ArchivedTaskDto>);
        }
    }
}
