﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Dtos.Notifications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class NotificationsController : BaseController
    {
        private readonly IMapper mapper;
        private readonly JfdiDbContext dbContext;

        public NotificationsController(IMapper mapper, JfdiDbContext dbContext)
        {
            this.mapper = mapper;
            this.dbContext = dbContext;
        }
    }
}
