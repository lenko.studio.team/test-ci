﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos.Comments;
using Jfdi.ProjectManagement.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class CommentsController : BaseController
    {
        private readonly JfdiDbContext database;
        private readonly IMapper mapper;
        private readonly ICurrentUserProvider currentUser;

        public CommentsController(
            JfdiDbContext database,
            IMapper mapper,
            ICurrentUserProvider currentUser)
        {
            this.database = database;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }

        [HttpPost("tasks/{taskId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<CommentDto> AddCommentToTaskInstance(Guid taskId, CreateCommentDto commentDto)
        {
            var task = await database.TaskEvents
                .Include(ti => ti.Comments)
                .FirstOrFail(ti => ti.Id == taskId);

            var comment = new Comment(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                commentDto.Content);

            task.Comments.Add(comment);
            await database.SaveChangesAsync();

            return mapper.Map<CommentDto>(comment);
        }

        [HttpGet("tasks/{taskId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<IEnumerable<CommentDto>> GetTaskInstanceComments(Guid taskId)
        {
            var task = await database.TaskEvents
                .Include(ti => ti.Comments)
                .ThenInclude(cc => cc.CreatedBy.TeamMember)
                .FirstOrFail(ti => ti.Id == taskId);

            return task.Comments.Select(mapper.Map<CommentDto>);
        }

        [HttpDelete("{commentId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task DeleteComment(Guid commentId)
        {
            var comment = await database.Comments
                .FirstOrFail(ti => ti.Id == commentId);

            database.Comments.Remove(comment);
            await database.SaveChangesAsync();
        }

        [HttpPut("{commentId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task UpdateComment(Guid commentId, CreateCommentDto commentDto)
        {
            var comment = await database.Comments
                .FirstOrFail(ti => ti.Id == commentId);

            comment.Data = commentDto.Content;
            await database.SaveChangesAsync();
        }
    }
}
