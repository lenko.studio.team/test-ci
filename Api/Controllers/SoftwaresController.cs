﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class SoftwaresController : BaseController
    {
        private readonly JfdiDbContext dbContext;
        private readonly IMapper mapper;
        private readonly ISoftwareService softwareService;
        private readonly IAttachmentService attachmentService;

        public SoftwaresController(
            JfdiDbContext dbContext,
            IMapper mapper,
            ISoftwareService softwareService,
            IAttachmentService attachmentService)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.softwareService = softwareService;
            this.attachmentService = attachmentService;
        }

        [HttpGet("{softwareId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<SoftwareDto> GetSoftware(Guid softwareId)
        {
            var software = await dbContext.Softwares
                .Include(s => s.Attachments)
                .FirstOrFail(s => s.Id == softwareId);

            return mapper.Map<SoftwareDto>(software);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<SoftwareDto>> GetSoftwares()
        {
            var query = dbContext.Softwares.AsQueryable();

            return await mapper.ProjectTo<SoftwareDto>(query).ToListAsync();
        }

        [HttpPost]
        public async Task<Guid> CreateSoftware(CreateSoftwareDto softwareDto)
        {
            var software = await softwareService.CreateSoftware(softwareDto);

            await dbContext.SaveChangesAsync();

            return software.Id;
        }

        [HttpDelete("{softwareId}")]
        public async Task DeleteSoftware(Guid softwareId)
        {
            var software = await dbContext.Softwares
                .Include(s => s.Attachments)
                .FirstOrFail(s => s.Id == softwareId);

            dbContext.Softwares.Remove(software);
            dbContext.Attachments.RemoveRange(software.Attachments);

            await dbContext.SaveChangesAsync();
        }

        [HttpPut("{softwareId}")]
        public async Task UpdateSoftware(Guid softwareId, CreateSoftwareDto softwareDto)
        {
            var software = await dbContext.Softwares
                .Include(s => s.Attachments)
                .FirstOrFail(s => s.Id == softwareId);

            software.Name = softwareDto.Name;
            software.Description = softwareDto.Description;
            software.Links = softwareDto.Links;
            software.Attachments.Clear();
            var attachments = await attachmentService.FindAttachment(softwareDto.AttachmentIds);
            attachments.ToList().ForEach(software.Attachments.Add);
            await dbContext.SaveChangesAsync();
        }
    }
}
