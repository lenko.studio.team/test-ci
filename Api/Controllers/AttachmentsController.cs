﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos.Attachments;
using Jfdi.ProjectManagement.Api.Models.Attachments;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AttachmentsController : BaseController
    {
        private readonly JfdiDbContext dbContext;

        private readonly IMapper mapper;

        private readonly ICurrentUserProvider currentUser;

        public AttachmentsController(
            JfdiDbContext dbContext,
            ICurrentUserProvider currentUser,
            IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }

        [HttpPost("direct")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Security", "SCS0016:Controller method is potentially vulnerable to Cross Site Request Forgery (CSRF).", Justification = "<Pending>")]
        public async Task<Guid> CreateDirectAttachment([FromForm] CreateDirectAttachmentDto directAttachment)
        {
            using var dataStream = directAttachment.Data.OpenReadStream();
            var data = new byte[directAttachment.Data.Length];
            await dataStream.ReadAsync(data, 0, data.Length);
            var attachment = new DirectAttachment(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                data)
            {
                Name = directAttachment.Data.FileName
            };
            dbContext.Attachments.Add(attachment);
            await dbContext.SaveChangesAsync();

            return attachment.Id;
        }

        [HttpPost("gdrive")]
        public async Task<Guid> CreateGDriveAttachment(CreateGDriveAttachmentDto directAttachment)
        {
            var attachment = new GDriveAttachment(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                directAttachment.Link)
            {
                Name = directAttachment.Name
            };
            dbContext.Attachments.Add(attachment);
            await dbContext.SaveChangesAsync();

            return attachment.Id;
        }

        [HttpGet("{attachmentId}/direct")]
        public async Task<FileResult> GetDirectAttachment(Guid attachmentId)
        {
            var attachment = await dbContext.Attachments
                .AsNoTracking()
                .FirstOrFail(a => a.Id == attachmentId && a.Type == Models.AttachmentType.Direct);

            return mapper.Map<FileResult>(attachment);
        }

        [HttpGet("{attachmentId}/gdrive")]
        public async Task<GDriveAttachmentDto> GetGDriveAttachment(Guid attachmentId)
        {
            var attachment = await dbContext.Attachments
                .AsNoTracking()
                .FirstOrFail(a => a.Id == attachmentId && a.Type == Models.AttachmentType.GoogleDrive);

            return mapper.Map<GDriveAttachmentDto>(attachment);
        }

        [HttpDelete("{attachmentId}")]
        public async Task DeleteAttachment(Guid attachmentId)
        {
            var attachment = await dbContext.Attachments
                .FindOrFailAsync(attachmentId);
            dbContext.Attachments.Remove(attachment);

            await dbContext.SaveChangesAsync();
        }
    }
}
