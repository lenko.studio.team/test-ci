﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ServiceTypesController : BaseController
    {
        private readonly JfdiDbContext database;

        private readonly IMapper mapper;

        private readonly ICurrentUserProvider currentUser;

        public ServiceTypesController(JfdiDbContext database, IMapper mapper, ICurrentUserProvider currentUser)
        {
            this.database = database;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<ServiceTypeDto>> GetServices()
        {
            var serviceTypes = await database.ServiceTypes.AsNoTracking().ToListAsync();

            foreach (var serviceType in serviceTypes)
            {
                serviceType.Services = await database.Services
                    .AsNoTracking()
                    .Where(s => s.ServiceType.Id == serviceType.Id)
                    .ToListAsync();
            }

            return serviceTypes.Select(mapper.Map<ServiceTypeDto>);
        }

        [HttpGet("{serviceTypeId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ServiceTypeDto> GetService(Guid serviceTypeId)
        {
            var query = await database.Services
                .Include(s => s.ServiceType)
                .Where(s => s.ServiceType.Id == serviceTypeId)
                .GroupBy(
                    s => s.ServiceType,
                    s => s)
                .FirstOrDefaultAsync();

            query.Key.Services = query.ToList();

            return mapper.Map<ServiceTypeDto>(query);
        }
    }
}
