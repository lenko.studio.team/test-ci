﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Jfdi.ProjectManagement.Api.Attributes;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Clients;
using Jfdi.ProjectManagement.Api.Dtos.Services;
using Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Jfdi.ProjectManagement.Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ServicesController : BaseController
    {
        private readonly JfdiDbContext database;
        private readonly IMapper mapper;
        private readonly ICurrentUserProvider currentUser;
        private readonly IServiceTypeService serviceTypeService;
        private readonly ISoftwareService softwareService;
        private readonly ITasksService tasksService;
        private readonly IServiceInstanceService serviceInstanceService;

        public ServicesController(
            JfdiDbContext database,
            IMapper mapper,
            ICurrentUserProvider currentUser,
            IServiceTypeService serviceTypeService,
            ISoftwareService softwareService,
            ITasksService tasksService,
            IServiceInstanceService serviceInstanceService)
        {
            this.database = database;
            this.mapper = mapper;
            this.currentUser = currentUser;
            this.serviceTypeService = serviceTypeService;
            this.softwareService = softwareService;
            this.tasksService = tasksService;
            this.serviceInstanceService = serviceInstanceService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IEnumerable<ServiceDto>> GetServices([FromQuery] ServiceFilterDto filter)
        {
            var query = database.Services
                .Include(s => s.ServiceType)
                .Include(s => s.Instancies)
                .Include(s => s.Tasks)
                .ThenInclude(t => t.Children)
                .Include(s => s.Softwares)
                .ThenInclude(s => s.Attachments)
                .Include(s => s.Attachments)
                .AsQueryable();

            if (!string.IsNullOrEmpty(filter.Title))
            {
                query = query
                    .Where(s => EF.Functions.Like(s.Name.ToLower(), $"%{filter.Title.ToLower()}%"));
            }

            if (filter.ServiceTypeIds.Any())
            {
                query = query.Where(s => filter.ServiceTypeIds.Contains(s.ServiceType.Id));
            }

            if (filter.OrderType != null)
            {
                query = serviceInstanceService.CreateOrderQuery(query, filter.OrderType);
            }

            var services = await query
                .Skip(filter.Offset)
                .Take(filter.Limit).ToListAsync();

            return services.Select(mapper.Map<ServiceDto>);
        }

        [HttpGet("{serviceId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ServiceDto> GetService(Guid serviceId)
        {
            var service = await database.Services
                .Include(s => s.ServiceType)
                .Include(s => s.Instancies)
                .Include(s => s.Tasks)
                .ThenInclude(t => t.Children)
                .Include(s => s.Softwares)
                .ThenInclude(s => s.Attachments)
                .Include(s => s.Attachments)
                .FirstOrFail(s => s.Id == serviceId);

            return mapper.Map<ServiceDto>(service);
        }

        /// <summary>
        /// Create service template.
        /// </summary>
        /// <remarks>
        /// Recurring can be: None: null,
        /// SemiWeekly: RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO;BYDAY=TU,FR,
        /// Weekly: RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO,
        /// SemiMonthly: RRULE:FREQ=WEEKLY;INTERVAL=2;WKST=MO,
        /// Quartely: RRULE:FREQ=MONTHLY;INTERVAL=3;WKST=MO,
        /// Annually: RRULE:FREQ=YEARLY;INTERVAL=1;WKST=MO.
        /// </remarks>
        /// <param name="model">Model for creating service template.</param>
        /// <returns>Created service template.</returns>
        [HttpPost]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public async Task<ServiceDto> PostService(CreateServiceDto model)
        {
            var service = new Service(
                model.Name,
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId);
            mapper.Map(model, service);
            service.ServiceType = await serviceTypeService.GetServiceType(model.ServiceTypeId);

            var softwares = await softwareService.FindSoftwares(model.SoftwareIds);
            softwares.ToList().ForEach(service.Softwares.Add);

            if (model.Attachments != null)
            {
                var attachments = await database.Attachments.Where(a => model.Attachments.Contains(a.Id)).ToListAsync();
                attachments.ForEach(service.Attachments.Add);
            }

            model.Tasks.Select(tasksService.CreateTask)
                .ToList()
                .ForEach(service.Tasks.Add);

            database.Services.Add(service);

            await database.SaveChangesAsync();
            return mapper.Map<ServiceDto>(service);
        }

        [HttpPut("{serviceId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task UpdateService(
            Guid serviceId,
            [FromBody] UpdateServiceDto serviceDto)
        {
            var service = await database.Services
                .Include(s => s.Instancies)
                .ThenInclude(si => si.Event)
                .Include(s => s.Attachments)
                .Include(s => s.Softwares)
                .FirstOrFail(s => s.Id == serviceId);
            mapper.Map(serviceDto, service);
            service.Instancies.ToList().ForEach(si => si.Event.RecurrenceRule = serviceDto.RecurrenceRule);

            if (serviceDto.Attachments != null)
            {
                var attachments = await database.Attachments.Where(a => serviceDto.Attachments.Contains(a.Id)).ToListAsync();
                service.Attachments.Clear();
                attachments.ForEach(service.Attachments.Add);
            }

            if (serviceDto.SoftwareIds != null)
            {
                var softwares = await softwareService.FindSoftwares(serviceDto.SoftwareIds);
                service.Softwares.Clear();
                softwares.ToList().ForEach(service.Softwares.Add);
            }

            await database.SaveChangesAsync();
        }

        [HttpDelete("{serviceId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task DeleteService(Guid serviceId)
        {
            var isTaskExist = await database.Services.Where(x => x.Id == serviceId)
                .AnyAsync(x => x.Tasks.Any());
            Assert.False(isTaskExist);
            var service = await database.Services
                .Include(s => s.Softwares)
                .FirstOrDefaultAsync(s => s.Id == serviceId);
            var serviceInstances = await database.ServiceInstances
                .Include(si => si.Attachments)
                .Include(si => si.Event)
                .Include(si => si.Softwares)
                .Include(si => si.Tasks)
                .IgnoreQueryFilters()
                .Where(si => si.Template.Id == serviceId)
                .ToListAsync();
            foreach (var serviceInstance in serviceInstances)
            {
                await serviceInstanceService.RemoveService(serviceInstance);
            }

            database.RemoveRange(service.Softwares);
            database.Remove(service);

            await database.SaveChangesAsync();
        }

        [HttpGet("{serviceId}/clients")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IEnumerable<ClientShortDto>> GetServiceClients(Guid serviceId)
        {
            var clients = await database.Clients
                .Where(c => c.Services.Any(si => si.Template.Id == serviceId))
                .ToListAsync();

            return clients.Select(mapper.Map<ClientShortDto>);
        }

        [HttpDelete("{serviceId}/tasks/{taskId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task DeleteTask(Guid serviceId, Guid taskId)
        {
            var service = await database.Services
                .Include(x => x.Tasks.Where(x => x.Id == taskId))
                .FirstOrFail(x => x.Id == serviceId);

            var task = service.Tasks.FirstOrFail(x => x.Id == taskId);
            await tasksService.RemoveTaskFromService(task, service);

            await database.SaveChangesAsync();
        }

        [HttpGet("{serviceId}/tasks")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<IEnumerable<TaskDto>> GetTasks(
            Guid serviceId,
            [FromQuery] TaskFilterDto filter)
        {
            var query = database.Services
                .Include(s => s.Tasks)
                .Where(s => s.Id == serviceId);

            return await query
                .SelectMany(s => s.Tasks)
                .Skip(filter.Offset)
                .Take(filter.Limit)
                .ProjectTo<TaskDto>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        [HttpPost("{serviceId}/tasks")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public async Task<TaskDto> PostTask(
            Guid serviceId,
            [FromBody] Guid taskId)
        {
            var service = await database.Services.FindOrFailAsync(serviceId);

            var serviceTask = await database.Tasks
                .Include(t => t.Children)
                .FirstOrFail(t => t.Id == taskId);
            var serviceInstances = await database.ServiceInstances
                .Include(si => si.Client.Calendar)
                .Where(c => c.Template.Id == serviceId)
                .ToListAsync();
            service.Tasks.Add(serviceTask);

            foreach (var serviceInstance in serviceInstances)
            {
                var taskInstance = await tasksService.CreateInstance(serviceTask, serviceInstance);
                serviceInstance.Tasks.Add(taskInstance);
            }

            await database.SaveChangesAsync();

            return mapper.Map<TaskDto>(serviceTask);
        }

        [HttpPut("archive/{serviceId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task ArchiveService(
            Guid serviceId)
        {
            var isNotDoneTasksExist = await database.ServiceInstances
                .Where(x => x.Template.Id == serviceId)
                .SelectMany(si => si.Tasks)
                .SelectMany(ti => ti.TaskEvents)
                .AnyAsync(te => te.State.Id != ITaskStateService.DoneStateId);
            if (isNotDoneTasksExist)
            {
                throw new ClientErrorException("There are still clients associated with this template. Remove this template from the clients first.");
            }

            var service = await database.Services
                .FirstOrFail(s => s.Id == serviceId);
            service.ArchiveInfo = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId);

            await database.SaveChangesAsync();
        }

        [HttpDelete("archive/{serviceId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        public async Task ReactiveService(Guid serviceId)
        {
            var data = await database.Services
                .Include(c => c.ArchiveInfo)
                .IgnoreQueryFilters()
                .FirstOrFail(s => s.Id == serviceId
                    && s.ArchiveInfo != null);

            database.Remove(data.ArchiveInfo);
            data.ArchiveInfo = null;

            await database.SaveChangesAsync();
        }

        [HttpGet("archive/count")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ArchiveCountDto> GetArchiveCount()
        {
            var query = database.Services
                .IgnoreQueryFilters()
                .Where(s => s.ArchiveInfo != null);
            var count = await query.CountAsync();
            return new ArchiveCountDto { Count = count };
        }

        [HttpGet("archive")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<ArchivedServiceDto>> GetArchiveServices([FromQuery] ServiceFilterDto filterDto)
        {
            var query = database.Services
                .Include(s => s.ServiceType)
                .Include(s => s.Tasks)
                .IgnoreQueryFilters()
                .Where(s => s.ArchiveInfo != null)
                .Skip(filterDto.Offset)
                .Take(filterDto.Limit);
            var data = await query.ToListAsync();
            return data.Select(mapper.Map<ArchivedServiceDto>);
        }
    }
}
