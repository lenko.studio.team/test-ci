﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Attributes;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class BlockersController : BaseController
    {
        private readonly JfdiDbContext database;

        private readonly IMapper mapper;

        private readonly ICurrentUserProvider currentUser;

        private readonly IBlockerService blockerService;

        public BlockersController(
            JfdiDbContext database,
            IMapper mapper,
            ICurrentUserProvider currentUser,
            IBlockerService blockerService)
        {
            this.database = database;
            this.mapper = mapper;
            this.currentUser = currentUser;
            this.blockerService = blockerService;
        }

        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public async Task<BlockerDto> PostBlocker(CreateBlockerDto model)
        {
            var taskInstance = await database.TaskEvents
                .Include(te => te.Instance.Event.Calendar)
                .Include(te => te.Instance.Service!.Client.TeamMemberAssignments)
                .FirstOrFail(te => te.Id == model.TaskId);
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                model.Title,
                model.StartDate,
                model.EndDate)
            {
                Calendar = taskInstance.Instance.Event.Calendar
            };

            var blocker = new Blocker(
                model.Title,
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId)
            {
                CalendarEvent = calendarEvent,
                Task = taskInstance,
                IsHighPriority = model.IsHighPriority,
                Description = model.Description
            };

            var serviceInstance = taskInstance.Instance.Service ?? throw new ClientErrorException("Task instance must have service");

            var client = serviceInstance.Client;
            if (model.ServiceId != null)
            {
                blocker.Service = await database.ServiceInstances
                    .Include(s => s.Client)
                    .FirstOrFail(si => si.Client == client && si.Id == model.ServiceId);
            }
            else
            {
                blocker.Service = serviceInstance;
            }

            if (model.AssignedUserId != null)
            {
                var user = client.TeamMemberAssignments.FirstOrFail(tm => tm.Id == model.AssignedUserId);
                blocker.AssignedUser = user;
            }

            blocker = await blockerService.AddBlocker(taskInstance, blocker);

            return mapper.Map<BlockerDto>(blocker);
        }

        [HttpPut("{blockerId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task UpdateBlocker(
            Guid blockerId,
            [FromBody] UpdateBlockerDto taskDto)
        {
            var blocker = await database.Blockers
                .Include(b => b.Service!.Client)
                .FirstOrFail(x => x.Id == blockerId);

            blocker.Title = taskDto.Title;
            blocker.Description = taskDto.Description;
            blocker.IsHighPriority = taskDto.IsHighPriority;

            if (taskDto.AssignedUserId != null)
            {
                var client = blocker.Service?.Client ?? throw new ClientErrorException("Blocker is missing service");
                var user = client.TeamMemberAssignments.FirstOrFail(tm => tm.Id == taskDto.AssignedUserId);
                blocker.AssignedUser = user;
            }
            else
            {
                blocker.AssignedUser = null;
            }

            await database.SaveChangesAsync();
        }

        [HttpGet("{blockerId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<BlockerDto> GetBlocker(Guid blockerId)
        {
            var query = database.Blockers
                .Include(b => b.Task)
                .Include(b => b.CalendarEvent)
                .Include(b => b.Service)
                .Include(b => b.AssignedUser)
                .AsQueryable();

            var task = await query.FirstOrFail(t => t.Id == blockerId);

            return mapper.Map<BlockerDto>(task);
        }

        [HttpDelete("{blockerId}/tasks/{taskId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task DeleteBlocker(Guid blockerId, Guid taskId)
        {
            var task = await database.TaskEvents
                .Include(ti => ti.State)
                .FirstOrFail(ti => ti.Id == taskId);

            await blockerService.DeleteBlocker(task, blockerId);
        }
    }
}
