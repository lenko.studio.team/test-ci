﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Jfdi.ProjectManagement.Api.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TaskStatesController : BaseController
    {
        private readonly JfdiDbContext database;

        private readonly IMapper mapper;

        public TaskStatesController(JfdiDbContext database, IMapper mapper)
        {
            this.database = database;
            this.mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public Task<List<TaskStateDto>> GetTaskStates()
        {
            return database.TaskStates
                .ProjectTo<TaskStateDto>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
