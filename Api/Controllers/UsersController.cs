﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Notifications;
using Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances;
using Jfdi.ProjectManagement.Api.Dtos.Users;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Jfdi.ProjectManagement.Api.Models.Users;
using Jfdi.ProjectManagement.Api.Services;
using Jfdi.ProjectManagement.Api.Services.Notifications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class UsersController : BaseController
    {
        private readonly IUserService userService;
        private readonly JfdiDbContext database;
        private readonly IMapper mapper;
        private readonly INotificationsService notificationsService;

        public UsersController(
            IUserService userService,
            JfdiDbContext database,
            IMapper mapper,
            INotificationsService notificationsService)
        {
            this.userService = userService;
            this.database = database;
            this.mapper = mapper;
            this.notificationsService = notificationsService;
        }

        [HttpDelete("{teamMemberId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task DeleteTeamMember(Guid teamMemberId)
        {
            var teamMember = await database.TeamMembers
                .Include(tm => tm.User)
                .FirstOrFail(tm => tm.Id == teamMemberId);
            database.TeamMembers.Remove(teamMember);
            database.Users.Remove(teamMember.User);
            await userService.DeleteUser(teamMember.User.Id);
            await database.SaveChangesAsync();
        }

        /// <summary>
        /// Create new team member.
        /// </summary>
        /// <remarks>
        /// Role can be : Staff; Manager; Executive.
        /// </remarks>
        /// <param name="newTeamMember">New team member.</param>
        /// <returns>Team member.</returns>
        [HttpPost]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<TeamMemberDto> PostTeamMember(
            CreateTeamMemberDto newTeamMember,
#pragma warning disable SA1611 // Element parameters should be documented
            [FromServices] ICurrentUserProvider currentUser)
#pragma warning restore SA1611 // Element parameters should be documented
        {
            if (currentUser.User.Permission == UserPermissions.Admin
                && newTeamMember.Permission == UserPermissions.Owner)
            {
                throw new AccessException();
            }

            var hasUserWithSameEmail = await database.TeamMembers
                .AnyAsync(tm => tm.User.Email == newTeamMember.Email);
            if (hasUserWithSameEmail)
            {
                throw new ClientErrorException("User exists with same email");
            }

            var teamMember = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                currentUser.CurrentUserId,
                newTeamMember.LastName,
                newTeamMember.FirstName)
            {
                Phone = newTeamMember.Phone
            };
            var keycloakUser = await userService.CreateUser(teamMember.Id, newTeamMember);
            try
            {
                teamMember.User = keycloakUser;
                mapper.Map(newTeamMember, teamMember);
                database.TeamMembers.Add(teamMember);
                notificationsService.AddNotifications(teamMember);

                await database.SaveChangesAsync();
                return mapper.Map<TeamMemberDto>(teamMember);
            }
            catch (Exception ex)
            {
                await userService.DeleteUser(keycloakUser.Id);
                throw new ClientErrorException(ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet("{teamMemberId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<TeamMemberDto> GetTeamMember(Guid teamMemberId)
        {
            var teamMember = await database.TeamMembers
                .Include(tm => tm.Clients)
                .ThenInclude(c => c.Services)
                .ThenInclude(si => si.Template)
                .ThenInclude(si => si.ServiceType)
                .FirstOrFail(x => x.Id == teamMemberId);

            return mapper.Map<TeamMemberDto>(teamMember);
        }

        [HttpGet("keycloak/{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<TeamMemberDto> GetTeamMemberByUser(Guid userId)
        {
            var teamMember = await database.TeamMembers
                .Include(tm => tm.User)
                .Include(tm => tm.Clients)
                .ThenInclude(c => c.Services)
                .ThenInclude(si => si.Template)
                .ThenInclude(si => si.ServiceType)
                .FirstOrFail(x => x.User.Id == userId);

            return mapper.Map<TeamMemberDto>(teamMember);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<PaginationDto<TeamMemberDto>> GetTeamMembers([FromQuery] TeamMemberFilterDto filterDto)
        {
            var query = database.TeamMembers
                .Include(tm => tm.User)
                .Include(tm => tm.Clients)
                .ThenInclude(c => c.Services)
                .ThenInclude(si => si.Template)
                .ThenInclude(si => si.ServiceType)
                .AsNoTracking();

            if (filterDto.EmailConfirmed.HasValue)
            {
                query = query.Where(t => t.EmailConfirmed == filterDto.EmailConfirmed.Value);
            }

            if (filterDto.Name != null)
            {
                query = query.Where(s => EF.Functions.Like(s.FirstName.ToLower(), $"%{filterDto.Name.ToLower()}%") ||
                        EF.Functions.Like(s.LastName.ToLower(), $"%{filterDto.Name.ToLower()}%"));
            }

            if (filterDto.Permissions.HasValue)
            {
                query = query.Where(t => t.User.Permission == filterDto.Permissions.Value);
            }

            if (filterDto.Order != null)
            {
                query = OrderQuery(query, filterDto.Order);
            }

            var totalCount = await query.CountAsync();
            var data = await query
                .Skip(filterDto.Offset)
                .Take(filterDto.Limit)
                .ProjectTo<TeamMemberDto>(mapper.ConfigurationProvider)
                .ToListAsync();

            return new PaginationDto<TeamMemberDto>
            {
                Data = data,
                TotalCount = totalCount
            };
        }

        [HttpPut("{teamMemberId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task UpdateUser(Guid teamMemberId, [FromBody] UpdateTeamMemberDto teamMemberDto)
        {
            var member = await database.TeamMembers.FindOrFailAsync(teamMemberId);
            mapper.Map(teamMemberDto, member);

            if (teamMemberDto.SupervisorId.HasValue)
            {
                var supervisor = await database.TeamMembers.FindOrFailAsync(teamMemberDto.SupervisorId);
                member.Supervisor = supervisor;
            }
            else
            {
                member.Supervisor = null;
            }

            await database.SaveChangesAsync();
        }

        /// <summary>
        /// Confirm user.
        /// </summary>
        /// <param name="teamMemberId">Team members id.</param>
        /// <param name="authTeamMemberDto">Data for confirming.</param>
        /// <remarks>
        /// Password requirements: Password rules of 18+ characters, with at least one capital letter, one low letter, one special symbol (@$!%*?&) and one number.
        /// </remarks>
        /// <returns>Task.</returns>
        [AllowAnonymous]
        [HttpPut("{teamMemberId}/confirm")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task ConfirmUser(Guid teamMemberId, [FromBody]AuthTeamMemberDto authTeamMemberDto)
        {
            var member = await database.TeamMembers
                .Include(tm => tm.User)
                .FirstOrFail(tm => tm.Id == teamMemberId);

            if (member.EmailConfirmed)
            {
                throw new ClientErrorException("Member already confirmed");
            }

            member.EmailConfirmed = true;
            await userService.UpdateUserCredentials(member.User.Id, authTeamMemberDto);

            await database.SaveChangesAsync();
        }

        [HttpPut("{teamMemberId}/reset-password")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task ResetPassword(Guid teamMemberId, ChangePasswordDto changePasswordDto)
        {
            var teamMember = await database.TeamMembers
                .Include(tm => tm.User)
                .FirstOrFail(tm => tm.Id == teamMemberId);
            await userService.ChangePassword(teamMember.User, changePasswordDto);
        }

        [HttpGet("archive/count")]
        public async Task<ArchiveCountDto> GetArchiveCount()
        {
            var query = database.TeamMembers
                .IgnoreQueryFilters()
                .Where(c => c.ArchiveInfo != null);
            var count = await query.CountAsync();
            return new ArchiveCountDto { Count = count };
        }

        [HttpPut("archive/{teamMemberId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        public async Task ArchiveTeamMember(
            Guid teamMemberId,
#pragma warning disable SA1611 // Element parameters should be documented
            [FromServices] ICurrentUserProvider currentUser)
#pragma warning restore SA1611 // Element parameters should be documented
        {
            var teamMember = await database.TeamMembers
                .Include(tm => tm.User)
                .FirstOrFail(tm => tm.Id == teamMemberId);
            var permission = await userService.GetPermission(teamMember.User.Id);
            if (currentUser.User.Permission == UserPermissions.Admin
                    && permission == UserPermissions.Owner)
            {
                throw new AccessException();
            }

            teamMember.ArchiveInfo = new ArchiveData(
                            Guid.NewGuid(),
                            DateTime.UtcNow,
                            currentUser.CurrentUserId);

            await database.SaveChangesAsync();
        }

        [HttpDelete("archive/{teamMemberId}")]
        [Authorize(Roles = UserPermission.OwnerAdmin)]
        public async Task ReactiveTeamMember(Guid teamMemberId)
        {
            var data = await database.TeamMembers
                .Include(c => c.ArchiveInfo)
                .IgnoreQueryFilters()
                .FirstOrFail(tm => tm.Id == teamMemberId
                    && tm.ArchiveInfo != null);
            database.Remove(data.ArchiveInfo);
            data.ArchiveInfo = null;

            await database.SaveChangesAsync();
        }

        [HttpGet("archive")]
        public async Task<IEnumerable<ArchiveTeamMemberDto>> GetArchiveTasks([FromQuery] TeamMemberFilterDto filterDto)
        {
            var query = database.TeamMembers
                .Include(tm => tm.User)
                .Include(tm => tm.Clients)
                .ThenInclude(c => c.Services)
                .ThenInclude(si => si.Template)
                .ThenInclude(si => si.ServiceType)
                .IgnoreQueryFilters()
                .Where(tm => tm.ArchiveInfo != null)
                .AsNoTracking();

            if (filterDto.EmailConfirmed.HasValue)
            {
                query = query.Where(t => t.EmailConfirmed == filterDto.EmailConfirmed.Value);
            }

            if (filterDto.Name != null)
            {
                query = query.Where(s => EF.Functions.Like(s.FirstName.ToLower(), $"%{filterDto.Name.ToLower()}%") ||
                        EF.Functions.Like(s.LastName.ToLower(), $"%{filterDto.Name.ToLower()}%"));
            }

            if (filterDto.Permissions.HasValue)
            {
                query = query.Where(t => t.User.Permission == filterDto.Permissions.Value);
            }

            if (filterDto.Order != null)
            {
                query = OrderQuery(query, filterDto.Order);
            }

            return await query
                .Skip(filterDto.Offset)
                .Take(filterDto.Limit)
                .ProjectTo<ArchiveTeamMemberDto>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        [HttpPut("{teamMemberId}/notifications")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task SetNotificationSetting(Guid teamMemberId, NotificationOptionDto notificationOption)
        {
            var option = await database.NotificationOptions
                .FirstOrFail(x => x.NotificationType == notificationOption.NotificationType
                    && x.TeamMember.Id == teamMemberId);
            option.IsOn = notificationOption.IsOn;
            await database.SaveChangesAsync();
        }

        [HttpGet("{teamMemberId}/notifications")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IEnumerable<NotificationOptionDto>> GetNotificationsOptions(Guid teamMemberId)
        {
            return await database.NotificationOptions
                .Where(x => x.TeamMember.Id == teamMemberId)
                .ProjectTo<NotificationOptionDto>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        private static IQueryable<TeamMember> OrderQuery(IQueryable<TeamMember> query, OrderTeamMemberDto orderDto)
        {
            switch (orderDto.Field)
            {
                case OrderTeamMemberDto.OrderTeamMemberField.Name:
                    return orderDto.Desc
                        ? query.OrderByDescending(s => s.FirstName.ToLower()).OrderByDescending(s => s.LastName.ToLower())
                        : query.OrderBy(s => s.FirstName.ToLower()).OrderBy(s => s.LastName.ToLower());

                case OrderTeamMemberDto.OrderTeamMemberField.ClientCount:
                    return orderDto.Desc
                        ? query.OrderByDescending(s => s.Clients.Count)
                        : query.OrderBy(s => s.Clients.Count);

                case OrderTeamMemberDto.OrderTeamMemberField.Permission:
                    return orderDto.Desc
                        ? query.OrderByDescending(s => s.User.Permission)
                        : query.OrderBy(s => s.User.Permission);

                case OrderTeamMemberDto.OrderTeamMemberField.Role:
                    return orderDto.Desc
                        ? query.OrderByDescending(s => s.Role)
                        : query.OrderBy(s => s.Role);
            }

            throw new NotImplementedException();
        }
    }
}
