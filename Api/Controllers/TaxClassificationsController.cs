﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Jfdi.ProjectManagement.Api.Attributes;
using Jfdi.ProjectManagement.Api.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class TaxClassificationsController : BaseController
    {
        private readonly JfdiDbContext database;

        private readonly IMapper mapper;

        public TaxClassificationsController(JfdiDbContext database, IMapper mapper)
        {
            this.database = database;
            this.mapper = mapper;
        }

        [HttpGet]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public Task<List<TaxClassificationDto>> GetTaxes()
        {
            return database.TaxClassifications
                .ProjectTo<TaxClassificationDto>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
