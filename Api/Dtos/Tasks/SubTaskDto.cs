﻿using System;
using Jfdi.ProjectManagement.Api.Models;

namespace Jfdi.ProjectManagement.Api.Dtos.Tasks
{
    public class SubTaskDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; } = null!;

        public string? RecurrenceRule { get; set; }

        public DateTime StartUtc { get; set; }

        public DateTime EndUtc { get; set; }
    }
}
