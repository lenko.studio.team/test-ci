﻿using System;
using System.Collections.Generic;
using Jfdi.ProjectManagement.Api.Dtos.Users;

namespace Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances
{
    public class TaskEventShortDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; } = null!;

        public Guid ClientId { get; set; }

        public ServiceShortDto Service { get; set; } = null!;

        public bool IsHighPriority { get; set; }

        public DateTime? EndDateUtc { get; set; }

        public IEnumerable<ShortTeamMemberDto> AssignedTo { get; set; } = new List<ShortTeamMemberDto>();

        public IEnumerable<ShortTeamMemberDto> Supervisors { get; set; } = new List<ShortTeamMemberDto>();

        public TaskStateDto State { get; set; } = null!;
    }
}
