﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class ClientTaskInstanceFilterDto
    {
        public bool IncludeBlockers { get; set; } = false;

        public bool IncludeSubTasks { get; set; } = false;

        public Guid? StateId { get; set; }

        public Guid? AssignedId { get; set; }

        public Guid? SupervisorId { get; set; }

        public Guid? ClientId { get; set; }

        public bool? IsHighPriority { get; set; }

        public DateTime? SearchStartUtc { get; set; }

        public DateTime? SearchEndUtc { get; set; }

        public OrderTypeDto? OrderType { get; set; }
    }
}
