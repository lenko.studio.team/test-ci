﻿using System;
using System.Collections.Generic;
using Jfdi.ProjectManagement.Api.Dtos.Clients;
using Jfdi.ProjectManagement.Api.Dtos.TaskInstancies;
using Jfdi.ProjectManagement.Api.Dtos.Users;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class TaskEventDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; } = null!;

        public ClientShortDto Client { get; set; } = null!;

        public ServiceShortDto Service { get; set; } = null!;

        public bool IsHighPriority { get; set; }

        public Guid InstanceId { get; set; }

        public DateTime StartDateUtc { get; set; }

        public DateTime EndDateUtc { get; set; }

        public SoftwareDto? Software { get; set; }

        public IEnumerable<string>? Links { get; set; }

        public IEnumerable<BlockerDto> Blockers { get; set; } = new List<BlockerDto>();

        public IEnumerable<ShortTeamMemberDto> AssignedTo { get; set; } = new List<ShortTeamMemberDto>();

        public IEnumerable<ShortTeamMemberDto> Supervisors { get; set; } = new List<ShortTeamMemberDto>();

        public IEnumerable<SubTaskEventDto> SubTasks { get; set; } = new List<SubTaskEventDto>();

        public string? RecurrenceRule { get; set; }

        public string? Info { get; set; }

        public bool ForReview { get; set; }

        public TaskStateDto State { get; set; } = null!;

        public bool IsStored { get; set; } = true;
    }
}
