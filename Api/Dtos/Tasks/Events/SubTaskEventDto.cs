﻿using System;
using System.Collections.Generic;

namespace Jfdi.ProjectManagement.Api.Dtos.TaskInstancies
{
    public class SubTaskEventDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; } = null!;

        public bool IsHighPriority { get; set; }

        public DateTime StartDateUtc { get; set; }

        public DateTime EndDateUtc { get; set; }

        public SoftwareDto? Software { get; set; }

        public IEnumerable<string>? Links { get; set; }

        public IEnumerable<BlockerDto> Blockers { get; set; } = new List<BlockerDto>();

        public TaskStateDto State { get; set; } = null!;
    }
}
