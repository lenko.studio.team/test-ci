﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class UpdateTaskParentDto
    {
        [Required]
        public Guid ParentId { get; set; }
    }
}
