﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jfdi.ProjectManagement.Api.Attributes;
using Jfdi.ProjectManagement.Api.Dtos.Tasks;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class CreateTaskDto
    {
        [StringLength(255)]
        public string Title { get; set; } = null!;

        [Recurrence]
        public string? RecurrenceRule { get; set; }

        [StringLength(255)]
        public string? Info { get; set; }

        [Required]
        public DateTime StartUtc { get; set; }

        public IEnumerable<CreateSubTaskDto> Children { get; set; } = new List<CreateSubTaskDto>();
    }
}
