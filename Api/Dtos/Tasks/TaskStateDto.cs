﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class TaskStateDto
    {
        public TaskStateDto(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        [Required]
        public Guid Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Title { get; set; }
    }
}
