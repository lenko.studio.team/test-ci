﻿using System;
using System.Collections.Generic;
using Jfdi.ProjectManagement.Api.Dtos.Tasks;
using Jfdi.ProjectManagement.Api.Models;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class TaskDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; } = null!;

        public string? RecurrenceRule { get; set; }

        public string? Info { get; set; }

        public DateTime StartUtc { get; set; }

        public DateTime EndUtc { get; set; }

        public IEnumerable<SubTaskDto>? Children { get; set; }
    }
}
