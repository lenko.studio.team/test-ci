﻿using System;
using System.ComponentModel.DataAnnotations;
using Jfdi.ProjectManagement.Api.Attributes;

namespace Jfdi.ProjectManagement.Api.Dtos.Tasks
{
    public class CreateSubTaskDto
    {
        [StringLength(255)]
        public string Title { get; set; } = null!;

        [Recurrence]
        public string? RecurrenceRule { get; set; }

        [Required]
        public DateTime StartUtc { get; set; }
    }
}
