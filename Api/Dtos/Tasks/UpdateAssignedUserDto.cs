﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos.Tasks
{
    public class UpdateAssignedUserDto
    {
        public Guid? AssignedUserId { get; set; }
    }
}
