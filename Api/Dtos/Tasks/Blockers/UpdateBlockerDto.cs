﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class UpdateBlockerDto
    {
        public UpdateBlockerDto(string title)
        {
            Title = title;
        }

        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        public string? Description { get; set; }

        public bool IsHighPriority { get; set; }

        public Guid? ServiceId { get; set; }

        public Guid? AssignedUserId { get; set; }
    }
}
