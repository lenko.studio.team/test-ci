﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class CreateBlockerDto
    {
        public CreateBlockerDto(string title)
        {
            Title = title;
        }

        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        [Required]
        public Guid TaskId { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        public string? Description { get; set; }

        public bool IsHighPriority { get; set; }

        public Guid? ServiceId { get; set; }

        public Guid? AssignedUserId { get; set; }
    }
}
