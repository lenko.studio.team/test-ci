﻿using System;
using System.ComponentModel.DataAnnotations;
using Jfdi.ProjectManagement.Api.Dtos.Users;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class BlockerDto
    {
        public BlockerDto(string title)
        {
            Title = title;
        }

        [Required]
        public Guid Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        public string? Description { get; set; }

        [Required]
        public Guid TaskId { get; set; }

        [Required]
        public Guid CalendarEventId { get; set; }

        public DateTime StartDateUtc { get; set; }

        public DateTime EndDateUtc { get; set; }

        public bool IsHighPriority { get; set; }

        public bool IsDone { get; set; } = false;

        public ServiceShortDto Service { get; set; } = null!;

        public ShortTeamMemberDto? AssignedUser { get; set; }
    }
}
