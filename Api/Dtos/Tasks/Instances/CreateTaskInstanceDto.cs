﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class CreateTaskInstanceDto : CreateTaskDto
    {
        [Required]
        public Guid ServiceId { get; set; }

        public bool ForReview { get; set; }

        public Guid? SoftwareId { get; set; }

        public bool IsHighPriority { get; set; }

        public Guid? ParentId { get; set; }

        public IEnumerable<Guid> AssignedTo { get; set; } = new List<Guid>();

        public IEnumerable<Guid> SupervisorIds { get; set; } = new List<Guid>();

        public IEnumerable<string>? Links { get; set; } = new List<string>();
    }
}
