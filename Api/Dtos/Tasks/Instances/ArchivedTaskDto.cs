﻿using System;
using System.Collections.Generic;
using Jfdi.ProjectManagement.Api.Dtos.Clients;
using Jfdi.ProjectManagement.Api.Dtos.TaskInstancies;

namespace Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances
{
    public class ArchivedTaskDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; } = null!;

        public ClientShortDto Client { get; set; } = null!;

        public ServiceShortDto Service { get; set; } = null!;

        public bool IsHighPriority { get; set; }

        public IEnumerable<SubTaskEventDto> SubTasks { get; set; } = new List<SubTaskEventDto>();

        public DateTime? ArchiveDate { get; set; }
    }
}
