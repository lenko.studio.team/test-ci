﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class UpdateTaskInstanceDto
    {
        [StringLength(255)]
        public string? Title { get; set; }

        [StringLength(255)]
        public string? Info { get; set; }

        public DateTime? StartUtc { get; set; }

        public Guid? ServiceId { get; set; }

        public bool? ForReview { get; set; }

        public Guid? SoftwareId { get; set; }

        public bool? IsHighPriority { get; set; }

        public IEnumerable<string>? Links { get; set; } = new List<string>();
    }
}
