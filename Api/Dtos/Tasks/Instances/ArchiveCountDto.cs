﻿namespace Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances
{
    public class ArchiveCountDto
    {
        public int Count { get; set; }
    }
}
