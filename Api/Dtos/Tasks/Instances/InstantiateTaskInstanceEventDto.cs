﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class InstantiateTaskInstanceEventDto
    {
        [Required]
        /// <summary>Date of occurrence of the task event. Should be after the end of the previous occurrence.</summary>
        public DateTime DateOfOccurrence { get; set; }
    }
}
