﻿using System;
using System.Collections.Generic;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class SoftwareDto
    {
        public SoftwareDto(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string? Description { get; set; }

        public IEnumerable<string>? Links { get; set; }

        public IEnumerable<AttachmentDto> Attachments { get; set; } = new List<AttachmentDto>();
    }
}
