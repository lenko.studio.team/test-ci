﻿using System;
using System.Collections.Generic;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class CreateSoftwareDto
    {
        public CreateSoftwareDto(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        public string? Description { get; set; }

        public IEnumerable<string>? Links { get; set; }

        public IEnumerable<Guid>? AttachmentIds { get; set; }
    }
}
