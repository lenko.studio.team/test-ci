﻿using System;
using Jfdi.ProjectManagement.Api.Models.Notifications;

namespace Jfdi.ProjectManagement.Api.Dtos.Notifications
{
    public class NotificationOptionDto
    {
        public NotificationType NotificationType { get; set; }

        public bool IsOn { get; set; } = true;
    }
}
