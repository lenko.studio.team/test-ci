﻿using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class AuthTeamMemberDto
    {
        public AuthTeamMemberDto(string userName, string password)
        {
            UserName = userName;
            Password = password;
        }

        [Required]
        public string UserName { get; set; }

        [Required]
        [StringLength(32)]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{18,}$")]
        public string Password { get; set; }
    }
}
