﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos.Attachments
{
    public class GDriveAttachmentDto
    {
        public GDriveAttachmentDto(
               Guid id,
               string name,
               string link)
        {
            Id = id;
            Name = name;
            Link = link;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }
    }
}
