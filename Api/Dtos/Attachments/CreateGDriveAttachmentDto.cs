﻿namespace Jfdi.ProjectManagement.Api.Dtos.Attachments
{
    public class CreateGDriveAttachmentDto
    {
        public CreateGDriveAttachmentDto(string name, string link)
        {
            Link = link;
            Name = name;
        }

        public string Name { get; set; }

        public string Link { get; set; }
    }
}
