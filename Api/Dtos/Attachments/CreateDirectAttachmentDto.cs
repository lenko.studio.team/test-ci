﻿using Microsoft.AspNetCore.Http;

namespace Jfdi.ProjectManagement.Api.Dtos.Attachments
{
    public class CreateDirectAttachmentDto
    {
        public IFormFile Data { get; set; } = null!;
    }
}
