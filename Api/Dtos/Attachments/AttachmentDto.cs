﻿using System;
using Jfdi.ProjectManagement.Api.Models;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class AttachmentDto
    {
        public AttachmentDto(
            Guid id,
            string name)
        {
            Id = id;
            Name = name;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public AttachmentType Type { get; set; }
    }
}
