﻿namespace Jfdi.ProjectManagement.Api.Dtos
{
    public abstract class FilterDto
    {
        public int Offset { get; set; }

        public int Limit { get; set; } = 10;
    }
}
