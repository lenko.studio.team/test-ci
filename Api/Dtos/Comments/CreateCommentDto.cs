﻿namespace Jfdi.ProjectManagement.Api.Dtos.Comments
{
    public class CreateCommentDto
    {
        public CreateCommentDto(string content)
        {
            Content = content;
        }

        public string Content { get; set; }
    }
}
