﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos.Comments
{
    public class CommentDto
    {
        public Guid Id { get; set; }

        public Guid TeamMemberId { get; set; }

        public string FirstName { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public DateTime CreateDate { get; set; }

        public string Content { get; set; } = null!;
    }
}
