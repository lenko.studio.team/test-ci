﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class ServiceShortDto
    {
        public ServiceShortDto(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Color { get; set; } = null!;

        public string Icon { get; set; } = null!;

        public string FillIcon { get; set; } = null!;

        public Guid ServiceTypeId { get; set; }
    }
}
