﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class ServiceTypeShortDto
    {
        public ServiceTypeShortDto(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }

        public string? Icon { get; set; }

        public string? FillIcon { get; set; }

        public string? Color { get; set; }
    }
}
