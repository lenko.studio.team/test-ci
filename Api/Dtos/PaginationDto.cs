﻿using System.Collections.Generic;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class PaginationDto<TData>
        where TData : class
    {
        public IEnumerable<TData> Data { get; set; } = new List<TData>();

        public long TotalCount { get; set; }
    }
}
