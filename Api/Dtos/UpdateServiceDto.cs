﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jfdi.ProjectManagement.Api.Attributes;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class UpdateServiceDto
    {
        public UpdateServiceDto(string name)
        {
            Name = name;
        }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Recurrence]
        public string? RecurrenceRule { get; set; }

        public bool IsHighPriority { get; set; } = false;

        [StringLength(255)]
        public string? Description { get; set; }

        public IEnumerable<string>? Links { get; set; }

        public IEnumerable<Guid>? Attachments { get; set; }

        public IEnumerable<Guid>? SoftwareIds { get; set; }
    }
}
