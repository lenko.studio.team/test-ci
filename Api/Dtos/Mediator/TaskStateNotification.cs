﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos.Mediator
{
    public class TaskStateNotification
    {
        public Guid TaskId { get; set; }

        public Guid NewStateId { get; set; }

        public override string ToString()
        {
            return $"Task {TaskId} state changed to {NewStateId}";
        }
    }
}
