﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class TaxClassificationDto
    {
        public TaxClassificationDto(Guid id, string title)
        {
            Id = id;
            Title = title;
        }

        public Guid Id { get; set; }

        public string Title { get; set; }
    }
}
