﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class CreateCalendarEventDto
    {
        public CreateCalendarEventDto(string title, DateTime startUtc, DateTime endUtc)
        {
            Title = title;
            StartUtc = startUtc;
            EndUtc = endUtc;
        }

        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        [Required]
        public DateTime StartUtc { get; set; }

        [Required]
        public DateTime EndUtc { get; set; }

        [StringLength(255)]
        public string? RecurrenceRule { get; set; }
    }
}
