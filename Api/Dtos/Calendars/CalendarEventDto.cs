﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos
{
#pragma warning disable SA1313 // Parameter casing
    public record CalendarEventDto(
        Guid Id,
        string Title,
        Guid CalendarId,
        string? RecurrenceRule,
        DateTime StartUtc,
        DateTime EndUtc);
#pragma warning restore SA1313 // Parameter casing
}
