﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class CalendarFilterDto : FilterDto
    {
        public DateTime? SearchStartUtc { get; set; }

        public DateTime? SearchEndUtc { get; set; }
    }
}
