﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    #pragma warning disable SA1313 // Parameter casing
    public record CalendarEventInstanceDto(DateTime StartUtc, DateTime EndUtc);
    #pragma warning restore SA1313 // Parameter casing
}
