﻿using System.Text.Json.Serialization;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class CredentialDto
    {
        public CredentialDto(string value)
        {
            Value = value;
        }

        [JsonPropertyName("type")]
        public string Type { get; set; } = "password";

        [JsonPropertyName("value")]
        public string Value { get; set; }

        [JsonPropertyName("temporary")]
        public bool Temporary { get; set; } = false;
    }
}
