﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class CreateUserDto
    {
        public CreateUserDto(
            string userName,
            string email)
        {
            UserName = userName;
            Email = email;
        }

        [JsonPropertyName("username")]
        public string UserName { get; set; }

        [JsonPropertyName("emailVerified")]
        public bool EmailVerified { get; set; } = false;

        [JsonPropertyName("enabled")]
        public bool Enabled { get; set; } = true;

        [JsonPropertyName("firstName")]
        public string? FirstName { get; set; }

        [JsonPropertyName("lastName")]
        public string? LastName { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("groups")]
        public IEnumerable<string>? Groups { get; set; }
    }
}
