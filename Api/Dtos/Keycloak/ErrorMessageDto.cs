﻿using System.Text.Json.Serialization;

namespace Jfdi.ProjectManagement.Api.Dtos.Keycloak
{
    public class ErrorMessageDto
    {
        [JsonPropertyName("errorMessage")]
        public string ErrorMessage { get; set; } = null!;
    }
}
