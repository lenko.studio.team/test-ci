﻿using System;
using System.Text.Json.Serialization;

namespace Jfdi.ProjectManagement.Api.Dtos.Keycloak
{
    public class UserGroupDto
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("name")]
        public string? Name { get; set; }
    }
}
