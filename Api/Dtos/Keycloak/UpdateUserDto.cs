﻿using System.Text.Json.Serialization;

namespace Jfdi.ProjectManagement.Api.Dtos.Keycloak
{
    public class UpdateUserDto
    {
        public UpdateUserDto(string userName, string password)
        {
            UserName = userName;
            Credentials = new[] { new CredentialDto(password) };
        }

        [JsonPropertyName("username")]
        public string UserName { get; set; }

        [JsonPropertyName("credentials")]
        public CredentialDto[] Credentials { get; set; }
    }
}
