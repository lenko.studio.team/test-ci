﻿using System;
using System.Collections.Generic;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class ServiceTypeDto
    {
        public ServiceTypeDto(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string? Icon { get; set; }

        public string? FillIcon { get; set; }

        public string? Color { get; set; }

        public IEnumerable<ServiceShortDto> Services { get; set; } = new List<ServiceShortDto>();
    }
}
