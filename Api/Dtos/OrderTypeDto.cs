﻿namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class OrderTypeDto
    {
        public OrderFields Field { get; set; }

        public bool Desc { get; set; }
    }
}
