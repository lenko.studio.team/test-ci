namespace Jfdi.ProjectManagement.Api.Dtos.Clients
{
    public class ClientsFilterDto : FilterDto
    {
        public string? Title { get; set; }

        public bool? WithoutAssignedMembers { get; set; }

        public OrderClientsDto? Order { get; set; }
    }
}
