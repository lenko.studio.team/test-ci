﻿using System;
using System.Collections.Generic;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class ClientDto
    {
        public ClientDto(Guid id, Guid calendarId, string companyName)
        {
            Id = id;
            CalendarId = calendarId;
            CompanyName = companyName;
        }

        public Guid Id { get; set; }

        public string CompanyName { get; set; }

        public Guid CalendarId { get; set; }

        public TaxClassificationDto? TaxClassification { get; set; }

        public string? PhysicalAddress { get; set; }

        public string? MailingAddress { get; set; }

        public string? CompanyTin { get; set; }

        public string? CompanyEin { get; set; }

        public string? StateRegistration { get; set; }

        public string? CityRegistration { get; set; }

        public string? Duns { get; set; }

        public DateTime? DateFormed { get; set; }

        public string? President { get; set; }

        public string? Tresurer { get; set; }

        public string? RegisteredAgent { get; set; }

        public string? Phone { get; set; }

        public string? ClientTag { get; set; }

        public DateTime? ServiceStartDate { get; set; }

        public IEnumerable<TeamMemberDto>? TeamMemberAssignments { get; set; }

        public string? OnePasswordClientAcronoym { get; set; }

        public IEnumerable<string>? ClientJfdiEmails { get; set; }

        public DateTime? YearEndDate { get; set; }

        public DateTime? InitialTransactionProcessingDate { get; set; }

        public IEnumerable<string>? Software { get; set; }

        public IEnumerable<ServiceShortDto> Services { get; set; } = new List<ServiceShortDto>();
    }
}
