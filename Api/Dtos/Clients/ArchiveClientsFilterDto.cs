﻿namespace Jfdi.ProjectManagement.Api.Dtos.Clients
{
    public class ArchiveClientsFilterDto : FilterDto
    {
        public string? Title { get; set; }

        public OrderClientsDto? Order { get; set; }
    }
}
