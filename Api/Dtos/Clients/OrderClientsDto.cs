namespace Jfdi.ProjectManagement.Api.Dtos.Clients
{
    public class OrderClientsDto
    {
        public enum OrderClientField
        {
            /// <summary>Order by company name alphabetical</summary>
            Name,

            /// <summary>Order by service start date</summary>
            StartDate,

            /// <summary>Order by number of services</summary>
            ServiceCount,
        }

        public OrderClientField Field { get; set; }

        public bool Desc { get; set; }
    }
}
