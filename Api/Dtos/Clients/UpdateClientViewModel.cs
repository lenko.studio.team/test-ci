﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class UpdateClientViewModel
    {
        public UpdateClientViewModel(string companyName)
        {
            CompanyName = companyName;
        }

        [Required]
        [StringLength(255)]
        public string? CompanyName { get; set; }

        public Guid? TaxClassificationId { get; set; }

        public string? PhysicalAddress { get; set; }

        public string? MailingAddress { get; set; }

        [RegularExpression("^\\d{2}-\\d{7}$")]
        public string? CompanyEin { get; set; }

        public string? StateRegistration { get; set; }

        public string? CityRegistration { get; set; }

        [RegularExpression("^\\d{9}$")]
        public string? Duns { get; set; }

        public DateTime? DateFormed { get; set; }

        public string? President { get; set; }

        public string? Tresurer { get; set; }

        public string? RegisteredAgent { get; set; }

        public string? Phone { get; set; }

        public string? ClientTag { get; set; }

        public DateTime? ServiceStartDate { get; set; }

        public string? OnePasswordClientAcronoym { get; set; }

        public IEnumerable<string>? ClientJfdiEmails { get; set; }

        public DateTime? YearEndDate { get; set; }

        public DateTime? InitialTransactionProcessingDate { get; set; }

        public IEnumerable<string>? Software { get; set; }
    }
}
