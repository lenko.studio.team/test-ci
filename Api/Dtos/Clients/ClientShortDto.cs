﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jfdi.ProjectManagement.Api.Dtos.Clients
{
    public class ClientShortDto
    {
        public ClientShortDto(
            Guid id,
            string companyName)
        {
            Id = id;
            CompanyName = companyName;
        }

        public Guid Id { get; set; }

        public string CompanyName { get; set; }

        public List<ServiceShortDto> Services { get; set; } = new List<ServiceShortDto>();
    }
}
