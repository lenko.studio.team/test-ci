﻿using System;
using System.Collections.Generic;

namespace Jfdi.ProjectManagement.Api.Dtos.Clients
{
    public class ArchiveClientDto
    {
        public Guid Id { get; set; }

        public string CompanyName { get; set; } = null!;

        public string? Tresurer { get; set; }

        public string? MailingAddress { get; set; }

        public string? Phone { get; set; }

        public IEnumerable<ServiceShortDto> Services { get; set; } = new List<ServiceShortDto>();

        public DateTime? ArchiveDate { get; set; }
    }
}
