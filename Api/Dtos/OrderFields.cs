﻿namespace Jfdi.ProjectManagement.Api.Dtos
{
    public enum OrderFields
    {
        /// <summary>
        /// Name.
        /// </summary>
        Name,

        /// <summary>
        /// Newly.
        /// </summary>
        Newly,

        /// <summary>
        /// Most clients assigned.
        /// </summary>
        MostClient,

        /// <summary>
        /// Start date
        /// </summary>
        StartDate,

        /// <summary>
        /// End date
        /// </summary>
        EndDate,
    }
}
