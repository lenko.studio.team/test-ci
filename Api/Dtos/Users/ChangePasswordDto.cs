﻿using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos.Users
{
    public class ChangePasswordDto
    {
        [Required]
        [StringLength(32)]
        public string OldPassword { get; set; } = null!;

        [Required]
        [StringLength(32)]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{18,}$")]
        public string NewPassword { get; set; } = null!;
    }
}
