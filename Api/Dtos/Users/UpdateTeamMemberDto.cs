﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos.Users
{
    public class UpdateTeamMemberDto
    {
        [StringLength(255)]
        public string LastName { get; set; } = null!;

        [StringLength(255)]
        public string FirstName { get; set; } = null!;

        [StringLength(255)]
        public string? PhysicalAddress { get; set; }

        public int? SocialSecurityNumber { get; set; }

        public DateTime? BirthDate { get; set; }

        public string? CompanyMailingAddress { get; set; }

        [StringLength(255)]
        public string? CellPhone { get; set; }

        [StringLength(255)]
        public string? FullName { get; set; }

        [StringLength(255)]
        public string? Address { get; set; }

        [StringLength(255)]
        public string? Phone { get; set; }

        public Guid? SupervisorId { get; set; }

        [StringLength(255)]
        public string? Salary { get; set; }

        public DateTime? StartDate { get; set; }

        [StringLength(255)]
        public string? NameOfCollege { get; set; }

        [StringLength(255)]
        public string? Degree { get; set; }
    }
}
