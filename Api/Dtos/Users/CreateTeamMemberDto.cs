﻿using System.ComponentModel.DataAnnotations;
using Jfdi.ProjectManagement.Api.Models.Users;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class CreateTeamMemberDto
    {
        public CreateTeamMemberDto(string lastName, string firstName, string email, string role)
        {
            LastName = lastName;
            FirstName = firstName;
            Email = email;
            Role = role;
        }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
#if DEBUG
#else
        [RegularExpression("^[A-Za-z0-9._%+-]+@jfdiaccountants.com$|")]
#endif
        public string Email { get; set; }

        [StringLength(255)]
        public string Role { get; set; }

        [StringLength(255)]
        public string Phone { get; set; } = null!;

        public UserPermissions Permission { get; set; }
    }
}
