﻿using System;
using System.Collections.Generic;
using Jfdi.ProjectManagement.Api.Dtos.Clients;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Users;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class TeamMemberDto
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string LastName { get; set; } = null!;

        public string FirstName { get; set; } = null!;

        public string Email { get; set; } = null!;

        public string? PhysicalAddress { get; set; }

        public int? SocialSecurityNumber { get; set; }

        public DateTime? BirthDate { get; set; }

        public string? CompanyMailingAddress { get; set; }

        public string? CellPhone { get; set; }

        public string? FullName { get; set; }

        public string? Address { get; set; }

        public string? Phone { get; set; }

        public string? Title { get; set; }

        public Guid? SupervisorId { get; set; }

        public string? Salary { get; set; }

        public DateTime? StartDate { get; set; }

        public string? NameOfCollege { get; set; }

        public string? Degree { get; set; }

        public UserPermissions Permission { get; set; }

        public TeamMemberRole Role { get; set; }

        public bool EmailConfirmed { get; set; } = false;

        public List<ClientShortDto> Clients { get; set; } = new List<ClientShortDto>();
    }
}
