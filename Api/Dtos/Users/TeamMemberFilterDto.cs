﻿using Jfdi.ProjectManagement.Api.Models.Users;

namespace Jfdi.ProjectManagement.Api.Dtos.Users
{
    public class TeamMemberFilterDto : FilterDto
    {
        public bool? EmailConfirmed { get; set; }

        public UserPermissions? Permissions { get; set; }

        public string? Name { get; set; }

        public OrderTeamMemberDto? Order { get; set; }
    }
}
