﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos.Users
{
    public class ShortTeamMemberDto
    {
        public ShortTeamMemberDto(string lastName, string firstName)
        {
            LastName = lastName;
            FirstName = firstName;
        }

        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string Email { get; set; } = null!;
    }
}
