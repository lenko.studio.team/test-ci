﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class UserFilterDto : FilterDto
    {
        public string? UserName { get; set; }
    }
}
