﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos.Users
{
    public class ArchiveTeamMemberDto
    {
        public Guid Id { get; set; }

        public string LastName { get; set; } = null!;

        public string FirstName { get; set; } = null!;

        public string Email { get; set; } = null!;

        public string? CellPhone { get; set; }

        public string? FullName { get; set; }

        public string? Phone { get; set; }

        public string? Role { get; set; }

        public int ClientsCount { get; set; }

        public DateTime? ArchiveDate { get; set; }
    }
}
