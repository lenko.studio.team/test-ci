namespace Jfdi.ProjectManagement.Api.Dtos.Users
{
    public class OrderTeamMemberDto
    {
        public enum OrderTeamMemberField
        {
            /// <summary>Order by role</summary>
            Role,

            /// <summary>Order by permission</summary>
            Permission,

            /// <summary>Order by number of assigned clients</summary>
            ClientCount,

            /// <summary>Order by name alphabetical</summary>
            Name,
        }

        public OrderTeamMemberField Field { get; set; }

        public bool Desc { get; set; }
    }
}
