﻿using System;
using System.Collections.Generic;
using Jfdi.ProjectManagement.Api.Models;
using Microsoft.AspNetCore.Http;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class ServiceDto
    {
        public ServiceDto(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string? Icon { get; set; }

        public string? FillIcon { get; set; }

        public string? Color { get; set; }

        public ServiceTypeDto ServiceType { get; set; } = null!;

        public string? RecurrenceRule { get; set; }

        public DateTime StartDateUtc { get; set; }

        public DateTime EndDateUtc { get; set; }

        public bool IsHighPriority { get; set; } = false;

        public string? Description { get; set; }

        public IEnumerable<TaskDto>? Tasks { get; set; }

        public IEnumerable<string>? Links { get; set; }

        public IEnumerable<AttachmentDto>? Attachments { get; set; }

        public IEnumerable<SoftwareDto>? Softwares { get; set; }

        public long ClientsCount { get; set; }
    }
}
