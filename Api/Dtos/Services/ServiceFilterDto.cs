﻿using System;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class ServiceFilterDto : FilterDto
    {
        public string? Title { get; set; }

        public Guid[] ServiceTypeIds { get; set; } = new Guid[] { };

        public OrderTypeDto? OrderType { get; set; }
    }
}
