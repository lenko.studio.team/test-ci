﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jfdi.ProjectManagement.Api.Attributes;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class CreateServiceDto
    {
        public CreateServiceDto(string name)
        {
            Name = name;
        }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        public Guid ServiceTypeId { get; set; }

        [Recurrence]
        public string? RecurrenceRule { get; set; }

        public bool IsHighPriority { get; set; } = false;

        [StringLength(255)]
        public string? Description { get; set; }

        public IEnumerable<CreateTaskDto> Tasks { get; set; } = null!;

        public IEnumerable<string>? Links { get; set; }

        public IEnumerable<Guid>? SoftwareIds { get; set; }

        public IEnumerable<Guid>? Attachments { get; set; }

        [Required]
        public DateTime StartDateUtc { get; set; }

        [Required]
        public DateTime EndDateUtc { get; set; }
    }
}
