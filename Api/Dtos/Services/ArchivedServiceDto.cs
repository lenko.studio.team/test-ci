﻿using System;
using System.Collections.Generic;

namespace Jfdi.ProjectManagement.Api.Dtos.Services
{
    public class ArchivedServiceDto
    {
        public Guid Id { get; set; }

        public string? Name { get; set; }

        public string? Icon { get; set; }

        public string? FillIcon { get; set; }

        public string? Color { get; set; }

        public string? RecurrenceRule { get; set; }

        public bool IsHighPriority { get; set; } = false;

        public IEnumerable<TaskDto>? Tasks { get; set; }

        public DateTime? ArchiveDate { get; set; }
    }
}
