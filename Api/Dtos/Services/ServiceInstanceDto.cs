﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Dtos
{
    public class ServiceInstanceDto
    {
        public ServiceInstanceDto(string name)
        {
            Name = name;
        }

        public Guid Id { get; set; }

        public Guid ServiceTypeId { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string? Description { get; set; }

        public string? Icon { get; set; }

        public string? FillIcon { get; set; }

        [StringLength(10)]
        public string? Color { get; set; }

        public bool IsHighPriority { get; set; } = false;

        public IEnumerable<string>? Links { get; set; }

        public IEnumerable<Guid> TaskIds { get; set; } = new List<Guid>();

        public IEnumerable<Guid> SoftwareIds { get; set; } = new List<Guid>();

        public Guid ClientId { get; set; }

        public string? RecurrenceRule { get; set; }

        public DateTime StartDateUtc { get; set; }

        public DateTime EndDateUtc { get; set; }

        public IEnumerable<AttachmentDto>? Attachments { get; set; } = new List<AttachmentDto>();
    }
}
