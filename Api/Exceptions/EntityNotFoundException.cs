﻿using System;

namespace Jfdi.ProjectManagement.Api.Exceptions
{
    public class EntityNotFoundException : ApplicationException
    {
        public EntityNotFoundException(Type notFoundObject)
            : this(notFoundObject.Name)
        {
        }

        public EntityNotFoundException(string propertyName)
        {
            Message = $"{propertyName} not found";
        }

        public override string Message { get; }
    }
}
