﻿using System;

namespace Jfdi.ProjectManagement.Api.Exceptions
{
    public class ClientErrorException : ApplicationException
    {
        public ClientErrorException(string? message)
            : base(message)
        {
        }
    }
}
