﻿using System;

namespace Jfdi.ProjectManagement.Api.Exceptions
{
    public class AccessException : ApplicationException
    {
        public AccessException()
            : base("Can't get access")
        {
        }
    }
}
