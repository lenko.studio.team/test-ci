﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Text.Json.Serialization;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Configuration;
using Jfdi.ProjectManagement.Api.Filters;
using Jfdi.ProjectManagement.Api.Middleware;
using Jfdi.ProjectManagement.Api.Services;
using Jfdi.ProjectManagement.Api.Services.Blockers;
using Jfdi.ProjectManagement.Api.Services.Notifications;
using Jfdi.ProjectManagement.Api.Services.Tasks;
using Jfdi.ProjectManagement.Api.Services.Tasks.Events;
using Jfdi.ProjectManagement.Api.Services.Tasks.TaskStates;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Context;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api
{
    public class Startup
    {
        private readonly CorsConfig corsConfig = new ();

        private readonly SwaggerConfig swaggerConfig = new ();

        private readonly KeycloakConfig keycloakConfig = new ();

        private readonly ProxyConfig proxyConfig = new ();

        private readonly DatabaseConfig dbConfig = new ();

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Configuration = configuration;
            Configuration.Bind("Cors", corsConfig);
            Configuration.Bind("Swagger", swaggerConfig);
            Configuration.Bind("Keycloak", keycloakConfig);
            Configuration.Bind("Database", dbConfig);
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddJsonOptions(opt =>
                {
                    opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    opt.JsonSerializerOptions.IgnoreNullValues = true;
                    opt.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                });

            services.AddLogging();

            AddSwaggerGen(services);

            services.AddHttpContextAccessor();

            services.AddCors();

            services.AddDbContext<JfdiDbContext>(options =>
            {
                var connString = JfdiDbContext.ConnectionStringFromConfig(dbConfig);
                Console.WriteLine($"Connesction string: {connString}");
                options.UseNpgsql(JfdiDbContext.ConnectionStringFromConfig(dbConfig));
            });

            services.AddAutoMapper(options =>
            {
                options.AddProfile(new MappingProfile());
            });

            services.AddScoped<ICurrentUserProvider, HttpContextUserProvider>();

            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = keycloakConfig.Authority;
                    options.Audience = keycloakConfig.Audience;
                    options.RequireHttpsMetadata = false;
                });

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.All;
                options.RequireHeaderSymmetry = false;
                if (proxyConfig.KnownNetworks != null && proxyConfig.KnownNetworksSize != null)
                {
                    for (int i = 0; i < proxyConfig.KnownNetworks.Length; i++)
                    {
                        options.KnownNetworks.Add(new IPNetwork(
                            IPAddress.Parse(proxyConfig.KnownNetworks[i]),
                            proxyConfig.KnownNetworksSize[i]));
                    }
                }
            });
            services.AddHttpClient<IUserService, KeycloakUserService>(client =>
            {
                if (string.IsNullOrEmpty(keycloakConfig.BaseUrl))
                {
                    throw new ArgumentNullException("Keycloak's base url should not be null or empty");
                }

                client.BaseAddress = new Uri(keycloakConfig.BaseUrl);
            });

            services.AddMassTransit(x =>
            {
                x.AddConsumer<TaskStateNotificationConsumer>();

                x.UsingInMemory((context, cfg) =>
                {
                    cfg.ConfigureEndpoints(context);
                });
            });

            services.AddMassTransitHostedService(true);

            services.Configure<KeycloakConfig>(Configuration.GetSection("Keycloak"));
            services.AddScoped<ITasksService, TasksService>();
            services.AddScoped<ITaskStateService, TaskStateService>();
            services.AddScoped<IServiceTypeService, ServiceTypeService>();
            services.AddScoped<ISoftwareService, SoftwareService>();
            services.AddScoped<IAttachmentService, AttachmentService>();
            services.AddScoped<IServiceInstanceService, ServiceInstanceService>();
            services.AddScoped<ITaskStateService, TaskStateService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IBlockerService, BlockerService>();
            services.AddScoped<ITaskStateService, TaskStateService>();
            services.AddScoped<ITeamMemberService, TeamMemberService>();
            services.AddScoped<ITaskEventService, TaskEventService>();
            services.AddScoped<INotificationsService, NotificationsService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            loggerFactory.AddSerilog();

            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint(swaggerConfig.SwaggerEndpoint, swaggerConfig.SwaggerName);
            });

            app.UseCors(options =>
            {
                options.WithOrigins(corsConfig.CorsAllowedUrls).AllowAnyMethod().AllowAnyHeader();
            });

            app.UseForwardedHeaders();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.Use(EnrichLogWithUserIdAndIp());

            app.UseSerilogRequestLogging();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseMiddleware<HttpStatusCodeExceptionMiddleware>();
            app.UseMiddleware<TransactionMiddleware>();

            app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });
        }

        private static Func<HttpContext, Func<Task>, Task> EnrichLogWithUserIdAndIp()
        {
            return async (httpContext, next) =>
            {
                string? username = null;
                if (httpContext.User != null)
                {
                    var userClaim = httpContext.User.Claims.FirstOrDefault(c =>
                         c.Type == ClaimTypes.NameIdentifier);
                    username = userClaim?.Value ?? username;
                }

                var ip = httpContext.Connection?.RemoteIpAddress?.MapToIPv4().ToString();
                using (LogContext.PushProperty("UserId", username))
                using (LogContext.PushProperty("Ip", ip))
                {
                    await next.Invoke();
                }
            };
        }

        private void AddSwaggerGen(IServiceCollection services)
        {
            var oidcUrl = $"{keycloakConfig.BaseUrl}auth/realms/{keycloakConfig.Realm}/protocol/openid-connect/";

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Jfdi Project Management", Version = "v1" });
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows()
                    {
                        ClientCredentials = new OpenApiOAuthFlow()
                        {
                            AuthorizationUrl = new Uri($"{oidcUrl}auth"),
                            TokenUrl = new Uri($"{oidcUrl}token"),
                        },
                    },
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            // https://github.com/domaindrivendev/Swashbuckle.AspNetCore#systemtextjson-stj-vs-newtonsoft
            // services.AddSwaggerGenNewtonsoftSupport();
        }
    }
}
