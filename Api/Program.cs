﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Middleware;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Jfdi.ProjectManagement.Api
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.Development.json", optional: true)
                .AddJsonFile("appsettings.Production.json", optional: true)
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom
                .Configuration(config)
                .CreateLogger();

            try
            {
                Log.Information("Starting application...");
                var host = CreateHostBuilder(args).Build();
                Migrate(host.Services);
                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application crashed.");
            }
            finally
            {
                Log.Information("Application shut down.");
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseSerilog();

        private static void Migrate(IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.CreateScope();
            var context = scope.ServiceProvider.GetService<JfdiDbContext>();
            var logger = scope.ServiceProvider.GetService<ILogger<Startup>>();

            if (context == null)
            {
                logger.LogError("Not found context");
                return;
            }

            var pendingMigrations = context.Database.GetPendingMigrations();
            if (pendingMigrations.Any())
            {
                var migrations = string.Join("\n", pendingMigrations);
                logger.LogInformation($"Pending migrations:{migrations}");
            }
            else
            {
                logger.LogInformation($"No pending migrations");
                return;
            }

            try
            {
                context.Database.Migrate();
                logger.LogInformation($"Migrate success!");
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Error while migrate");
                throw;
            }
        }
    }
}
