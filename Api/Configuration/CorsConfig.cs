﻿namespace Jfdi.ProjectManagement.Api.Configuration
{
    public class CorsConfig : ConfigurationOptions
    {
        public string[] CorsAllowedUrls { get; set; } = null!;
    }
}
