﻿namespace Jfdi.ProjectManagement.Api.Configuration
{
    public class KeycloakConfig : ConfigurationOptions
    {
        public string? ClientId { get; set; }

        public string? ClientSecret { get; set; }

        public string? BaseUrl { get; set; }

        public string? Realm { get; set; }

        public string? Audience { get; set; }

        public string? RedirectUrl { get; set; }

        public string? Authority => $"{BaseUrl}auth/realms/{Realm}";

        public string? MemberGroup { get; set; }

        public string? AdminGroup { get; set; }

        public string? OwnerGroup { get; set; }
    }
}
