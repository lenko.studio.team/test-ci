﻿namespace Jfdi.ProjectManagement.Api.Configuration
{
    public class SwaggerConfig : ConfigurationOptions
    {
        public string? SwaggerName { get; set; }

        public string? SwaggerEndpoint { get; set; }
    }
}
