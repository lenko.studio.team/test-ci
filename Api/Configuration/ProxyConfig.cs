﻿namespace Jfdi.ProjectManagement.Api.Configuration
{
    public class ProxyConfig : ConfigurationOptions
    {
        public string[] KnownNetworks { get; set; } = null!;

        public int[] KnownNetworksSize { get; set; } = null!;
    }
}
