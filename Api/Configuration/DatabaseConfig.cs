﻿namespace Jfdi.ProjectManagement.Api.Configuration
{
    public class DatabaseConfig : ConfigurationOptions
    {
        public string? Host { get; set; }

        public int? Port { get; set; }

        public string? User { get; set; }

        public string? Password { get; set; }

        public string? InitialCatalog { get; set; }
    }
}
