﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace Jfdi.ProjectManagement.Api
{
    public static class QueryExtensions
    {
        public static async Task<T> FirstOrFail<T>(
            this DbSet<T> query,
            Expression<Func<T, bool>> predicate)
            where T : class
        {
            var result = await query.FirstOrDefaultAsync(predicate);

            if (result is null)
            {
                throw new EntityNotFoundException(typeof(T));
            }

            return result;
        }

        public static async Task<T> FirstOrFail<T, TProperty>(
            this IIncludableQueryable<T, TProperty> query,
            Expression<Func<T, bool>> predicate)
            where T : class
        {
            var result = await query.FirstOrDefaultAsync(predicate);

            if (result is null)
            {
                throw new EntityNotFoundException(typeof(T));
            }

            return result;
        }

        public static async Task<T> FirstOrFail<T>(
            this IQueryable<T> query,
            Expression<Func<T, bool>> predicate)
            where T : class
        {
            var result = await query.FirstOrDefaultAsync(predicate);

            if (result is null)
            {
                throw new EntityNotFoundException(typeof(T));
            }

            return result;
        }

        public static T FirstOrFail<T>(
            this IEnumerable<T> query,
            Func<T, bool> predicate)
            where T : class
        {
            var result = query.FirstOrDefault(predicate);

            if (result is null)
            {
                throw new EntityNotFoundException(typeof(T));
            }

            return result;
        }

        public static async Task<T> FindOrFailAsync<T>(
            this DbSet<T> query,
            params object[] keyValues)
            where T : class
        {
            var result = await query.FindAsync(keyValues);

            if (result is null)
            {
                throw new EntityNotFoundException(typeof(T));
            }

            return result;
        }
    }
}
