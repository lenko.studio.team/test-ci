﻿using System;
using System.Linq;
using AutoMapper;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Attachments;
using Jfdi.ProjectManagement.Api.Dtos.Clients;
using Jfdi.ProjectManagement.Api.Dtos.Comments;
using Jfdi.ProjectManagement.Api.Dtos.Notifications;
using Jfdi.ProjectManagement.Api.Dtos.Services;
using Jfdi.ProjectManagement.Api.Dtos.TaskInstancies;
using Jfdi.ProjectManagement.Api.Dtos.Tasks;
using Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances;
using Jfdi.ProjectManagement.Api.Dtos.Users;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Attachments;
using Jfdi.ProjectManagement.Api.Models.Notifications;
using Microsoft.AspNetCore.Mvc;

namespace Jfdi.ProjectManagement.Api
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            AllowNullCollections = true;
            CreateMap<Client, ClientDto>()
                .ForMember(dest => dest.CalendarId, opt => opt.MapFrom(src => src.Calendar.Id))
                .ForMember(dest => dest.TeamMemberAssignments, opt => opt.MapFrom(src => src.TeamMemberAssignments));

            CreateMap<Client, ArchiveClientDto>()
                .ForMember(dest => dest.ArchiveDate, opt => opt.MapFrom(src => src.ArchiveInfo!.ArchivedAtUtc))
                .ForMember(dest => dest.Services, opt => opt.MapFrom(src => src.Services));

            CreateMap<ClientDto, Client>();

            CreateMap<TaxClassification, TaxClassificationDto>();

            CreateMap<UpdateClientViewModel, Client>()
                .ForMember(dest => dest.TeamMemberAssignments, opt => opt.Ignore());

            CreateMap<TeamMember, TeamMemberDto>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.Id))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
                .ForMember(dest => dest.Clients, opt => opt.MapFrom(src => src.Clients))
                .ForMember(dest => dest.Permission, opt => opt.MapFrom(src => src.User.Permission));

            CreateMap<TeamMember, ArchiveTeamMemberDto>()
                .ForMember(dest => dest.ArchiveDate, opt => opt.MapFrom(src => src.ArchiveInfo!.ArchivedAtUtc))
                .ForMember(dest => dest.ClientsCount, opt => opt.MapFrom(src => src.Clients.Count()));

            CreateMap<CalendarEvent, CalendarEventDto>();

            CreateMap<Task, TaskDto>()
                .ForMember(dest => dest.StartUtc, opt => opt.MapFrom(src => src.StartDateUtc))
                .ForMember(dest => dest.EndUtc, opt => opt.MapFrom(src => src.EndDateUtc))
                .ForMember(dest => dest.Children, opt => opt.MapFrom(src => src.Children));

            CreateMap<CreateTaskDto, Task>()
                .ForMember(dest => dest.StartDateUtc, opt => opt.MapFrom(src => src.StartUtc))
                .ForMember(dest => dest.Children, opt => opt.Ignore());
            CreateMap<Task, SubTaskDto>()
                .ForMember(dest => dest.StartUtc, opt => opt.MapFrom(src => src.StartDateUtc))
                .ForMember(dest => dest.EndUtc, opt => opt.MapFrom(src => src.EndDateUtc));

            CreateMap<TaskEvent, TaskEventDto>()
                .ForMember(dest => dest.SubTasks, opt => opt.MapFrom(src => src.Children))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Instance.Name))
                .ForMember(dest => dest.StartDateUtc, opt => opt.MapFrom(src => src.StartDateUtc))
                .ForMember(dest => dest.EndDateUtc, opt => opt.MapFrom(src => src.EndDateUtc))
                .ForMember(dest => dest.AssignedTo, opt => opt.MapFrom(src => src.Instance.Assignees))
                .ForMember(dest => dest.Supervisors, opt => opt.MapFrom(src => src.Instance.Supervisors))
                .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.State))
                .ForMember(dest => dest.Client, opt => opt.MapFrom(src => src.Instance.Service!.Client))
                .ForMember(dest => dest.Service, opt => opt.MapFrom(src => src.Instance.Service))
                .ForMember(dest => dest.RecurrenceRule, opt => opt.MapFrom(src => src.Instance.Event.RecurrenceRule))
                .ForMember(dest => dest.IsHighPriority, opt => opt.MapFrom(src => src.Instance.IsHighPriority))
                .ForMember(dest => dest.Links, opt => opt.MapFrom(src => src.Instance.Links))
                .ForMember(dest => dest.ForReview, opt => opt.MapFrom(src => src.Instance.ForReview))
                .ForMember(dest => dest.InstanceId, opt => opt.MapFrom(src => src.Instance.Id))
                .ForMember(dest => dest.Blockers, opt => opt.MapFrom(src => src.Blockers == null ? null : src.Blockers));

            CreateMap<TaskEvent, ArchivedTaskDto>()
                .ForMember(dest => dest.ArchiveDate, opt => opt.MapFrom(src => src.ArchiveInfo!.ArchivedAtUtc))
                .ForMember(dest => dest.SubTasks, opt => opt.MapFrom(src => src.Children))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Instance.Name))
                .ForMember(dest => dest.Client, opt => opt.MapFrom(src => src.Instance.Service!.Client));

            CreateMap<TaskEvent, TaskEventShortDto>()
                .ForMember(dest => dest.ClientId, opt => opt.MapFrom(src => src.Instance.Service!.Client.Id))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Instance.Name))
                .ForMember(dest => dest.EndDateUtc, opt => opt.MapFrom(src => src.EndDateUtc))
                .ForMember(dest => dest.AssignedTo, opt => opt.MapFrom(src => src.Instance.Assignees))
                .ForMember(dest => dest.Supervisors, opt => opt.MapFrom(src => src.Instance.Supervisors))
                .ForMember(dest => dest.Service, opt => opt.MapFrom(src => src.Instance.Service))
                .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.State));

            CreateMap<Service, ServiceDto>()
                .ForMember(dest => dest.Icon, opt => opt.MapFrom(src => src.ServiceType.Icon))
                .ForMember(dest => dest.FillIcon, opt => opt.MapFrom(src => src.ServiceType.FillIcon))
                .ForMember(dest => dest.Attachments, opt => opt.MapFrom(src => src.Attachments))
                .ForMember(dest => dest.ClientsCount, opt => opt.MapFrom(src => src.Instancies.Count))
                .ForMember(dest => dest.Color, opt => opt.MapFrom(src => src.ServiceType.Color));

            CreateMap<Service, ArchivedServiceDto>()
                .ForMember(dest => dest.ArchiveDate, opt => opt.MapFrom(src => src.ArchiveInfo!.ArchivedAtUtc))
                 .ForMember(dest => dest.Icon, opt => opt.MapFrom(src => src.ServiceType.Icon))
                 .ForMember(dest => dest.FillIcon, opt => opt.MapFrom(src => src.ServiceType.FillIcon))
                 .ForMember(dest => dest.Color, opt => opt.MapFrom(src => src.ServiceType.Color));

            CreateMap<ServiceInstance, ServiceInstanceDto>()
                .ForMember(dest => dest.ServiceTypeId, opt => opt.MapFrom(src => src.Template.Id))
                .ForMember(dest => dest.Icon, opt => opt.MapFrom(src => src.Template.ServiceType.Icon))
                .ForMember(dest => dest.FillIcon, opt => opt.MapFrom(src => src.Template.ServiceType.FillIcon))
                .ForMember(dest => dest.Attachments, opt => opt.MapFrom(src => src.Attachments))
                .ForMember(dest => dest.RecurrenceRule, opt => opt.MapFrom(src => src.Event.RecurrenceRule))
                .ForMember(dest => dest.StartDateUtc, opt => opt.MapFrom(src => src.Event.StartUtc))
                .ForMember(dest => dest.EndDateUtc, opt => opt.MapFrom(src => src.Event.EndUtc))
                .ForMember(dest => dest.TaskIds, opt => opt.MapFrom(src => src.Tasks.Select(x => x.Id)))
                .ForMember(dest => dest.SoftwareIds, opt => opt.MapFrom(src => src.Softwares.Select(x => x.Id)))
                .ForMember(dest => dest.Color, opt => opt.MapFrom(src => src.Template.ServiceType.Color));

            CreateMap<Blocker, BlockerDto>()
                .ForMember(dest => dest.TaskId, opt => opt.MapFrom(src => src.Task.Id))
                .ForMember(dest => dest.CalendarEventId, opt => opt.MapFrom(src => src.CalendarEvent.Id))
                .ForMember(dest => dest.StartDateUtc, opt => opt.MapFrom(src => src.CalendarEvent.StartUtc))
                .ForMember(dest => dest.EndDateUtc, opt => opt.MapFrom(src => src.CalendarEvent.EndUtc))
                .ForMember(dest => dest.AssignedUser, opt => opt.MapFrom(src => src.AssignedUser))
                .ForMember(dest => dest.Service, opt => opt.MapFrom(src => src.Service));
            CreateMap<TaskState, TaskStateDto>();

            CreateMap<CreateTeamMemberDto, TeamMember>();
            CreateMap<TeamMember, ShortTeamMemberDto>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.Id))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email));

            CreateMap<Client, ClientShortDto>();

            CreateMap<ServiceInstance, ServiceShortDto>()
                .ForMember(dest => dest.ServiceTypeId, opt => opt.MapFrom(src => src.Template.Id))
                .ForMember(dest => dest.Icon, opt => opt.MapFrom(src => src.Template.ServiceType.Icon))
                .ForMember(dest => dest.ServiceTypeId, opt => opt.MapFrom(src => src.Template.ServiceType.Id))
                .ForMember(dest => dest.FillIcon, opt => opt.MapFrom(src => src.Template.ServiceType.FillIcon))
                .ForMember(dest => dest.Color, opt => opt.MapFrom(src => src.Template.ServiceType.Color));

            CreateMap<Software, SoftwareDto>()
                .ForMember(dest => dest.Attachments, opt => opt.MapFrom(src => src.Attachments));

            CreateMap<ServiceType, ServiceTypeDto>()
                .ForMember(dest => dest.Services, opt => opt.MapFrom(src => src.Services));
            CreateMap<ServiceType, ServiceTypeShortDto>();

            CreateMap<Service, ServiceShortDto>()
                .ForMember(dest => dest.Icon, opt => opt.MapFrom(src => src.ServiceType.Icon))
                .ForMember(dest => dest.ServiceTypeId, opt => opt.MapFrom(src => src.ServiceType.Id))
                .ForMember(dest => dest.Color, opt => opt.MapFrom(src => src.ServiceType.Color))
                .ForMember(dest => dest.FillIcon, opt => opt.MapFrom(src => src.ServiceType.FillIcon));

            CreateMap<CreateServiceDto, Service>()
                .ForMember(dest => dest.Softwares, opt => opt.Ignore())
                .ForMember(dest => dest.Tasks, opt => opt.Ignore())
                .ForMember(dest => dest.Attachments, opt => opt.Ignore());

            CreateMap<UpdateServiceDto, Service>()
               .ForMember(dest => dest.Softwares, opt => opt.Ignore())
               .ForMember(dest => dest.Tasks, opt => opt.Ignore())
               .ForMember(dest => dest.Attachments, opt => opt.Ignore());

            CreateMap<CreateSoftwareDto, Software>();
            CreateMap<Attachment, AttachmentDto>()
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type));

            CreateMap<DirectAttachment, FileResult>()
                .ConstructUsing(fb => new FileContentResult(fb.Data, "application/octet-stream")
                {
                    FileDownloadName = fb.Name
                });
            CreateMap<GDriveAttachment, GDriveAttachmentDto>();
            CreateMap<UpdateTeamMemberDto, TeamMember>();
            CreateMap<Comment, CommentDto>()
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Data))
                .ForMember(dest => dest.CreateDate, opt => opt.MapFrom(src => src.CreatedAtUtc))
                .ForMember(dest => dest.TeamMemberId, opt => opt.MapFrom(src => src.CreatedById))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.CreatedBy.TeamMember == null ? null : src.CreatedBy.TeamMember.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.CreatedBy.TeamMember == null ? null : src.CreatedBy.TeamMember.LastName));

            CreateMap<TaskEvent, SubTaskEventDto>()
                .ForMember(dest => dest.StartDateUtc, opt => opt.MapFrom(src => src.StartDateUtc))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Instance.Name))
                .ForMember(dest => dest.EndDateUtc, opt => opt.MapFrom(src => src.EndDateUtc))
                .ForMember(dest => dest.Blockers, opt => opt.MapFrom(src => src.Blockers))
                .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.State));
            CreateMap<NotificationOption, NotificationOptionDto>();
            CreateMap<NotificationOptionDto, NotificationOption>();
        }
    }
}
