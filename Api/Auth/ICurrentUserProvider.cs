﻿using System;

namespace Jfdi.ProjectManagement.Api.Auth
{
    public interface ICurrentUserProvider
    {
        public AuthUser User { get; }

        public Guid CurrentUserId => User.User.Id;
    }
}
