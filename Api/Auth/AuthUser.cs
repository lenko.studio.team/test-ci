﻿using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Users;

namespace Jfdi.ProjectManagement.Api.Auth
{
    public class AuthUser
    {
        public AuthUser(User user, UserPermissions permission)
        {
            User = user;
            Permission = permission;
        }

        public User User { get; set; } = null!;

        public UserPermissions Permission { get; set; }
    }
}
