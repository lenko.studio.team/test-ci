﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Jfdi.ProjectManagement.Api.Auth
{
    public class HttpContextUserProvider : ICurrentUserProvider
    {
        private static readonly object AddEmailKeyLock = new ();
        private static readonly Dictionary<string, object> CreateNewUserLocks = new ();

        public HttpContextUserProvider(
            IHttpContextAccessor httpContextAccessor,
            JfdiDbContext db,
            ILogger<HttpContextUserProvider> logger)
        {
            if (httpContextAccessor.HttpContext == null)
            {
                logger.LogInformation("No HTTP Context, starting as system user");
                var unAuthUser = FindOrCreateUser(
                    db,
                    logger,
                    email: "system",
                    sub: default(Guid).ToString(),
                    UserPermissions.Owner);
                User = new AuthUser(unAuthUser, UserPermissions.Owner);
                return;
            }

            var ctxUser = httpContextAccessor.HttpContext.User;
            if (ctxUser == null)
            {
                throw new ArgumentException("HttpContext user should not be null!");
            }

            if (ctxUser.Identity?.IsAuthenticated == false)
            {
                throw new ArgumentException("HttpContext user should be authenticated!");
            }

            var roleClaim = ctxUser.FindFirst(x => x.Type == ClaimTypes.Role)?.Value;
            if (roleClaim == null)
            {
                throw new AccessException();
            }

            var emailClaim = ctxUser.FindFirst(x => x.Type == ClaimTypes.Email)?.Value;
            if (emailClaim == null)
            {
                emailClaim = ctxUser.FindFirst(x => x.Type == "preferred_username")?.Value;
            }

            if (emailClaim == null)
            {
                logger.LogError("User claims did not contain an email/username! {claims}", ctxUser.Claims);
                throw new ArgumentException("User claims did not contain an email/username!");
            }

            var sub = ctxUser.FindFirst(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            if (sub == null)
            {
                logger.LogError("User claims did not contain a sub claim! {claims}", ctxUser.Claims);
                throw new ArgumentException("User claims did not contain a sub claim!");
            }

            var role = UserPermission.ParsePermission(roleClaim);
            var user = FindOrCreateUser(db, logger, emailClaim, sub, role);
            User = new AuthUser(user, role);
        }

        public AuthUser User { get; }

        private static User FindOrCreateUser(
            JfdiDbContext db,
            ILogger<HttpContextUserProvider> logger,
            string email,
            string sub,
            UserPermissions permission)
        {
#pragma warning disable SCS0006 // Weak hashing function.
            var crypto = CryptoConfig.CreateFromName("MD5");
#pragma warning restore SCS0006 // Weak hashing function.
            if (crypto == null)
            {
                throw new CryptographicUnexpectedOperationException(nameof(crypto));
            }

            var hashBytes = ((HashAlgorithm)crypto).ComputeHash(Encoding.UTF8.GetBytes(email));
            var lockKey = Encoding.UTF8.GetString(hashBytes);
            lock (AddEmailKeyLock)
            {
                if (!CreateNewUserLocks.ContainsKey(lockKey))
                {
                    CreateNewUserLocks.Add(lockKey, new object());
                }
            }

            lock (CreateNewUserLocks[lockKey])
            {
                var user = db.Users.FirstOrDefault(x => x.Email == email);
                if (user == null)
                {
                    logger.LogInformation("User not found in database, creating new entry. Email: {email}, Sub: {sub}", email, sub);
                    user = new User(Guid.Parse(sub), email, DateTime.UtcNow, permission);
                    db.Users.Add(user);
                    db.SaveChanges();
                }

                return user;
            }
        }
    }
}
