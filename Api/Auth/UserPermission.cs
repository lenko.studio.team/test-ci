﻿using System;
using Jfdi.ProjectManagement.Api.Exceptions;
using Jfdi.ProjectManagement.Api.Models.Users;

namespace Jfdi.ProjectManagement.Api.Auth
{
    public class UserPermission
    {
        public const string Owner = "jfdiapp.roles.owner";

        public const string Admin = "jfdiapp.roles.admin";

        public const string Member = "jfdiapp.roles.member";

        public const string OwnerAdmin = Owner + "," + Admin;

        public static UserPermissions ParsePermission(string permission)
        {
            var str = permission.Replace("jfdiapp.roles.", string.Empty);
            if (Enum.TryParse<UserPermissions>(str, true, out var userPermission))
            {
                return userPermission;
            }

            throw new AccessException();
        }

        public static bool TryParsePermission(string permissionName, out UserPermissions permission)
        {
            return Enum.TryParse(permissionName, true, out permission);
        }

        public static string ToString(UserPermissions userPermission)
        {
            switch (userPermission)
            {
                case UserPermissions.Owner:
                    return Owner;
                case UserPermissions.Admin:
                    return Admin;
                case UserPermissions.Member:
                    return Member;
            }

            throw new AccessException();
        }
    }
}
