﻿using System;
using System.Net;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Jfdi.ProjectManagement.Api.Middleware
{
    public class HttpStatusCodeExceptionMiddleware
    {
        private readonly ILogger logger;

        private readonly RequestDelegate next;

        public HttpStatusCodeExceptionMiddleware(
            RequestDelegate next,
            ILogger<HttpStatusCodeExceptionMiddleware> logger)
        {
            this.next = next;
            this.logger = logger;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await next(httpContext);
            }
            catch (AccessException ae)
            {
                await WriteResponse(httpContext, ae.Message, HttpStatusCode.Forbidden);
            }
            catch (EntityNotFoundException notFoundException)
            {
                await WriteResponse(httpContext, notFoundException.Message, HttpStatusCode.NotFound);
            }
            catch (DbUpdateConcurrencyException)
            {
                await WriteResponse(httpContext, "Resource locked", HttpStatusCode.Conflict);
            }
            catch (ClientErrorException exception)
            {
                await WriteResponse(httpContext, exception.Message, HttpStatusCode.BadRequest);
            }
            catch (Exception exception)
            {
                logger.LogError(exception, "Runtime error");
                await WriteResponse(httpContext, "Internal server error");
            }
        }

        private static async Task WriteResponse(
            HttpContext context,
            string message,
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError,
            string contentType = "text/plain; charset=utf-8")
        {
            context.Response.StatusCode = (int)statusCode;
            context.Response.ContentType = contentType;
            await context.Response.WriteAsync(message);
        }
    }
}
