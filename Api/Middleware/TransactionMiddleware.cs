﻿using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Services;
using Microsoft.AspNetCore.Http;

namespace Jfdi.ProjectManagement.Api.Middleware
{
    public class TransactionMiddleware
    {
        private readonly RequestDelegate next;

        public TransactionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public Task Invoke(HttpContext httpContext, ITransactionService transactionService)
        {
            return transactionService.InvokeInTransaction(next, httpContext);
        }
    }
}
