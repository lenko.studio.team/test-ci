﻿using System;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class CalendarEvent : AuditableEntity
    {
        public CalendarEvent(
            Guid id,
            DateTime createdAtUtc,
            Guid createdById,
            string title,
            DateTime startUtc,
            DateTime endUtc,
            string? recurrenceRule = null)
            : base(id, createdAtUtc, createdById)
        {
            Title = title;
            StartUtc = startUtc;
            EndUtc = endUtc;
            RecurrenceRule = recurrenceRule;
        }

        public string Title { get; set; }

        [IsUtc]
        public DateTime StartUtc { get; set; }

        [IsUtc]
        public DateTime EndUtc { get; set; }

        public Guid CalendarId { get; set; }

        public string? RecurrenceRule { get; set; }

        public virtual Calendar Calendar { get; set; } = null!;
    }
}
