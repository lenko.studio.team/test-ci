﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class ServiceInstance : AuditableEntity
    {
        public ServiceInstance(
               Guid id,
               DateTime createdAtUtc,
               Guid createdById,
               string name)
               : base(
                   id,
                   createdAtUtc,
                   createdById)
        {
            Name = name;
        }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string? Description { get; set; }

        public IEnumerable<string>? Links { get; set; }

        public Service Template { get; set; } = null!;

        public Client Client { get; set; } = null!;

        public bool IsHighPriority { get; set; } = false;

        public CalendarEvent Event { get; set; } = null!;

        public virtual ICollection<Software> Softwares { get; set; } = new Collection<Software>();

        public virtual ICollection<TaskInstance> Tasks { get; set; } = new Collection<TaskInstance>();

        public virtual ICollection<Attachment> Attachments { get; set; } = new Collection<Attachment>();
    }
}
