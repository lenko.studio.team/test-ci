﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class Calendar : AuditableEntity
    {
        public Calendar(Guid id, DateTime createdAtUtc, Guid createdById)
            : base(id, createdAtUtc, createdById)
        {
        }

        public virtual ICollection<CalendarEvent> CalendarEvents { get; set; } = new Collection<CalendarEvent>();
    }
}
