﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jfdi.ProjectManagement.Api.Models
{
    public abstract class Entity
    {
        protected Entity(Guid id)
        {
            Id = id;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }
    }
}
