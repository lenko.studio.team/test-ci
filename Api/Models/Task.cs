﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class Task : AuditableEntity
    {
        public Task(
            string title,
            Guid id,
            DateTime createdAtUtc,
            Guid createdById)
            : base(id, createdAtUtc, createdById)
        {
            Title = title;
        }

        [StringLength(255)]
        public string Title { get; set; }

        public Task? Parent { get; set; }

        public virtual ICollection<Task> Children { get; set; } = new Collection<Task>();

        [StringLength(255)]
        public string? RecurrenceRule { get; set; }

        [StringLength(255)]
        public string? Info { get; set; }

        [IsUtc]
        public DateTime StartDateUtc { get; set; }

        [IsUtc]
        public DateTime EndDateUtc { get; set; }
    }
}
