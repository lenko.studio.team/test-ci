﻿using System;
using System.ComponentModel.DataAnnotations;
using Jfdi.ProjectManagement.Api.Models.Users;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Models
{
    [Index(nameof(Email), IsUnique = true)]
    public class User : Entity
    {
        public User(Guid id, string email, DateTime createdAt, UserPermissions permission)
            : base(id)
        {
            Email = email;
            CreatedAt = createdAt;
            Permission = permission;
        }

        [StringLength(255)]
        public string Email { get; set; }

        [IsUtc]
        public DateTime CreatedAt { get; set; }

        public UserPermissions Permission { get; set; }

        public virtual TeamMember? TeamMember { get; set; }
    }
}
