﻿namespace Jfdi.ProjectManagement.Api.Models.Users
{
    public enum UserPermissions
    {
        /// <summary>
        /// Owner.
        /// </summary>
        Owner,

        /// <summary>
        /// Admin.
        /// </summary>
        Admin,

        /// <summary>
        /// Member.
        /// </summary>
        Member
    }
}
