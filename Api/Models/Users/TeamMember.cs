﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Jfdi.ProjectManagement.Api.Models.Notifications;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class TeamMember : AuditableEntity, ISoftArchive
    {
        public TeamMember(
            Guid id,
            DateTime createdAtUtc,
            Guid createdById,
            string lastName,
            string firstName)
            : base(id, createdAtUtc, createdById)
        {
            LastName = lastName;
            FirstName = firstName;
        }

        public virtual User User { get; set; } = null!;

        public Guid UserId { get; set; }

        [StringLength(255)]
        public string LastName { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }

        [StringLength(255)]
        public string? PhysicalAddress { get; set; }

        public int? SocialSecurityNumber { get; set; }

        [IsUtc]
        public DateTime? BirthDate { get; set; }

        public string? CompanyMailingAddress { get; set; }

        [StringLength(255)]
        public string? CellPhone { get; set; }

        [StringLength(255)]
        public string? FullName { get; set; }

        [StringLength(255)]
        public string? Address { get; set; }

        [StringLength(255)]
        public string? Phone { get; set; }

        [StringLength(255)]
        public string? Title { get; set; }

        public TeamMember? Supervisor { get; set; }

        [StringLength(255)]
        public string? Salary { get; set; }

        [IsUtc]
        public DateTime? StartDate { get; set; }

        [StringLength(255)]
        public string? NameOfCollege { get; set; }

        [StringLength(255)]
        public string? Degree { get; set; }

        [Column(TypeName = "character varying(30)")]
        public TeamMemberRole Role { get; set; } = TeamMemberRole.Staff;

        public bool EmailConfirmed { get; set; } = false;

        public virtual ICollection<Client> Clients { get; set; } = new Collection<Client>();

        public virtual ICollection<TaskInstance> AssignesTasks { get; set; } = new Collection<TaskInstance>();

        public virtual ICollection<TaskInstance> SupervisorTasks { get; set; } = new Collection<TaskInstance>();

        public ArchiveData? ArchiveInfo { get; set; }

        public virtual ICollection<NotificationOption> NotificationOptions { get; set; } = new Collection<NotificationOption>();
    }
}
