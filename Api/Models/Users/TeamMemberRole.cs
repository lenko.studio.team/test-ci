﻿namespace Jfdi.ProjectManagement.Api.Models
{
    public enum TeamMemberRole
    {
        /// <summary>
        /// Executive.
        /// </summary>
        Executive,

        /// <summary>
        /// Manager.
        /// </summary>
        Manager,

        /// <summary>
        /// Staff.
        /// </summary>
        Staff
    }
}
