﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class Software : AuditableEntity
    {
        public Software(
            string name,
            Guid id,
            DateTime createdAtUtc,
            Guid createdById)
            : base(id, createdAtUtc, createdById)
        {
            Name = name;
        }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string? Description { get; set; }

        public IEnumerable<string>? Links { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; } = new Collection<Attachment>();

        public virtual ICollection<Service> Services { get; set; } = new Collection<Service>();

        public virtual ICollection<ServiceInstance> ServiceInstancies { get; set; } = new Collection<ServiceInstance>();
    }
}
