﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class TaskInstance : AuditableEntity
    {
        public TaskInstance(
               Guid id,
               DateTime createdAtUtc,
               Guid createdById,
               string name)
               : base(
                   id,
                   createdAtUtc,
                   createdById)
        {
            Name = name;
        }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string? Info { get; set; }

        public bool ForReview { get; set; }

        public bool IsHighPriority { get; set; }

        public IEnumerable<string>? Links { get; set; }

        public CalendarEvent Event { get; set; } = null!;

        public ServiceInstance? Service { get; set; }

        public Task? Template { get; set; }

        public virtual ICollection<TeamMember> Assignees { get; set; } = new Collection<TeamMember>();

        public virtual ICollection<TeamMember> Supervisors { get; set; } = new Collection<TeamMember>();

        public TaskInstance? Parent { get; set; }

        public ICollection<TaskInstance> Children { get; set; } = new Collection<TaskInstance>();

        public Software? Software { get; set; }

        public ICollection<TaskEvent> TaskEvents { get; set; } = new Collection<TaskEvent>();
    }
}
