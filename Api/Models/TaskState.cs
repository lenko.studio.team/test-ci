﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class TaskState : Entity
    {
        public TaskState(
            string title,
            Guid id)
            : base(id)
        {
            Title = title;
        }

        [StringLength(255)]
        public string Title { get; set; }
    }
}
