﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Jfdi.ProjectManagement.Api.Models.Archives;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class Service : AuditableEntity, ISoftArchive
    {
        public Service(
            string name,
            Guid id,
            DateTime createdAtUtc,
            Guid createdById)
            : base(id, createdAtUtc, createdById)
        {
            Name = name;
        }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string? Description { get; set; }

        public bool IsHighPriority { get; set; }

        public IEnumerable<string>? Links { get; set; }

        public ServiceType ServiceType { get; set; } = null!;

        public string? RecurrenceRule { get; set; }

        public virtual ICollection<Task> Tasks { get; set; } = new Collection<Task>();

        public virtual ICollection<Software> Softwares { get; set; } = new Collection<Software>();

        public virtual ICollection<Attachment> Attachments { get; set; } = new Collection<Attachment>();

        [IsUtc]
        public DateTime StartDateUtc { get; set; }

        [IsUtc]
        public DateTime EndDateUtc { get; set; }

        public ArchiveData? ArchiveInfo { get; set; }

        public virtual ICollection<ServiceInstance> Instancies { get; set; } = new Collection<ServiceInstance>();
    }
}
