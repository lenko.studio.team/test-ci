﻿using System;

namespace Jfdi.ProjectManagement.Api.Models.Archives
{
    public class ArchiveData : Entity
    {
        public ArchiveData(
            Guid id,
            DateTime archivedAtUtc,
            Guid archivedById)
            : base(id)
        {
            ArchivedAtUtc = archivedAtUtc;
            ArchivedById = archivedById;
        }

        public Guid ArchivedById { get; set; }

        public virtual User ArchivedBy { get; set; } = null!;

        [IsUtc]
        public DateTime ArchivedAtUtc { get; set; }
    }
}
