﻿namespace Jfdi.ProjectManagement.Api.Models.Archives
{
    public interface ISoftArchive
    {
        ArchiveData? ArchiveInfo { get; set; }
    }
}
