﻿using System;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class IsUtcAttribute : Attribute
    {
        public IsUtcAttribute(bool isUtc = true) => IsUtc = isUtc;

        public bool IsUtc { get; }
    }
}
