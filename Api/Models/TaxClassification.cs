﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class TaxClassification : Entity
    {
        public TaxClassification(Guid id, string title)
            : base(id)
        {
            Title = title;
        }

        [StringLength(255)]
        public string Title { get; set; }
    }
}
