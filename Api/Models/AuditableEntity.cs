﻿using System;

namespace Jfdi.ProjectManagement.Api.Models
{
    public abstract class AuditableEntity : Entity
    {
        protected AuditableEntity(
            Guid id,
            DateTime createdAtUtc,
            Guid createdById)
            : base(id)
        {
            CreatedAtUtc = createdAtUtc;
            CreatedById = createdById;
        }

        [IsUtc]
        public DateTime CreatedAtUtc { get; set; }

        public Guid CreatedById { get; set; }

        public virtual User CreatedBy { get; set; } = null!;
    }
}
