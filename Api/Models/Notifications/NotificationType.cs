﻿namespace Jfdi.ProjectManagement.Api.Models.Notifications
{
    public enum NotificationType
    {
        /// <summary>
        /// Task completion notifications (an e-mail gets send to members assigned to the task upon it's completion).
        /// </summary>
        TaskCompletion,

        /// <summary>
        /// "approaching due date" reminder notification (e-mail gets send to the members assigned to the task based on the due date reminder set on the Tasks when scheduling Task).
        /// </summary>
        TaskDueDate,

        /// <summary>
        /// Notifications when someone assings you a new task.
        /// </summary>
        TaskAssigning,

        /// <summary>
        ///  Task is overdue (when Task becomes overdue, you should receive a notification e-mail that the task is now overdue).
        /// </summary>
        TaskOverdue
    }
}
