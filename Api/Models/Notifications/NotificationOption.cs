﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Models.Notifications
{
    [Index(nameof(NotificationType), "TeamMemberId", IsUnique = true)]
    public class NotificationOption : Entity
    {
        public NotificationOption(Guid id)
            : base(id)
        {
        }

        public TeamMember TeamMember { get; set; } = null!;

        public NotificationType NotificationType { get; set; }

        public bool IsOn { get; set; }
    }
}
