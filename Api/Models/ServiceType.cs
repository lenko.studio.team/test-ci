﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class ServiceType : Entity
    {
        public ServiceType(Guid id, string name, string icon, string fillIcon, string color)
            : base(id)
        {
            Name = name;
            Icon = icon;
            Color = color;
            FillIcon = fillIcon;
        }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Icon { get; set; }

        [StringLength(255)]
        public string FillIcon { get; set; }

        [StringLength(10)]
        public string Color { get; set; }

        [NotMapped]
        public IList<Service> Services { get; set; } = new List<Service>();
    }
}
