﻿using System;

namespace Jfdi.ProjectManagement.Api.Models.Attachments
{
    public class GDriveAttachment : Attachment
    {
        public GDriveAttachment(Guid id, DateTime createdAtUtc, Guid createdById, string link)
            : base(id, createdAtUtc, createdById)
        {
            Link = link;
            Type = AttachmentType.GoogleDrive;
        }

        public string Link { get; set; }
    }
}
