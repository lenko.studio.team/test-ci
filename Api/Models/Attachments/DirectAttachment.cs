﻿using System;

namespace Jfdi.ProjectManagement.Api.Models.Attachments
{
    public class DirectAttachment : Attachment
    {
        public DirectAttachment(Guid id, DateTime createdAtUtc, Guid createdById, byte[] data)
            : base(id, createdAtUtc, createdById)
        {
            Data = data;
            Type = AttachmentType.Direct;
        }

        public byte[] Data { get; set; }
    }
}
