﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class Attachment : AuditableEntity
    {
        public Attachment(
            Guid id,
            DateTime createdAtUtc,
            Guid createdById)
            : base(id, createdAtUtc, createdById)
        {
        }

        [StringLength(255)]
        public string? Name { get; set; }

        public AttachmentType Type { get; set; }
    }
}
