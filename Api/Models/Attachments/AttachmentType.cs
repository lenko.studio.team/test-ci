﻿namespace Jfdi.ProjectManagement.Api.Models
{
    public enum AttachmentType
    {
        /// <summary>
        /// Base attachment.
        /// </summary>
        None,

        /// <summary>
        /// Load directly.
        /// </summary>
        Direct,

        /// <summary>
        /// Google Drive file.
        /// </summary>
        GoogleDrive
    }
}
