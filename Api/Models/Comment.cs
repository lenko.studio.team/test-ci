﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class Comment : AuditableEntity
    {
        public Comment(Guid id, DateTime createdAtUtc, Guid createdById, string data)
            : base(id, createdAtUtc, createdById)
        {
            Data = data;
        }

        [StringLength(255)]
        public string Data { get; set; }
    }
}
