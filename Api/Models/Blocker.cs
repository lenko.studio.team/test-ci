﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class Blocker : AuditableEntity
    {
        public Blocker(
            string title,
            Guid id,
            DateTime createdAtUtc,
            Guid createdById)
            : base(
                id,
                createdAtUtc,
                createdById)
        {
            Title = title;
        }

        [StringLength(255)]
        public string Title { get; set; }

        public string? Description { get; set; }

        public bool IsHighPriority { get; set; }

        public bool IsDone { get; set; } = false;

        public ServiceInstance? Service { get; set; }

        public TaskEvent Task { get; set; } = null!;

        public CalendarEvent CalendarEvent { get; set; } = null!;

        public TeamMember? AssignedUser { get; set; }
    }
}
