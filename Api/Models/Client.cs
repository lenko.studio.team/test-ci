﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Microsoft.EntityFrameworkCore;

namespace Jfdi.ProjectManagement.Api.Models
{
    [Index(nameof(CompanyName), IsUnique = true)]
    public class Client : AuditableEntity, ISoftArchive
    {
        public Client(
            Guid id,
            DateTime createdAtUtc,
            Guid createdById,
            string companyName)
            : base(
                id,
                createdAtUtc,
                createdById)
        {
            CompanyName = companyName;
        }

        public Calendar Calendar { get; set; } = null!;

        [StringLength(255)]
        public string CompanyName { get; set; }

        [StringLength(255)]
        public TaxClassification? TaxClassification { get; set; }

        [StringLength(255)]
        public string? PhysicalAddress { get; set; }

        [StringLength(255)]
        public string? MailingAddress { get; set; }

        [StringLength(255)]
        public string? CompanyEin { get; set; }

        [StringLength(255)]
        public string? StateRegistration { get; set; }

        [StringLength(255)]
        public string? CityRegistration { get; set; }

        [StringLength(255)]
        public string? Duns { get; set; }

        public DateTime? DateFormed { get; set; }

        [StringLength(255)]
        public string? President { get; set; }

        [StringLength(255)]
        public string? Tresurer { get; set; }

        [StringLength(255)]
        public string? RegisteredAgent { get; set; }

        [StringLength(255)]
        public string? Phone { get; set; }

        [StringLength(255)]
        public string? ClientTag { get; set; }

        public DateTime? ServiceStartDate { get; set; }

        public virtual ICollection<TeamMember> TeamMemberAssignments { get; set; } = new Collection<TeamMember>();

        [StringLength(255)]
        public string? OnePasswordClientAcronoym { get; set; }

        public IEnumerable<string>? ClientJfdiEmails { get; set; }

        public DateTime? YearEndDate { get; set; }

        public DateTime? InitialTransactionProcessingDate { get; set; }

        [StringLength(255)]
        public IEnumerable<string>? Software { get; set; }

        public virtual ICollection<ServiceInstance> Services { get; set; } = new Collection<ServiceInstance>();

        public ArchiveData? ArchiveInfo { get; set; }
    }
}
