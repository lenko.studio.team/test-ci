﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Jfdi.ProjectManagement.Api.Models.Archives;

namespace Jfdi.ProjectManagement.Api.Models
{
    public class TaskEvent : Entity, ISoftArchive
    {
        public TaskEvent(Guid id)
            : base(id)
        {
        }

        public TaskInstance Instance { get; set; } = null!;

        [IsUtc]
        public DateTime StartDateUtc { get; set; }

        [IsUtc]
        public DateTime EndDateUtc { get; set; }

        public TaskState State { get; set; } = null!;

        public virtual ICollection<Blocker> Blockers { get; set; } = new Collection<Blocker>();

        public ICollection<Comment> Comments { get; set; } = new Collection<Comment>();

        public ICollection<TaskEvent> Children { get; set; } = new Collection<TaskEvent>();

        public TaskEvent? Parent { get; set; }

        public ArchiveData? ArchiveInfo { get; set; }
    }
}
