﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class TaskAssign : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TeamMembers_Clients_ClientId",
                table: "TeamMembers");

            migrationBuilder.DropIndex(
                name: "IX_TeamMembers_ClientId",
                table: "TeamMembers");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "TeamMembers");

            migrationBuilder.CreateTable(
                name: "ClientTeamMember",
                columns: table => new
                {
                    ClientsId = table.Column<Guid>(type: "uuid", nullable: false),
                    TeamMemberAssignmentsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientTeamMember", x => new { x.ClientsId, x.TeamMemberAssignmentsId });
                    table.ForeignKey(
                        name: "FK_ClientTeamMember_Clients_ClientsId",
                        column: x => x.ClientsId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClientTeamMember_TeamMembers_TeamMemberAssignmentsId",
                        column: x => x.TeamMemberAssignmentsId,
                        principalTable: "TeamMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaskInstanceTeamMember",
                columns: table => new
                {
                    AssigneesId = table.Column<Guid>(type: "uuid", nullable: false),
                    AssignesTasksId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskInstanceTeamMember", x => new { x.AssigneesId, x.AssignesTasksId });
                    table.ForeignKey(
                        name: "FK_TaskInstanceTeamMember_TaskInstances_AssignesTasksId",
                        column: x => x.AssignesTasksId,
                        principalTable: "TaskInstances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskInstanceTeamMember_TeamMembers_AssigneesId",
                        column: x => x.AssigneesId,
                        principalTable: "TeamMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClientTeamMember_TeamMemberAssignmentsId",
                table: "ClientTeamMember",
                column: "TeamMemberAssignmentsId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskInstanceTeamMember_AssignesTasksId",
                table: "TaskInstanceTeamMember",
                column: "AssignesTasksId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientTeamMember");

            migrationBuilder.DropTable(
                name: "TaskInstanceTeamMember");

            migrationBuilder.AddColumn<Guid>(
                name: "ClientId",
                table: "TeamMembers",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TeamMembers_ClientId",
                table: "TeamMembers",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_TeamMembers_Clients_ClientId",
                table: "TeamMembers",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
