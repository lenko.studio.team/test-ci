﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class Permissions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArchiveDate",
                table: "TeamMembers");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "TeamMembers");

            migrationBuilder.DropColumn(
                name: "IsArchive",
                table: "TeamMembers");

            migrationBuilder.DropColumn(
                name: "ArchiveDate",
                table: "TaskEvents");

            migrationBuilder.DropColumn(
                name: "IsArchive",
                table: "TaskEvents");

            migrationBuilder.DropColumn(
                name: "ArchiveDate",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "IsArchive",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ArchiveDate",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "IsArchive",
                table: "Clients");

            migrationBuilder.AddColumn<string>(
                name: "Permission",
                table: "Users",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "TeamMembers",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<Guid>(
                name: "ArchiveInfoId",
                table: "TeamMembers",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ArchiveInfoId",
                table: "TaskEvents",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ArchiveInfoId",
                table: "Services",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ArchiveInfoId",
                table: "Clients",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ArchiveData",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ArchivedById = table.Column<Guid>(type: "uuid", nullable: false),
                    ArchivedAtUtc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArchiveData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArchiveData_Users_ArchivedById",
                        column: x => x.ArchivedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TeamMembers_ArchiveInfoId",
                table: "TeamMembers",
                column: "ArchiveInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_TeamMembers_UserId",
                table: "TeamMembers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskEvents_ArchiveInfoId",
                table: "TaskEvents",
                column: "ArchiveInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_ArchiveInfoId",
                table: "Services",
                column: "ArchiveInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_ArchiveInfoId",
                table: "Clients",
                column: "ArchiveInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_ArchiveData_ArchivedById",
                table: "ArchiveData",
                column: "ArchivedById");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_ArchiveData_ArchiveInfoId",
                table: "Clients",
                column: "ArchiveInfoId",
                principalTable: "ArchiveData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ArchiveData_ArchiveInfoId",
                table: "Services",
                column: "ArchiveInfoId",
                principalTable: "ArchiveData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskEvents_ArchiveData_ArchiveInfoId",
                table: "TaskEvents",
                column: "ArchiveInfoId",
                principalTable: "ArchiveData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TeamMembers_ArchiveData_ArchiveInfoId",
                table: "TeamMembers",
                column: "ArchiveInfoId",
                principalTable: "ArchiveData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TeamMembers_Users_UserId",
                table: "TeamMembers",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_ArchiveData_ArchiveInfoId",
                table: "Clients");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_ArchiveData_ArchiveInfoId",
                table: "Services");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskEvents_ArchiveData_ArchiveInfoId",
                table: "TaskEvents");

            migrationBuilder.DropForeignKey(
                name: "FK_TeamMembers_ArchiveData_ArchiveInfoId",
                table: "TeamMembers");

            migrationBuilder.DropForeignKey(
                name: "FK_TeamMembers_Users_UserId",
                table: "TeamMembers");

            migrationBuilder.DropTable(
                name: "ArchiveData");

            migrationBuilder.DropIndex(
                name: "IX_TeamMembers_ArchiveInfoId",
                table: "TeamMembers");

            migrationBuilder.DropIndex(
                name: "IX_TeamMembers_UserId",
                table: "TeamMembers");

            migrationBuilder.DropIndex(
                name: "IX_TaskEvents_ArchiveInfoId",
                table: "TaskEvents");

            migrationBuilder.DropIndex(
                name: "IX_Services_ArchiveInfoId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Clients_ArchiveInfoId",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Permission",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ArchiveInfoId",
                table: "TeamMembers");

            migrationBuilder.DropColumn(
                name: "ArchiveInfoId",
                table: "TaskEvents");

            migrationBuilder.DropColumn(
                name: "ArchiveInfoId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ArchiveInfoId",
                table: "Clients");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "TeamMembers",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchiveDate",
                table: "TeamMembers",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "TeamMembers",
                type: "character varying(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "IsArchive",
                table: "TeamMembers",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchiveDate",
                table: "TaskEvents",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchive",
                table: "TaskEvents",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchiveDate",
                table: "Services",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchive",
                table: "Services",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchiveDate",
                table: "Clients",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchive",
                table: "Clients",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }
    }
}
