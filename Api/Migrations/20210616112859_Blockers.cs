﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1122:Use string.Empty for empty strings", Justification = "<Pending>")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1118:Parameter should not span multiple lines", Justification = "<Pending>")]
    public partial class Blockers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskInstances_ServiceInstances_ServiceId",
                table: "TaskInstances");

            migrationBuilder.RenameColumn(
                name: "Title",
                table: "Services",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Title",
                table: "Clients",
                newName: "CompanyName");

            migrationBuilder.RenameIndex(
                name: "IX_Clients_Title",
                table: "Clients",
                newName: "IX_Clients_CompanyName");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "character varying(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<DateTime>(
                name: "DueDateUtc",
                table: "Tasks",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "ForReview",
                table: "Tasks",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Info",
                table: "Tasks",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ParentId",
                table: "Tasks",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Recurring",
                table: "Tasks",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Role",
                table: "Tasks",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "SupervisorId",
                table: "Tasks",
                type: "uuid",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "ServiceId",
                table: "TaskInstances",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddColumn<DateTime>(
                name: "DueDateUtc",
                table: "TaskInstances",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsHighPriority",
                table: "TaskInstances",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Links",
                table: "TaskInstances",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "TaskInstances",
                type: "character varying(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "ParentId",
                table: "TaskInstances",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SoftwareId",
                table: "TaskInstances",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Services",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsHighPriority",
                table: "Services",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Links",
                table: "Services",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Recurring",
                table: "Services",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "ServiceTypeId",
                table: "Services",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ServiceInstances",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsHighPriority",
                table: "ServiceInstances",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Links",
                table: "ServiceInstances",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "ServiceInstances",
                type: "character varying(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<uint>(
                name: "xmin",
                table: "ServiceInstances",
                type: "xid",
                rowVersion: true,
                nullable: false,
                defaultValue: 0u);

            migrationBuilder.AddColumn<string>(
                name: "CityRegistration",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClientJfdiEmails",
                table: "Clients",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClientTag",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyEin",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateFormed",
                table: "Clients",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Duns",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InitialTransactionProcessingDate",
                table: "Clients",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MailingAddress",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OnePasswordClientAcronoym",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhysicalAddress",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "President",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RegisteredAgent",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ServiceStartDate",
                table: "Clients",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Software",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StateRegistration",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TaxClassificationId",
                table: "Clients",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tresurer",
                table: "Clients",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "YearEndDate",
                table: "Clients",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Blockers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    TaskId = table.Column<Guid>(type: "uuid", nullable: false),
                    CalendarEventId = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAtUtc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedById = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blockers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Blockers_CalendarEvents_CalendarEventId",
                        column: x => x.CalendarEventId,
                        principalTable: "CalendarEvents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Blockers_TaskInstances_TaskId",
                        column: x => x.TaskId,
                        principalTable: "TaskInstances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Blockers_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Data = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    TaskInstanceId = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAtUtc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedById = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_TaskInstances_TaskInstanceId",
                        column: x => x.TaskInstanceId,
                        principalTable: "TaskInstances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Icon = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    FillIcon = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Color = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Softwares",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Links = table.Column<string>(type: "text", nullable: true),
                    ServiceId = table.Column<Guid>(type: "uuid", nullable: true),
                    ServiceInstanceId = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAtUtc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedById = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Softwares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Softwares_ServiceInstances_ServiceInstanceId",
                        column: x => x.ServiceInstanceId,
                        principalTable: "ServiceInstances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Softwares_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Softwares_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaxClassifications",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxClassifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TeamMembers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    LastName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    FirstName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Email = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    PhysicalAddress = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    SocialSecurityNumber = table.Column<int>(type: "integer", nullable: true),
                    BirthDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CompanyMailingAddress = table.Column<string>(type: "text", nullable: true),
                    CellPhone = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    FullName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Address = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Phone = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Title = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    SupervisorId = table.Column<Guid>(type: "uuid", nullable: true),
                    Salary = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    StartDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    NameOfCollege = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Degree = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Role = table.Column<string>(type: "character varying(30)", nullable: false),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    ClientId = table.Column<Guid>(type: "uuid", nullable: true),
                    xmin = table.Column<uint>(type: "xid", rowVersion: true, nullable: false),
                    CreatedAtUtc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedById = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeamMembers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TeamMembers_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TeamMembers_TeamMembers_SupervisorId",
                        column: x => x.SupervisorId,
                        principalTable: "TeamMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TeamMembers_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClientServiceType",
                columns: table => new
                {
                    ClientsId = table.Column<Guid>(type: "uuid", nullable: false),
                    ServiceTypesId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientServiceType", x => new { x.ClientsId, x.ServiceTypesId });
                    table.ForeignKey(
                        name: "FK_ClientServiceType_Clients_ClientsId",
                        column: x => x.ClientsId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClientServiceType_ServiceTypes_ServiceTypesId",
                        column: x => x.ServiceTypesId,
                        principalTable: "ServiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Attachments",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Type = table.Column<string>(type: "text", nullable: false),
                    ServiceId = table.Column<Guid>(type: "uuid", nullable: true),
                    ServiceInstanceId = table.Column<Guid>(type: "uuid", nullable: true),
                    SoftwareId = table.Column<Guid>(type: "uuid", nullable: true),
                    Data = table.Column<byte[]>(type: "bytea", nullable: true),
                    Link = table.Column<string>(type: "text", nullable: true),
                    CreatedAtUtc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedById = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attachments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Attachments_ServiceInstances_ServiceInstanceId",
                        column: x => x.ServiceInstanceId,
                        principalTable: "ServiceInstances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Attachments_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Attachments_Softwares_SoftwareId",
                        column: x => x.SoftwareId,
                        principalTable: "Softwares",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Attachments_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ServiceTypes",
                columns: new[] { "Id", "Color", "FillIcon", "Icon", "Name" },
                values: new object[,]
                {
                    { new Guid("a7db5845-babc-4230-ae13-ca4d865db6a6"), "#EBBB38", "bookkeeping-fill", "bookkeeping", "Bookkeeping" },
                    { new Guid("ed17ca07-fb2e-4204-86b6-9de0d0e3e5da"), "#8F56CB", "month-end-close-fill", "month-end-close", "Month End Close" },
                    { new Guid("4681430b-752d-4520-8372-6f41e3918469"), "#65A9D9", "ap-fill", "ap", "Accounts Payables" },
                    { new Guid("9e8940ec-4317-4a2b-a1dd-668827943a99"), "#5DC5BE", "ar-fill", "ar", "Accounts Receivables" },
                    { new Guid("9e8940ec-4317-4a2b-a1dd-668827943a98"), "#DB669E", "payroll-fill", "payroll", "Payroll" },
                    { new Guid("9e8940ec-4317-4a2b-a1dd-668827943a78"), "#8BC465", "reporting-fill", "reporting", "Reporting" },
                    { new Guid("9e8940ec-4317-4a2b-a1dd-668827943a10"), "#E26C35", "compliance-fill", "compliance", "Compliance" },
                    { new Guid("9e8940ec-4317-4a2b-a1dd-668827943a43"), "#707070", "project-fill", "project", "Special Projects" }
                });

            migrationBuilder.InsertData(
                table: "TaxClassifications",
                columns: new[] { "Id", "Title" },
                values: new object[,]
                {
                    { new Guid("a7db5845-babc-4230-ae13-ca4d865db6a6"), "C-Corp" },
                    { new Guid("4681430b-752d-4520-8372-6f41e3918469"), "Partnership" },
                    { new Guid("ed17ca07-fb2e-4204-86b6-9de0d0e3e5da"), "S-Corp" },
                    { new Guid("9e8940ec-4317-4a2b-a1dd-668827943a99"), "Single member LLC" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ParentId",
                table: "Tasks",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_SupervisorId",
                table: "Tasks",
                column: "SupervisorId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskInstances_ParentId",
                table: "TaskInstances",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskInstances_SoftwareId",
                table: "TaskInstances",
                column: "SoftwareId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_ServiceTypeId",
                table: "Services",
                column: "ServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_TaxClassificationId",
                table: "Clients",
                column: "TaxClassificationId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_CreatedById",
                table: "Attachments",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_ServiceId",
                table: "Attachments",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_ServiceInstanceId",
                table: "Attachments",
                column: "ServiceInstanceId");

            migrationBuilder.CreateIndex(
                name: "IX_Attachments_SoftwareId",
                table: "Attachments",
                column: "SoftwareId");

            migrationBuilder.CreateIndex(
                name: "IX_Blockers_CalendarEventId",
                table: "Blockers",
                column: "CalendarEventId");

            migrationBuilder.CreateIndex(
                name: "IX_Blockers_CreatedById",
                table: "Blockers",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Blockers_TaskId",
                table: "Blockers",
                column: "TaskId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientServiceType_ServiceTypesId",
                table: "ClientServiceType",
                column: "ServiceTypesId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_CreatedById",
                table: "Comments",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_TaskInstanceId",
                table: "Comments",
                column: "TaskInstanceId");

            migrationBuilder.CreateIndex(
                name: "IX_Softwares_CreatedById",
                table: "Softwares",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Softwares_ServiceId",
                table: "Softwares",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_Softwares_ServiceInstanceId",
                table: "Softwares",
                column: "ServiceInstanceId");

            migrationBuilder.CreateIndex(
                name: "IX_TeamMembers_ClientId",
                table: "TeamMembers",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_TeamMembers_CreatedById",
                table: "TeamMembers",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_TeamMembers_SupervisorId",
                table: "TeamMembers",
                column: "SupervisorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_TaxClassifications_TaxClassificationId",
                table: "Clients",
                column: "TaxClassificationId",
                principalTable: "TaxClassifications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_ServiceTypes_ServiceTypeId",
                table: "Services",
                column: "ServiceTypeId",
                principalTable: "ServiceTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInstances_ServiceInstances_ServiceId",
                table: "TaskInstances",
                column: "ServiceId",
                principalTable: "ServiceInstances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInstances_Softwares_SoftwareId",
                table: "TaskInstances",
                column: "SoftwareId",
                principalTable: "Softwares",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInstances_TaskInstances_ParentId",
                table: "TaskInstances",
                column: "ParentId",
                principalTable: "TaskInstances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Tasks_ParentId",
                table: "Tasks",
                column: "ParentId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_TeamMembers_SupervisorId",
                table: "Tasks",
                column: "SupervisorId",
                principalTable: "TeamMembers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_TaxClassifications_TaxClassificationId",
                table: "Clients");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_ServiceTypes_ServiceTypeId",
                table: "Services");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskInstances_ServiceInstances_ServiceId",
                table: "TaskInstances");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskInstances_Softwares_SoftwareId",
                table: "TaskInstances");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskInstances_TaskInstances_ParentId",
                table: "TaskInstances");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_Tasks_ParentId",
                table: "Tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_TeamMembers_SupervisorId",
                table: "Tasks");

            migrationBuilder.DropTable(
                name: "Attachments");

            migrationBuilder.DropTable(
                name: "Blockers");

            migrationBuilder.DropTable(
                name: "ClientServiceType");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "TaxClassifications");

            migrationBuilder.DropTable(
                name: "TeamMembers");

            migrationBuilder.DropTable(
                name: "Softwares");

            migrationBuilder.DropTable(
                name: "ServiceTypes");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_ParentId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_SupervisorId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_TaskInstances_ParentId",
                table: "TaskInstances");

            migrationBuilder.DropIndex(
                name: "IX_TaskInstances_SoftwareId",
                table: "TaskInstances");

            migrationBuilder.DropIndex(
                name: "IX_Services_ServiceTypeId",
                table: "Services");

            migrationBuilder.DropIndex(
                name: "IX_Clients_TaxClassificationId",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "DueDateUtc",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "ForReview",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "Info",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "Recurring",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "Role",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "SupervisorId",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "DueDateUtc",
                table: "TaskInstances");

            migrationBuilder.DropColumn(
                name: "IsHighPriority",
                table: "TaskInstances");

            migrationBuilder.DropColumn(
                name: "Links",
                table: "TaskInstances");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "TaskInstances");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "TaskInstances");

            migrationBuilder.DropColumn(
                name: "SoftwareId",
                table: "TaskInstances");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "IsHighPriority",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "Links",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "Recurring",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ServiceTypeId",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "ServiceInstances");

            migrationBuilder.DropColumn(
                name: "IsHighPriority",
                table: "ServiceInstances");

            migrationBuilder.DropColumn(
                name: "Links",
                table: "ServiceInstances");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "ServiceInstances");

            migrationBuilder.DropColumn(
                name: "xmin",
                table: "ServiceInstances");

            migrationBuilder.DropColumn(
                name: "CityRegistration",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "ClientJfdiEmails",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "ClientTag",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CompanyEin",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "DateFormed",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Duns",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "InitialTransactionProcessingDate",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "MailingAddress",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "OnePasswordClientAcronoym",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "PhysicalAddress",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "President",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "RegisteredAgent",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "ServiceStartDate",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Software",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "StateRegistration",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "TaxClassificationId",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Tresurer",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "YearEndDate",
                table: "Clients");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Services",
                newName: "Title");

            migrationBuilder.RenameColumn(
                name: "CompanyName",
                table: "Clients",
                newName: "Title");

            migrationBuilder.RenameIndex(
                name: "IX_Clients_CompanyName",
                table: "Clients",
                newName: "IX_Clients_Title");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<Guid>(
                name: "ServiceId",
                table: "TaskInstances",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInstances_ServiceInstances_ServiceId",
                table: "TaskInstances",
                column: "ServiceId",
                principalTable: "ServiceInstances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
