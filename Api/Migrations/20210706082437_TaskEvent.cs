﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class TaskEvent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Blockers_TaskInstances_TaskId",
                table: "Blockers");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_TaskInstances_TaskInstanceId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskInstances_TaskStates_StateId",
                table: "TaskInstances");

            migrationBuilder.DropIndex(
                name: "IX_TaskInstances_StateId",
                table: "TaskInstances");

            migrationBuilder.DropColumn(
                name: "StateId",
                table: "TaskInstances");

            migrationBuilder.RenameColumn(
                name: "TaskInstanceId",
                table: "Comments",
                newName: "TaskEventId");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_TaskInstanceId",
                table: "Comments",
                newName: "IX_Comments_TaskEventId");

            migrationBuilder.CreateTable(
                name: "TaskEvents",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    InstanceId = table.Column<Guid>(type: "uuid", nullable: false),
                    StartDateUtc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    EndDateUtc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    StateId = table.Column<Guid>(type: "uuid", nullable: true),
                    ParentId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskEvents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaskEvents_TaskEvents_ParentId",
                        column: x => x.ParentId,
                        principalTable: "TaskEvents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaskEvents_TaskInstances_InstanceId",
                        column: x => x.InstanceId,
                        principalTable: "TaskInstances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskEvents_TaskStates_StateId",
                        column: x => x.StateId,
                        principalTable: "TaskStates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TaskEvents_InstanceId",
                table: "TaskEvents",
                column: "InstanceId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskEvents_ParentId",
                table: "TaskEvents",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskEvents_StateId",
                table: "TaskEvents",
                column: "StateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Blockers_TaskEvents_TaskId",
                table: "Blockers",
                column: "TaskId",
                principalTable: "TaskEvents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_TaskEvents_TaskEventId",
                table: "Comments",
                column: "TaskEventId",
                principalTable: "TaskEvents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Blockers_TaskEvents_TaskId",
                table: "Blockers");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_TaskEvents_TaskEventId",
                table: "Comments");

            migrationBuilder.DropTable(
                name: "TaskEvents");

            migrationBuilder.RenameColumn(
                name: "TaskEventId",
                table: "Comments",
                newName: "TaskInstanceId");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_TaskEventId",
                table: "Comments",
                newName: "IX_Comments_TaskInstanceId");

            migrationBuilder.AddColumn<Guid>(
                name: "StateId",
                table: "TaskInstances",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaskInstances_StateId",
                table: "TaskInstances",
                column: "StateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Blockers_TaskInstances_TaskId",
                table: "Blockers",
                column: "TaskId",
                principalTable: "TaskInstances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_TaskInstances_TaskInstanceId",
                table: "Comments",
                column: "TaskInstanceId",
                principalTable: "TaskInstances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInstances_TaskStates_StateId",
                table: "TaskInstances",
                column: "StateId",
                principalTable: "TaskStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
