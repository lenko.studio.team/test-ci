﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class ServiceInstancies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceInstances_Services_TemplateId",
                table: "ServiceInstances");

            migrationBuilder.AlterColumn<Guid>(
                name: "TemplateId",
                table: "ServiceInstances",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceInstances_Services_TemplateId",
                table: "ServiceInstances",
                column: "TemplateId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceInstances_Services_TemplateId",
                table: "ServiceInstances");

            migrationBuilder.AlterColumn<Guid>(
                name: "TemplateId",
                table: "ServiceInstances",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceInstances_Services_TemplateId",
                table: "ServiceInstances",
                column: "TemplateId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
