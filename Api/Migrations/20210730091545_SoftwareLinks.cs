﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class SoftwareLinks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ServiceInstanceSoftware",
                columns: table => new
                {
                    ServiceInstanciesId = table.Column<Guid>(type: "uuid", nullable: false),
                    SoftwaresId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceInstanceSoftware", x => new { x.ServiceInstanciesId, x.SoftwaresId });
                    table.ForeignKey(
                        name: "FK_ServiceInstanceSoftware_ServiceInstances_ServiceInstanciesId",
                        column: x => x.ServiceInstanciesId,
                        principalTable: "ServiceInstances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceInstanceSoftware_Softwares_SoftwaresId",
                        column: x => x.SoftwaresId,
                        principalTable: "Softwares",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceSoftware",
                columns: table => new
                {
                    ServicesId = table.Column<Guid>(type: "uuid", nullable: false),
                    SoftwaresId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceSoftware", x => new { x.ServicesId, x.SoftwaresId });
                    table.ForeignKey(
                        name: "FK_ServiceSoftware_Services_ServicesId",
                        column: x => x.ServicesId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceSoftware_Softwares_SoftwaresId",
                        column: x => x.SoftwaresId,
                        principalTable: "Softwares",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceInstanceSoftware_SoftwaresId",
                table: "ServiceInstanceSoftware",
                column: "SoftwaresId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceSoftware_SoftwaresId",
                table: "ServiceSoftware",
                column: "SoftwaresId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ServiceInstanceSoftware");

            migrationBuilder.DropTable(
                name: "ServiceSoftware");

            migrationBuilder.AddColumn<Guid>(
                name: "ServiceId",
                table: "Softwares",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ServiceInstanceId",
                table: "Softwares",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Softwares_ServiceId",
                table: "Softwares",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_Softwares_ServiceInstanceId",
                table: "Softwares",
                column: "ServiceInstanceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Softwares_ServiceInstances_ServiceInstanceId",
                table: "Softwares",
                column: "ServiceInstanceId",
                principalTable: "ServiceInstances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Softwares_Services_ServiceId",
                table: "Softwares",
                column: "ServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
