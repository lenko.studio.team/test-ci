﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class TaskSupervisiors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskInstanceTeamMember_TaskInstances_AssignesTasksId",
                table: "TaskInstanceTeamMember");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskInstanceTeamMember_TeamMembers_AssigneesId",
                table: "TaskInstanceTeamMember");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TaskInstanceTeamMember",
                table: "TaskInstanceTeamMember");

            migrationBuilder.RenameTable(
                name: "TaskInstanceTeamMember",
                newName: "AssigneeTasks");

            migrationBuilder.RenameIndex(
                name: "IX_TaskInstanceTeamMember_AssignesTasksId",
                table: "AssigneeTasks",
                newName: "IX_AssigneeTasks_AssignesTasksId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AssigneeTasks",
                table: "AssigneeTasks",
                columns: new[] { "AssigneesId", "AssignesTasksId" });

            migrationBuilder.CreateTable(
                name: "SupervisorTasks",
                columns: table => new
                {
                    SupervisorTasksId = table.Column<Guid>(type: "uuid", nullable: false),
                    SupervisorsId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupervisorTasks", x => new { x.SupervisorTasksId, x.SupervisorsId });
                    table.ForeignKey(
                        name: "FK_SupervisorTasks_TaskInstances_SupervisorTasksId",
                        column: x => x.SupervisorTasksId,
                        principalTable: "TaskInstances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SupervisorTasks_TeamMembers_SupervisorsId",
                        column: x => x.SupervisorsId,
                        principalTable: "TeamMembers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SupervisorTasks_SupervisorsId",
                table: "SupervisorTasks",
                column: "SupervisorsId");

            migrationBuilder.AddForeignKey(
                name: "FK_AssigneeTasks_TaskInstances_AssignesTasksId",
                table: "AssigneeTasks",
                column: "AssignesTasksId",
                principalTable: "TaskInstances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AssigneeTasks_TeamMembers_AssigneesId",
                table: "AssigneeTasks",
                column: "AssigneesId",
                principalTable: "TeamMembers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AssigneeTasks_TaskInstances_AssignesTasksId",
                table: "AssigneeTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_AssigneeTasks_TeamMembers_AssigneesId",
                table: "AssigneeTasks");

            migrationBuilder.DropTable(
                name: "SupervisorTasks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AssigneeTasks",
                table: "AssigneeTasks");

            migrationBuilder.RenameTable(
                name: "AssigneeTasks",
                newName: "TaskInstanceTeamMember");

            migrationBuilder.RenameIndex(
                name: "IX_AssigneeTasks_AssignesTasksId",
                table: "TaskInstanceTeamMember",
                newName: "IX_TaskInstanceTeamMember_AssignesTasksId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TaskInstanceTeamMember",
                table: "TaskInstanceTeamMember",
                columns: new[] { "AssigneesId", "AssignesTasksId" });

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInstanceTeamMember_TaskInstances_AssignesTasksId",
                table: "TaskInstanceTeamMember",
                column: "AssignesTasksId",
                principalTable: "TaskInstances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInstanceTeamMember_TeamMembers_AssigneesId",
                table: "TaskInstanceTeamMember",
                column: "AssigneesId",
                principalTable: "TeamMembers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
