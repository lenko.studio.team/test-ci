﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class AddFieldsToBlockers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TeamMembers_UserId",
                table: "TeamMembers");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "TeamMembers",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "AssignedUserId",
                table: "Blockers",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Blockers",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDone",
                table: "Blockers",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsHighPriority",
                table: "Blockers",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "ServiceId",
                table: "Blockers",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TeamMembers_UserId",
                table: "TeamMembers",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Blockers_AssignedUserId",
                table: "Blockers",
                column: "AssignedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Blockers_ServiceId",
                table: "Blockers",
                column: "ServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Blockers_ServiceInstances_ServiceId",
                table: "Blockers",
                column: "ServiceId",
                principalTable: "ServiceInstances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Blockers_TeamMembers_AssignedUserId",
                table: "Blockers",
                column: "AssignedUserId",
                principalTable: "TeamMembers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Blockers_ServiceInstances_ServiceId",
                table: "Blockers");

            migrationBuilder.DropForeignKey(
                name: "FK_Blockers_TeamMembers_AssignedUserId",
                table: "Blockers");

            migrationBuilder.DropIndex(
                name: "IX_TeamMembers_UserId",
                table: "TeamMembers");

            migrationBuilder.DropIndex(
                name: "IX_Blockers_AssignedUserId",
                table: "Blockers");

            migrationBuilder.DropIndex(
                name: "IX_Blockers_ServiceId",
                table: "Blockers");

            migrationBuilder.DropColumn(
                name: "AssignedUserId",
                table: "Blockers");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Blockers");

            migrationBuilder.DropColumn(
                name: "IsDone",
                table: "Blockers");

            migrationBuilder.DropColumn(
                name: "IsHighPriority",
                table: "Blockers");

            migrationBuilder.DropColumn(
                name: "ServiceId",
                table: "Blockers");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "TeamMembers",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.CreateIndex(
                name: "IX_TeamMembers_UserId",
                table: "TeamMembers",
                column: "UserId");
        }
    }
}
