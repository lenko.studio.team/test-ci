﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class TaskAndServiceEvent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_TeamMembers_SupervisorId",
                table: "Tasks");

            migrationBuilder.DropIndex(
                name: "IX_Tasks_SupervisorId",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "DueDateUtc",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "Recurring",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "SupervisorId",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "DueDateUtc",
                table: "TaskInstances");

            migrationBuilder.DropColumn(
                name: "Recurring",
                table: "Services");

            migrationBuilder.AddColumn<string>(
                name: "RecurrenceRule",
                table: "Tasks",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDateUtc",
                table: "Services",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "RecurrenceRule",
                table: "Services",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDateUtc",
                table: "Services",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "EventId",
                table: "ServiceInstances",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceInstances_EventId",
                table: "ServiceInstances",
                column: "EventId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceInstances_CalendarEvents_EventId",
                table: "ServiceInstances",
                column: "EventId",
                principalTable: "CalendarEvents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceInstances_CalendarEvents_EventId",
                table: "ServiceInstances");

            migrationBuilder.DropIndex(
                name: "IX_ServiceInstances_EventId",
                table: "ServiceInstances");

            migrationBuilder.DropColumn(
                name: "RecurrenceRule",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "EndDateUtc",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "RecurrenceRule",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "StartDateUtc",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "EventId",
                table: "ServiceInstances");

            migrationBuilder.AddColumn<DateTime>(
                name: "DueDateUtc",
                table: "Tasks",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Recurring",
                table: "Tasks",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "SupervisorId",
                table: "Tasks",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DueDateUtc",
                table: "TaskInstances",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Recurring",
                table: "Services",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_SupervisorId",
                table: "Tasks",
                column: "SupervisorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_TeamMembers_SupervisorId",
                table: "Tasks",
                column: "SupervisorId",
                principalTable: "TeamMembers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
