﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class ServiceInstanceAndTaskInstance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndUtc",
                table: "Tasks");

            migrationBuilder.AddColumn<Guid>(
                name: "CalendarId",
                table: "Clients",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ServiceInstances",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    TemplateId = table.Column<Guid>(type: "uuid", nullable: true),
                    ClientId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAtUtc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedById = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceInstances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceInstances_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceInstances_Services_TemplateId",
                        column: x => x.TemplateId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceInstances_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaskInstances",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    EventId = table.Column<Guid>(type: "uuid", nullable: true),
                    ServiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    TemplateId = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAtUtc = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedById = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskInstances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaskInstances_CalendarEvents_EventId",
                        column: x => x.EventId,
                        principalTable: "CalendarEvents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaskInstances_ServiceInstances_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "ServiceInstances",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskInstances_Tasks_TemplateId",
                        column: x => x.TemplateId,
                        principalTable: "Tasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaskInstances_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Clients_CalendarId",
                table: "Clients",
                column: "CalendarId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceInstances_ClientId",
                table: "ServiceInstances",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceInstances_CreatedById",
                table: "ServiceInstances",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceInstances_TemplateId",
                table: "ServiceInstances",
                column: "TemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskInstances_CreatedById",
                table: "TaskInstances",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_TaskInstances_EventId",
                table: "TaskInstances",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskInstances_ServiceId",
                table: "TaskInstances",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskInstances_TemplateId",
                table: "TaskInstances",
                column: "TemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Calendars_CalendarId",
                table: "Clients",
                column: "CalendarId",
                principalTable: "Calendars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Calendars_CalendarId",
                table: "Clients");

            migrationBuilder.DropTable(
                name: "TaskInstances");

            migrationBuilder.DropTable(
                name: "ServiceInstances");

            migrationBuilder.DropIndex(
                name: "IX_Clients_CalendarId",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CalendarId",
                table: "Clients");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndUtc",
                table: "Tasks",
                type: "timestamp without time zone",
                maxLength: 255,
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
