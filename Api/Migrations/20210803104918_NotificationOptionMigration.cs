﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class NotificationOptionMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\"");
            migrationBuilder.Sql("INSERT INTO public.\"NotificationOptions\" (\"Id\", \"TeamMemberId\", \"IsOn\", \"NotificationType\") SELECT uuid_generate_v4(), \"Id\", 'true', 'TaskCompletion' FROM public.\"TeamMembers\"");
            migrationBuilder.Sql("INSERT INTO public.\"NotificationOptions\" (\"Id\", \"TeamMemberId\", \"IsOn\", \"NotificationType\") SELECT uuid_generate_v4(), \"Id\", 'true', 'TaskDueDate' FROM public.\"TeamMembers\"");
            migrationBuilder.Sql("INSERT INTO public.\"NotificationOptions\" (\"Id\", \"TeamMemberId\", \"IsOn\", \"NotificationType\") SELECT uuid_generate_v4(), \"Id\", 'true', 'TaskAssigning' FROM public.\"TeamMembers\"");
            migrationBuilder.Sql("INSERT INTO public.\"NotificationOptions\" (\"Id\", \"TeamMemberId\", \"IsOn\", \"NotificationType\") SELECT uuid_generate_v4(), \"Id\", 'true', 'TaskOverdue' FROM public.\"TeamMembers\"");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
