﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1118:Parameter should not span multiple lines", Justification = "<Pending>")]
    public partial class TaskStates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "StateId",
                table: "TaskInstances",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TaskStates",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskStates", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Title" },
                values: new object[,]
                {
                    { new Guid("a7db5845-babc-4230-ae13-ca4d865db6a6"), "TO-DO" },
                    { new Guid("b62cb356-de5a-444b-85d4-4c5ca05793b6"), "In progress" },
                    { new Guid("5994906d-4759-4a07-b6eb-62ba7348ea5e"), "Blocked" },
                    { new Guid("0366dac4-b435-475d-ba85-96ba8a112981"), "For review" },
                    { new Guid("f6550273-0245-4180-b5e6-425c88d892bd"), "Done" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_TaskInstances_StateId",
                table: "TaskInstances",
                column: "StateId");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskInstances_TaskStates_StateId",
                table: "TaskInstances",
                column: "StateId",
                principalTable: "TaskStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskInstances_TaskStates_StateId",
                table: "TaskInstances");

            migrationBuilder.DropTable(
                name: "TaskStates");

            migrationBuilder.DropIndex(
                name: "IX_TaskInstances_StateId",
                table: "TaskInstances");

            migrationBuilder.DropColumn(
                name: "StateId",
                table: "TaskInstances");
        }
    }
}
