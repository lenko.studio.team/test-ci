﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class ArchivingTaskTeammember : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Role",
                table: "Tasks");

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchiveDate",
                table: "TeamMembers",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchive",
                table: "TeamMembers",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchiveDate",
                table: "TaskEvents",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchive",
                table: "TaskEvents",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchiveDate",
                table: "Services",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchive",
                table: "Services",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchiveDate",
                table: "Clients",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsArchive",
                table: "Clients",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArchiveDate",
                table: "TeamMembers");

            migrationBuilder.DropColumn(
                name: "IsArchive",
                table: "TeamMembers");

            migrationBuilder.DropColumn(
                name: "ArchiveDate",
                table: "TaskEvents");

            migrationBuilder.DropColumn(
                name: "IsArchive",
                table: "TaskEvents");

            migrationBuilder.DropColumn(
                name: "ArchiveDate",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "IsArchive",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "ArchiveDate",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "IsArchive",
                table: "Clients");

            migrationBuilder.AddColumn<int>(
                name: "Role",
                table: "Tasks",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
