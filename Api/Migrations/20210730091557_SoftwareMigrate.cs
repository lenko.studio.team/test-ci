﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Jfdi.ProjectManagement.Api.Migrations
{
    public partial class SoftwareMigrate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO public.\"ServiceInstanceSoftware\" (\"ServiceInstanciesId\", \"SoftwaresId\") SELECT \"ServiceInstanceId\", \"Id\" FROM public.\"Softwares\" WHERE \"ServiceInstanceId\" IS NOT NULL");
            migrationBuilder.Sql("INSERT INTO public.\"ServiceSoftware\" (\"ServicesId\", \"SoftwaresId\") SELECT \"ServiceId\", \"Id\" FROM public.\"Softwares\" WHERE \"ServiceId\" IS NOT NULL");

            migrationBuilder.DropForeignKey(
                name: "FK_Softwares_ServiceInstances_ServiceInstanceId",
                table: "Softwares");

            migrationBuilder.DropForeignKey(
                name: "FK_Softwares_Services_ServiceId",
                table: "Softwares");

            migrationBuilder.DropIndex(
                name: "IX_Softwares_ServiceId",
                table: "Softwares");

            migrationBuilder.DropIndex(
                name: "IX_Softwares_ServiceInstanceId",
                table: "Softwares");

            migrationBuilder.DropColumn(
                name: "ServiceId",
                table: "Softwares");

            migrationBuilder.DropColumn(
                name: "ServiceInstanceId",
                table: "Softwares");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
