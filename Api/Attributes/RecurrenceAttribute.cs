﻿using System;
using System.ComponentModel.DataAnnotations;
using Ical.Net.DataTypes;

namespace Jfdi.ProjectManagement.Api.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class RecurrenceAttribute : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            if (!(value is string valueString))
            {
                return new ValidationResult("Not string");
            }

            if (string.IsNullOrEmpty(valueString))
            {
                return ValidationResult.Success;
            }

            try
            {
                var pattern = new RecurrencePattern(valueString);
                return pattern.Frequency == Ical.Net.FrequencyType.None
                    ? new ValidationResult("Fail to parse")
                    : ValidationResult.Success;
            }
            catch
            {
                return new ValidationResult("Not rrule");
            }
        }
    }
}
