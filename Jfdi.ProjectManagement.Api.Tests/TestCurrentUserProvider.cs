﻿using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Models;

namespace Jfdi.ProjectManagement.Api.Tests
{
    class TestCurrentUserProvider : ICurrentUserProvider
    {
        public TestCurrentUserProvider()
        {
            var user = new User(default, "test", default, Models.Users.UserPermissions.Owner);
            User = new AuthUser(user, Models.Users.UserPermissions.Owner);
        }

        public AuthUser User { get; }
    }
}
