﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Jfdi.ProjectManagement.Api.Tests.Fixture
{
    public class TestValueContainer
    {
        private readonly Dictionary<Type, object> dict = new Dictionary<Type, object>();

        public void Add(object value)
        {
            var type = value.GetType();
            dict.Add(type, value);
        }

        public T Get<T>()
        {
            return (T)dict.Values.Single(x => x is T);
        }
    }
}
