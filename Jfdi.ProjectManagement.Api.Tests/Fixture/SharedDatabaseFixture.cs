﻿using System;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Npgsql;

namespace Jfdi.ProjectManagement.Api.Tests.Fixture
{
    [SuppressMessage("Major Code Smell", "S3881:\"IDisposable\" should be implemented correctly", Justification = "Test method, not critical")]
    public class SharedDatabaseFixture : IDisposable
    {
        private static readonly object _lock = new ();
        private static bool _databaseInitialized;

        public SharedDatabaseFixture()
        {
            var builder = new NpgsqlConnectionStringBuilder()
            {
                Host = "localhost",
                Port = 5434,
                Database = "jfdi_test",
                Username = "docker",
                Password = "secret",
                ConnectionIdleLifetime = 60,
            };

            Connection = new NpgsqlConnection(builder.ConnectionString);

            Seed();

            Connection.Open();
        }

        public DbConnection Connection { get; }

        public JfdiDbContext CreateContext(DbTransaction transaction = null)
        {
            var context = new JfdiDbContext(
                new DbContextOptionsBuilder<JfdiDbContext>().UseNpgsql(Connection).Options,
                new LoggerFactory()
            );

            if (transaction != null)
            {
                context.Database.UseTransaction(transaction);
            }

            return context;
        }

        private void Seed()
        {
            lock (_lock)
            {
                if (!_databaseInitialized)
                {
                    using (var context = CreateContext())
                    {
                        context.Database.EnsureDeleted();
                        context.Database.EnsureCreated();

                        // Seed if required
                        var testUser = new TestCurrentUserProvider().User;
                        context.Users.Add(testUser.User);

                        context.SaveChanges();

                        context.Database.ExecuteSqlRaw("UPDATE \"Users\" SET \"Id\" = '00000000-0000-0000-0000-000000000000'");
                    }

                    _databaseInitialized = true;
                }
            }
        }

        public void Dispose()
        {
            Connection.Dispose();
        }
    }
}
