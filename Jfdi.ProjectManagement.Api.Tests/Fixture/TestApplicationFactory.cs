﻿using System.IO;
using System.Linq;
using Jfdi.ProjectManagement.Api.Middleware;
using Jfdi.ProjectManagement.Api.Services;
using Jfdi.ProjectManagement.Api.Tests.Mocks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Jfdi.ProjectManagement.Api.Tests.Fixture
{
    public class TestApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.UseEnvironment("Testing");

            // Parallel test classes should not use the same DB
            var dbName = "test_db_" + Path.GetRandomFileName();

            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                        typeof(DbContextOptions<JfdiDbContext>));

                services.Remove(descriptor);

                services.AddDbContext<JfdiDbContext>(options =>
                {
                    options.UseInMemoryDatabase(dbName);
                });

                var sp = services.BuildServiceProvider();

                using var scope = sp.CreateScope();
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<JfdiDbContext>();

                db.Database.EnsureDeleted();
                db.Database.EnsureCreated();

                // Seed if required
                var testUser = new TestCurrentUserProvider().User;
                db.Users.Add(testUser.User);

                db.SaveChanges();
            });

            builder.ConfigureTestServices(services =>
            {
                services.AddTestAuth();
                services.AddKeycloakMock();
                services.Replace<ITransactionService>(new MockTransactionService());
            });
        }
    }
}
