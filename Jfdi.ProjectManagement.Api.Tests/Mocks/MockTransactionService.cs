﻿using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Services;
using Microsoft.AspNetCore.Http;

namespace Jfdi.ProjectManagement.Api.Tests.Mocks
{
    public class MockTransactionService : ITransactionService
    {
        public Task InvokeInTransaction(RequestDelegate request, HttpContext httpContext)
        {
            return request.Invoke(httpContext);
        }
    }
}
