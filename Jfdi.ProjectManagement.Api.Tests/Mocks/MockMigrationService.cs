﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace Jfdi.ProjectManagement.Api.Tests.Mocks
{
    public class MockMigrationService : BackgroundService
    {
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.Delay(1);
        }
    }
}
