﻿using System;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Users;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Users;
using Jfdi.ProjectManagement.Api.Services;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Tests.Mocks
{
    public class UserServiceMock : IUserService
    {
        public Task ChangePassword(User user, ChangePasswordDto changePasswordDto)
        {
            return Task.Delay(10);
        }

        public Task<User> CreateUser(Guid userId, CreateTeamMemberDto newTeamMember)
        {
            var user = new User(
                userId,
                newTeamMember.Email,
                DateTime.UtcNow,
                newTeamMember.Permission);
            return Task.FromResult(user);
        }

        public Task DeleteUser(Guid userKeycloakId)
        {
            return Task.Delay(10);
        }

        public Task<UserPermissions> GetPermission(Guid id)
        {
            return Task.FromResult(UserPermissions.Admin);
        }

        public Task UpdateUserCredentials(Guid userId, AuthTeamMemberDto authTeamMemberDto)
        {
            return Task.Delay(10);
        }
    }
}
