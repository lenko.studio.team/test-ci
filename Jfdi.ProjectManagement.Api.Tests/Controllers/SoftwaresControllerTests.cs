﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Attachments;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Tests.Controllers
{
    public class SoftwaresControllerTests : TestCase
    {
        public SoftwaresControllerTests(
            TestApplicationFactory<Startup> fixture,
            ITestOutputHelper testOutput)
            : base(fixture, testOutput)
        {
        }

        [Fact()]
        public async Task SoftwareExist_GetSoftware_SoftwareReturn()
        {
            // Arrange
            var software = new Software(
                "software",
                Guid.NewGuid(),
                DateTime.Now,
                default)
            {
                Links = new[] { "vk.com" }
            };

            Database(db =>
            {
                db.Softwares.Add(software);
                db.SaveChanges();
            });

            var query = $"api/softwares/{software.Id}";

            // Act
            var actualSoftware = await GetJson<SoftwareDto>(query);

            // Assert
            Assert.Equal(software.Id, actualSoftware.Id);
            Assert.Equal(software.Name, actualSoftware.Name);
            Assert.Equal(software.Description, actualSoftware.Description);
            Assert.Collection(actualSoftware.Links,
                l => Assert.Equal(software.Links.First(), l));
        }

        [Fact()]
        public async Task SoftwareExist_DeleteSoftware_Success()
        {
            // Arrange
            var software = new Software(
                "software",
                Guid.NewGuid(),
                DateTime.Now,
                default)
            {
                Links = new[] { "vk.com" }
            };

            Database(db =>
            {
                db.Softwares.Add(software);
                db.SaveChanges();
            });

            var query = $"api/softwares/{software.Id}";

            // Act
            var response = await DeleteJson(query);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            AssertDatabaseHasNot<Software>(s =>
                s.Id == software.Id);
        }

        [Fact()]
        public async Task NewSoftware_AddSoftware_Success()
        {
            // Arrange
            var attachment = new DirectAttachment(
                Guid.NewGuid(),
                DateTime.Now,
                default,
                default)
            {
                Name = "software",
            };

            Database(db =>
            {
                db.Attachments.Add(attachment);
                db.SaveChanges();
            });

            var payload = new
            {
                name = "test",
                description = "description",
                links = new[] { "facebook.com" },
                attachmentIds = new[] { attachment.Id }
            };

            var query = $"api/softwares";

            // Act
            var actualSoftwareId = await PostJson<Guid>(query, payload);

            // Assert
            Database(db =>
            {
                var actualSoftware = db.Softwares.Include(s => s.Attachments)
                    .Single(s => s.Id == actualSoftwareId);
                Assert.Equal(payload.name, actualSoftware.Name);
                Assert.Equal(payload.attachmentIds.First(), actualSoftware.Attachments.First().Id);
            });
        }

        [Fact()]
        public async Task NewSoftware_UpdateSoftware_Success()
        {
            // Arrange
            var attachment = new DirectAttachment(
                Guid.NewGuid(),
                DateTime.Now,
                default,
                default)
            {
                Name = "software",
            };
            var software = new Software(
                "software",
                Guid.NewGuid(),
                DateTime.Now,
                default)
            {
                Links = new[] { "vk.com" }
            };
            Database(db =>
            {
                db.Attachments.Add(attachment);
                db.Softwares.Add(software);
                db.SaveChanges();
            });

            var payload = new
            {
                name = "test",
                description = "description",
                links = new[] { "facebook.com" },
                attachmentIds = new[] { attachment.Id }
            };

            var query = $"api/softwares/{software.Id}";

            // Act
            var response = await PutJson(query, payload);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var actualSoftware = db.Softwares.Include(s => s.Attachments)
                    .Single(s => s.Id == software.Id);
                Assert.Equal(payload.name, actualSoftware.Name);
                Assert.Equal(payload.description, actualSoftware.Description);
                Assert.Equal(payload.attachmentIds.First(), actualSoftware.Attachments.First().Id);
            });
        }
    }
}
