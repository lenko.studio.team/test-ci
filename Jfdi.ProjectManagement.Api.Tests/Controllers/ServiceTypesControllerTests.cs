﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Jfdi.ProjectManagement.Api.Services;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Xunit;
using Xunit.Abstractions;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Tests.Controllers
{
    public class ServiceTypesControllerTests : TestCase
    {
        public ServiceTypesControllerTests(TestApplicationFactory<Startup> fixture, ITestOutputHelper testOutput)
            : base(fixture, testOutput)
        {
        }

        [Fact()]
        public async Task EmptyServices_GetServiceTypes_ReturnEmptyServices()
        {
            // Arrange
            
            // Act
            var serviceTypes = await GetJson<IEnumerable<ServiceTypeDto>>("api/serviceTypes");

            // Assert
            Assert.Collection(
                serviceTypes,
                x => AssertServiceType(IServiceTypeService.BookkeepingId, x),
                x => AssertServiceType(IServiceTypeService.MonthEndCloseId, x),
                x => AssertServiceType(IServiceTypeService.AccountsPayablesId, x),
                x => AssertServiceType(IServiceTypeService.AccountsReceivablesId, x),
                x => AssertServiceType(IServiceTypeService.PayrollId, x),
                x => AssertServiceType(IServiceTypeService.ReportingId, x),
                x => AssertServiceType(IServiceTypeService.ComplianceId, x),
                x => AssertServiceType(IServiceTypeService.SpecialProjectsId, x));

            static void AssertServiceType(Guid expectServiceTypeId, ServiceTypeDto actualServiceType)
            {
                Assert.Equal(expectServiceTypeId, actualServiceType.Id);
                Assert.Empty(actualServiceType.Services);
            }
        }

        [Fact()]
        public async Task NotEmptyServices_GetServiceTypes_ReturnServices()
        {
            // Arrange
            var range = Enumerable.Range(0, 20);
            var services = new List<Service>();
            foreach(var seed in range)
            {
                var service = new Service(
                    $"service_{seed}",
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default);
                services.Add(service);
            }

            Database(db =>
            {
                var serviceType = db.ServiceTypes.Find(IServiceTypeService.BookkeepingId);
                services.ForEach(s => s.ServiceType = serviceType);
                db.Services.AddRange(services);
                db.SaveChanges();
            });

            // Act
            var serviceTypes = await GetJson<IEnumerable<ServiceTypeDto>>("api/serviceTypes");

            // Assert
            Assert.Collection(
                serviceTypes,
                x => AssertNotEmptyServiceType(IServiceTypeService.BookkeepingId, x),
                x => AssertServiceType(IServiceTypeService.MonthEndCloseId, x),
                x => AssertServiceType(IServiceTypeService.AccountsPayablesId, x),
                x => AssertServiceType(IServiceTypeService.AccountsReceivablesId, x),
                x => AssertServiceType(IServiceTypeService.PayrollId, x),
                x => AssertServiceType(IServiceTypeService.ReportingId, x),
                x => AssertServiceType(IServiceTypeService.ComplianceId, x),
                x => AssertServiceType(IServiceTypeService.SpecialProjectsId, x));

            static void AssertServiceType(Guid expectServiceTypeId, ServiceTypeDto actualServiceType)
            {
                Assert.Equal(expectServiceTypeId, actualServiceType.Id);
                Assert.Empty(actualServiceType.Services);
            }

            static void AssertNotEmptyServiceType(Guid expectServiceTypeId, ServiceTypeDto actualServiceType)
            {
                Assert.Equal(expectServiceTypeId, actualServiceType.Id);
                Assert.NotEmpty(actualServiceType.Services);
            }
        }

        [Fact()]
        public async Task NotEmptyServicesWithArchives_GetServiceTypes_ReturnServices()
        {
            // Arrange
            var range = Enumerable.Range(0, 20);
            var services = new List<Service>();
            foreach (var seed in range)
            {
                var service = new Service(
                    $"service_{seed}",
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default);
                services.Add(service);
            }
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var archiveService = services[4];
            archiveService.ArchiveInfo = archive;
            Database(db =>
            {
                var serviceType = db.ServiceTypes.Find(IServiceTypeService.BookkeepingId);
                services.ForEach(s => s.ServiceType = serviceType);
                db.Services.AddRange(services);
                db.SaveChanges();
            });

            // Act
            var serviceTypes = await GetJson<IEnumerable<ServiceTypeDto>>("api/serviceTypes");

            // Assert
            Assert.Collection(
                serviceTypes,
                x => AssertNotEmptyServiceType(IServiceTypeService.BookkeepingId, x),
                x => AssertServiceType(IServiceTypeService.MonthEndCloseId, x),
                x => AssertServiceType(IServiceTypeService.AccountsPayablesId, x),
                x => AssertServiceType(IServiceTypeService.AccountsReceivablesId, x),
                x => AssertServiceType(IServiceTypeService.PayrollId, x),
                x => AssertServiceType(IServiceTypeService.ReportingId, x),
                x => AssertServiceType(IServiceTypeService.ComplianceId, x),
                x => AssertServiceType(IServiceTypeService.SpecialProjectsId, x));

            static void AssertServiceType(Guid expectServiceTypeId, ServiceTypeDto actualServiceType)
            {
                Assert.Equal(expectServiceTypeId, actualServiceType.Id);
                Assert.Empty(actualServiceType.Services);
            }

            static void AssertNotEmptyServiceType(Guid expectServiceTypeId, ServiceTypeDto actualServiceType)
            {
                Assert.Equal(expectServiceTypeId, actualServiceType.Id);
                Assert.Equal(19, actualServiceType.Services.Count());
            }
        }
    }
}
