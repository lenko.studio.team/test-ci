﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Clients;
using Jfdi.ProjectManagement.Api.Dtos.TaskInstancies;
using Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Jfdi.ProjectManagement.Api.Services;
using Jfdi.ProjectManagement.Api.Tests;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;
using JfdiTask = Jfdi.ProjectManagement.Api.Models.Task;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Controllers.Tests
{
    public class ClientsControllerTests : TestCase
    {
        public ClientsControllerTests(TestApplicationFactory<Startup> fixture, ITestOutputHelper testOutput)
            : base(fixture, testOutput)
        {
        }

        [Fact()]
        public async Task NewClientData_PostClient_ClientIsCreated()
        {
            // Arrange
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
            {
                StartDateUtc = DateTime.UtcNow,
                EndDateUtc = DateTime.UtcNow
            };

            Database(db =>
            {
                service.ServiceType = db.ServiceTypes.Find(IServiceTypeService.AccountsPayablesId);
                db.Services.Add(service);
                db.SaveChanges();
            });
            var payload = new
            {
                serviceIds = new[] { service.Id },
                companyName = "test",
                taxClassificationId = ITaxClassificationService.CCorpId,
                physicalAddress = "address1",
                mailingAddress = "mail1",
                companyEin = "11-1111111",
                stateRegistration = "state1",
                cityRegistration = "city1",
                duns = "123456789",
                dateFormed = DateTime.UtcNow,
                president = "president",
                tresurer = "tresurer",
                registeredAgent = "agent",
                phone = "+7123123123",
                clientTag = "cool",
                serviceStartDate = DateTime.UtcNow,
                onePasswordClientAcronoym = "acronym",
                clientJfdiEmails = new[] { "email@mail.to" },
                yearEndDate = DateTime.UtcNow.AddYears(1),
                initialTransactionProcessingDate = DateTime.UtcNow.AddDays(2),
                software = new[] { "pc" },
            };

            // Act
            var res = await PostJson("api/clients", payload);

            // Assert
            Assert.True(res.IsSuccessStatusCode);

            Fixture.Database(db =>
            {
                Assert.NotNull(db.Clients.FirstOrDefault(x => x.CompanyName == "test"));
            });
            Database(db =>
            {
                var actualClient = db.Clients
                    .Include(c => c.Services)
                    .ThenInclude(si => si.Template)
                    .Include(c => c.TeamMemberAssignments)
                    .Include(c => c.TaxClassification)
                    .First(c => c.CompanyName == "test");

                Assert.Equal(payload.cityRegistration, actualClient.CityRegistration);
                Assert.Collection(actualClient.ClientJfdiEmails, x => Assert.Equal(payload.clientJfdiEmails[0], x));
                Assert.Equal(payload.clientTag, actualClient.ClientTag);
                Assert.Equal(payload.companyName, actualClient.CompanyName);
                Assert.Equal(payload.companyEin, actualClient.CompanyEin);
                Assert.Equal(payload.dateFormed, actualClient.DateFormed);
                Assert.Equal(payload.duns, actualClient.Duns);
                Assert.Equal(payload.initialTransactionProcessingDate, actualClient.InitialTransactionProcessingDate);
                Assert.Equal(payload.mailingAddress, actualClient.MailingAddress);
                Assert.Equal(payload.onePasswordClientAcronoym, actualClient.OnePasswordClientAcronoym);
                Assert.Equal(payload.phone, actualClient.Phone);
                Assert.Equal(payload.physicalAddress, actualClient.PhysicalAddress);
                Assert.Equal(payload.president, actualClient.President);
                Assert.Equal(payload.registeredAgent, actualClient.RegisteredAgent);
                Assert.Equal(payload.serviceStartDate, actualClient.ServiceStartDate);
                Assert.Equal(payload.stateRegistration, actualClient.StateRegistration);
                Assert.Equal(payload.taxClassificationId, actualClient.TaxClassification.Id);
                Assert.Empty(actualClient.TeamMemberAssignments);
            });
        }

        [Fact()]
        public async Task NewClientDataWithoutAdditionalFields_PostClient_ClientIsCreated()
        {
            // Arrange
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var user = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "Doe",
                "Joe");
            Database(db =>
            {
                db.TeamMembers.Add(user);
                service.ServiceType = db.ServiceTypes.Find(IServiceTypeService.AccountsPayablesId);
                db.Services.Add(service);
                db.SaveChanges();
            });

            var payload = new
            {
                companyName = "test",
                serviceIds = new[] { service.Id },
                serviceTypeIds = new[] { IServiceTypeService.AccountsPayablesId },
                teamMemberAssignmentIds = new[] { user.Id }
            };

            // Act
            var res = await PostJson("api/clients", payload);

            // Assert
            Assert.True(res.IsSuccessStatusCode);

            Fixture.Database(db =>
            {
                Assert.NotNull(db.Clients.FirstOrDefault(x => x.CompanyName == "test"));
            });
            Database(db =>
            {
                var actualClient = db.Clients
                    .Include(c => c.Services)
                    .ThenInclude(si => si.Template)
                    .Include(c => c.TeamMemberAssignments)
                    .First(c => c.CompanyName == "test");

                Assert.Null(actualClient.CityRegistration);
                Assert.Null(actualClient.ClientJfdiEmails);
                Assert.Null(actualClient.ClientTag);
                Assert.Null(actualClient.CompanyEin);
                Assert.Null(actualClient.DateFormed);
                Assert.Null(actualClient.Duns);
                Assert.Null(actualClient.InitialTransactionProcessingDate);
                Assert.Null(actualClient.MailingAddress);
                Assert.Null(actualClient.OnePasswordClientAcronoym);
                Assert.Null(actualClient.Phone);
                Assert.Null(actualClient.PhysicalAddress);
                Assert.Null(actualClient.President);
                Assert.Null(actualClient.RegisteredAgent);
                Assert.Null(actualClient.ServiceStartDate);
                Assert.Null(actualClient.Software);
                Assert.Null(actualClient.StateRegistration);
                Assert.Null(actualClient.TaxClassification);
                Assert.Collection(actualClient.TeamMemberAssignments,
                    x => Assert.Equal(user.Id, x.Id));
                Assert.Equal(payload.companyName, actualClient.CompanyName);
            });
        }

        [Fact]
        public async Task NewClientWithWrongTin_PostClient_Get400Error()
        {
            // Arrange
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);

            Database(db =>
            {
                db.Services.Add(service);
                db.SaveChanges();
            });

            var payload = new
            {
                companyName = "test",
                companyEin = "11-111",
            };

            // Act
            var res = await PostJson("api/clients", payload);

            // Assert
            Assert.False(res.IsSuccessStatusCode);
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, res.StatusCode);

            AssertDatabaseHasNot<Client>(c => c.CompanyName == "test");
        }

        [Fact()]
        public async Task ClientData_PutClient_SuccesUpdate()
        {
            // Arrange
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service");
            serviceInstance.Template = service;

            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client")
            {
                PhysicalAddress = "address1",
                MailingAddress = "mail1",
                CompanyEin = "11-111-11",
                StateRegistration = "state1",
                CityRegistration = "city1",
                Duns = "22-222-22",
                DateFormed = DateTime.UtcNow,
                President = "president",
                Tresurer = "tresurer",
                RegisteredAgent = "agent",
                Phone = "+7123123123",
                ClientTag = "cool",
                ServiceStartDate = DateTime.UtcNow,
                OnePasswordClientAcronoym = "acronym",
                ClientJfdiEmails = new[] { "email@mail.to" },
                YearEndDate = DateTime.UtcNow.AddYears(1),
                InitialTransactionProcessingDate = DateTime.UtcNow.AddDays(2),
                Software = new[] { "pc" },
            };
            client.Services.Add(serviceInstance);

            Database(db =>
            {
                client.TaxClassification = db.TaxClassifications.Find(ITaxClassificationService.CCorpId);
                db.Services.Add(service);
                db.ServiceInstances.Add(serviceInstance);
                db.Clients.Add(client);
                db.SaveChanges();
            });

            var payload = new
            {
                companyName = "test1",
                taxClassificationId = ITaxClassificationService.SingleMemberId,
                physicalAddress = "new address",
                mailingAddress = "new mail",
                companyEin = "12-3456789",
                stateRegistration = "new state1",
                cityRegistration = "new city1",
                duns = "123456789",
                dateFormed = DateTime.UtcNow.AddDays(6),
                president = "new president",
                tresurer = "new tresurer",
                registeredAgent = "new agent",
                phone = "+7123123145",
                clientTag = "cool",
                serviceStartDate = DateTime.UtcNow.AddMinutes(5),
                onePasswordClientAcronoym = "new acronym",
                clientJfdiEmails = new[] { "email@mail.from" },
                yearEndDate = DateTime.UtcNow.AddYears(2),
                initialTransactionProcessingDate = DateTime.UtcNow.AddDays(5),
                software = new[] { "new pc" },
            };

            // Act
            var res = await PutJson($"api/clients/{client.Id}", payload);

            // Assert
            Assert.True(res.IsSuccessStatusCode);

            Database(db =>
            {
                var actualClient = db.Clients
                    .Include(c => c.Services)
                    .ThenInclude(si => si.Template)
                    .Include(c => c.TeamMemberAssignments)
                    .Include(c => c.TaxClassification)
                    .First(c => c.Id == client.Id);

                Assert.Equal(payload.cityRegistration, actualClient.CityRegistration);
                Assert.Collection(actualClient.ClientJfdiEmails, x => Assert.Equal(payload.clientJfdiEmails[0], x));
                Assert.Equal(payload.clientTag, actualClient.ClientTag);
                Assert.Equal(payload.companyName, actualClient.CompanyName);
                Assert.Equal(payload.companyEin, actualClient.CompanyEin);
                Assert.Equal(payload.dateFormed, actualClient.DateFormed);
                Assert.Equal(payload.duns, actualClient.Duns);
                Assert.Equal(payload.initialTransactionProcessingDate, actualClient.InitialTransactionProcessingDate);
                Assert.Equal(payload.mailingAddress, actualClient.MailingAddress);
                Assert.Equal(payload.onePasswordClientAcronoym, actualClient.OnePasswordClientAcronoym);
                Assert.Equal(payload.phone, actualClient.Phone);
                Assert.Equal(payload.physicalAddress, actualClient.PhysicalAddress);
                Assert.Equal(payload.president, actualClient.President);
                Assert.Equal(payload.registeredAgent, actualClient.RegisteredAgent);
                Assert.Equal(payload.serviceStartDate, actualClient.ServiceStartDate);
                Assert.Collection(actualClient.Software, s => Assert.Equal(payload.software[0], s));
                Assert.Equal(payload.stateRegistration, actualClient.StateRegistration);
                Assert.Equal(payload.taxClassificationId, actualClient.TaxClassification.Id);
                Assert.Empty(actualClient.TeamMemberAssignments);

                Assert.Collection<ServiceInstance>(
                    actualClient.Services,
                    s => Assert.Equal(service.Id, s.Template.Id));
            });
        }

        [Fact()]
        public async Task ClientData_DeleteClient_ClientIsDeleted()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service event",
                default,
                default,
                "RRULE")
            {
                Calendar = calendar
            };
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service")
            {
                Event = serviceEvent
            };
            var taskEvent = new CalendarEvent(
                 Guid.NewGuid(),
                 DateTime.UtcNow,
                 default,
                 "task event",
                 default,
                 default,
                 "RRULE")
            {
                Calendar = calendar
            };
            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task")
            {
                Event = taskEvent
            };
            var eventTask = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = taskInstance
            };
            var blockerEvent = new CalendarEvent(
                 Guid.NewGuid(),
                 DateTime.UtcNow,
                 default,
                 "blocker event",
                 default,
                 default,
                 "RRULE")
            {
                Calendar = calendar
            };
            var blocker = new Blocker(
                "blocker",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
            {
                CalendarEvent = blockerEvent,
                Task = eventTask
            };
            serviceInstance.Tasks.Add(taskInstance);
            taskInstance.Service = serviceInstance;

            Database(db =>
            {
                client.Calendar = calendar;
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.Services.Add(service);
                service.Tasks.Add(task);
                serviceInstance.Template = service;

                db.Tasks.Add(task);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.TaskInstances.Add(taskInstance);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}";

            // Act
            var res = await DeleteJson(url);

            // Assert
            Assert.True(res.IsSuccessStatusCode);

            AssertDatabaseHasNot<Client>(c =>
                c.Id == client.Id);

            AssertDatabaseHasNot<Calendar>(c =>
                c.Id == calendar.Id);

            AssertDatabaseHasNot<TaskInstance>(ti =>
                ti.Id == taskInstance.Id);

            AssertDatabaseHasNot<ServiceInstance>(si =>
                si.Id == serviceInstance.Id);
            AssertDatabaseHasNot<Blocker>(b =>
                b.Id == blocker.Id);
            AssertDatabaseHasNot<CalendarEvent>(si =>
                si.Id == serviceEvent.Id);
            AssertDatabaseHasNot<CalendarEvent>(si =>
                si.Id == taskEvent.Id);
            AssertDatabaseHasNot<CalendarEvent>(si =>
                si.Id == blockerEvent.Id);
        }

        [Fact()]
        public async Task ClientDataWithoutTasks_DeleteClient_ClientIsDeleted()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);

            var serviceEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service event",
                default,
                default,
                "RRULE")
            {
                Calendar = calendar
            };
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service")
            {
                Event = serviceEvent
            };

            Database(db =>
            {
                client.Calendar = calendar;
                db.Clients.Add(client);

                db.Services.Add(service);
                serviceInstance.Template = service;
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}";

            // Act
            var res = await DeleteJson(url);

            // Assert
            Assert.True(res.IsSuccessStatusCode);

            AssertDatabaseHasNot<Client>(c =>
                c.Id == client.Id);

            AssertDatabaseHasNot<ServiceInstance>(si =>
                si.Id == serviceInstance.Id);
            AssertDatabaseHasNot<ServiceInstance>(si =>
                si.Id == serviceInstance.Id);

            AssertDatabaseHas<Service>(s =>
                s.Id == service.Id);
        }

        [Theory]
        [InlineData(-4)]
        [InlineData(0)]
        [InlineData(5)]
        public async Task MultipleTaskInstances_AddTask_TasksAdded(int startDateOffset)
        {
            // Arrange
            var startDate = DateTime.UtcNow.AddDays(startDateOffset);
            var user = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "John",
                "Doe");
            var supervisor = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "John",
                "Doe");
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            client.TeamMemberAssignments.Add(user);
            client.TeamMemberAssignments.Add(supervisor);
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service");
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            client.Calendar = calendar;

            Database(db =>
            {
                db.Clients.Add(client);

                db.Services.Add(service);
                serviceInstance.Template = service;

                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.Calendars.Add(calendar);

                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/instances";
            var payload = new
            {
                title = "test",
                serviceId = serviceInstance.Id,
                recurrenceRule = "FREQ=DAILY;INTERVAL=1;COUNT=2",
                forReview = true,
                role = TeamMemberRole.Manager.ToString(),
                info = "some info",
                startUtc = startDate,
                assignedTo = new[] { user.Id },
                supervisorIds = new[] { supervisor.Id }
            };

            // Act
            var response = await PostJson<TaskEventDto>(url, payload);

            // Assert

            Database(db =>
            {
                var actualTaskInstance = db.TaskEvents
                    .Include(ti => ti.Instance.Event)
                    .Include(ti => ti.Instance.Template)
                    .Include(ti => ti.Instance.Service)
                    .Include(ti => ti.State)
                    .Include(ti => ti.Instance.Assignees)
                    .Include(ti => ti.Instance.Supervisors)
                    .First(x => x.Id == response.Id);
                Assert.Equal(serviceInstance.Id, actualTaskInstance.Instance.Service.Id);

                var actualCalendar = db.Calendars
                    .Include(c => c.CalendarEvents)
                    .First(x => x.Id == calendar.Id);

                var actualCalendarEvent = actualCalendar.CalendarEvents.First();
                Assert.Equal(actualCalendarEvent.Id, actualTaskInstance.Instance.Event.Id);

                Assert.Equal(user.Id, actualTaskInstance.Instance.Assignees.First().Id);
                Assert.Equal(supervisor.Id, actualTaskInstance.Instance.Supervisors.First().Id);
                Assert.Equal(payload.startUtc.DayOfYear, actualTaskInstance.StartDateUtc.DayOfYear);
                Assert.Equal(payload.startUtc.DayOfYear, actualTaskInstance.EndDateUtc.DayOfYear);
                Assert.Equal(ITaskStateService.TodoStateId, actualTaskInstance.State.Id);
            });
        }

        [Fact]
        public async Task OneTaskInstance_RemoveTask_TasksRemoved()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service");
            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = taskInstance
            };
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            client.Calendar = calendar;
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "testEvent",
                DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1))
            {
                CalendarId = calendar.Id
            };
            calendar.CalendarEvents.Add(calendarEvent);
            Database(db =>
            {
                db.Clients.Add(client);

                db.Services.Add(service);
                service.Tasks.Add(task);
                serviceInstance.Template = service;

                db.Tasks.Add(task);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.TaskInstances.Add(taskInstance);
                serviceInstance.Tasks.Add(taskInstance);
                taskInstance.Service = serviceInstance;
                taskInstance.Event = calendarEvent;
                taskInstance.Template = task;

                db.Calendars.Add(calendar);
                db.CalendarEvents.Add(calendarEvent);
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/events/{taskEvent.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            AssertDatabaseHasNot<TaskEvent>(t =>
                t.Id == taskEvent.Id);
            AssertDatabaseHas<JfdiTask>(t => t.Id == task.Id);
            AssertDatabaseHas<Service>(s => s.Id == service.Id);
        }
        [Fact]
        public async Task OneTaskInstance_RemoveTaskInstance_TasksRemoved()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service");
            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = taskInstance
            };
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            client.Calendar = calendar;
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "testEvent",
                DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1))
            {
                CalendarId = calendar.Id
            };
            calendar.CalendarEvents.Add(calendarEvent);
            Database(db =>
            {
                db.Clients.Add(client);

                db.Services.Add(service);
                service.Tasks.Add(task);
                serviceInstance.Template = service;

                db.Tasks.Add(task);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.TaskInstances.Add(taskInstance);
                serviceInstance.Tasks.Add(taskInstance);
                taskInstance.Service = serviceInstance;
                taskInstance.Event = calendarEvent;
                taskInstance.Template = task;

                db.Calendars.Add(calendar);
                db.CalendarEvents.Add(calendarEvent);
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/instances/{taskEvent.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            AssertDatabaseHasNot<TaskEvent>(t =>
                t.Id == taskEvent.Id);
            AssertDatabaseHasNot<TaskInstance>(t =>
                t.Id == taskInstance.Id);
            AssertDatabaseHas<JfdiTask>(t => t.Id == task.Id);
            AssertDatabaseHas<Service>(s => s.Id == service.Id);
        }

        [Fact]
        public async Task MultipleTaskInstance_GetTasks_ReturnTasks()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            client.Calendar = calendar;
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            service.Tasks.Add(task);
            var serviceEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service-event",
                DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1))
            {
                Calendar = calendar,
            };
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service")
            {
                Client = client,
                Event = serviceEvent
            };
            serviceInstance.Template = service;
            var adhocEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "adhock-event",
                DateTime.UtcNow.AddDays(-1),
                DateTime.UtcNow.AddMinutes(60).AddDays(-1))
            {
                Calendar = calendar
            };
            var adHocTaskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task")
            {
                Event = adhocEvent,
                Service = serviceInstance
            };
            var adHocTaskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = adHocTaskInstance,
                StartDateUtc = DateTime.UtcNow.AddDays(-1),
                EndDateUtc = DateTime.UtcNow.AddMinutes(60).AddDays(-1)
            };
            var recurrenceEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "recurence-event",
                DateTime.UtcNow.AddDays(-1),
                DateTime.UtcNow.AddMinutes(60).AddDays(-1),
                "RRULE:FREQ=DAILY;COUNT=30;INTERVAL=1;WKST=MO")
            {
                Calendar = calendar
            };
            var reccurenceTaskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task")
            {
                Event = recurrenceEvent,
                Service = serviceInstance,
                Template = task
            };
            var reccurenceTaskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = reccurenceTaskInstance,
                StartDateUtc = DateTime.UtcNow.AddDays(-1),
                EndDateUtc = DateTime.UtcNow.AddMinutes(60).AddDays(-1)
            };
            serviceInstance.Tasks.Add(reccurenceTaskInstance);
            serviceInstance.Tasks.Add(adHocTaskInstance);
            Database(db =>
            {
                db.Clients.Add(client);
                db.TaskInstances.AddRange(reccurenceTaskInstance, adHocTaskInstance);
                db.TaskEvents.AddRange(reccurenceTaskEvent, adHocTaskEvent);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks";

            // Act
            var actualTasks = await GetJson<IEnumerable<TaskEventShortDto>>(url);

            // Assert
            Assert.Collection(actualTasks,
                x => AssertTask(reccurenceTaskEvent, x),
                x => AssertTask(adHocTaskEvent, x));

            void AssertTask(TaskEvent expectTask, TaskEventShortDto actualTask)
            {
                Assert.Equal(expectTask.Id, actualTask.Id);
                Assert.Equal(expectTask.Instance.Name, actualTask.Title);
                Assert.Equal(expectTask.Instance.Service.Id, actualTask.Service.Id);
                Assert.Equal(expectTask.Instance.Service.Client.Id, actualTask.ClientId);
            }
        }

        [Fact]
        public async Task OneTaskInstance_UpdateTask_TaskUpdated()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service");
            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = taskInstance
            };
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            client.Calendar = calendar;
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "testEvent",
                DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1))
            {
                CalendarId = calendar.Id
            };
            calendar.CalendarEvents.Add(calendarEvent);
            Database(db =>
            {
                db.Clients.Add(client);

                db.Services.Add(service);
                service.Tasks.Add(task);
                serviceInstance.Template = service;

                db.Tasks.Add(task);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.TaskInstances.Add(taskInstance);
                serviceInstance.Tasks.Add(taskInstance);
                taskInstance.Service = serviceInstance;
                taskInstance.Event = calendarEvent;
                taskInstance.Template = task;

                db.Calendars.Add(calendar);
                db.CalendarEvents.Add(calendarEvent);
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/instances/{taskEvent.Id}";
            var payload = new 
            {
                title = "New title",
                startUtc = DateTime.UtcNow
            };

            // Act
            var response = await PutJson(url, payload);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            Database(db =>
            {
                var dbEvent = db.TaskEvents.Include(x => x.Instance).First(t => t.Id == taskEvent.Id);
                Assert.Equal(dbEvent.Instance.Name, payload.title);
                Assert.Equal(dbEvent.StartDateUtc, payload.startUtc.Date);
                Assert.Equal(dbEvent.EndDateUtc, payload.startUtc.Date.AddDays(1).AddTicks(-1));
            });
        }

        [Fact]
        public async Task MultipleServiceInstances_GetServicesByClient_ServicesReturn()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            client.Calendar = calendar;
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);

            var range = Enumerable.Range(0, 20);
            var serviceInstances = new List<ServiceInstance>();
            foreach (var seed in range)
            {
                var serviceInstance = new ServiceInstance(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "test service")
                {
                    Template = service,
                    Client = client
                };
                serviceInstance.Template = service;
                var serviceEvent = new CalendarEvent(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"service event {seed}",
                    DateTime.UtcNow.AddDays(seed),
                    DateTime.UtcNow.AddDays(seed + 1))
                {
                    Calendar = calendar
                };
                serviceInstance.Event = serviceEvent;
                var taskInstance = new TaskInstance(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "task")
                {
                    Service = serviceInstance
                };
                var taskEvent = new CalendarEvent(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"task event {seed}",
                    DateTime.UtcNow.AddDays(seed),
                    DateTime.UtcNow.AddDays(seed + 1))
                {
                    Calendar = calendar
                };
                taskInstance.Event = taskEvent;

                serviceInstance.Tasks.Add(taskInstance);
                serviceInstances.Add(serviceInstance);
                client.Services.Add(serviceInstance);
            }
            Database(db =>
            {
                db.Clients.Add(client);

                db.Services.Add(service);
                service.Tasks.Add(task);

                db.Tasks.Add(task);

                db.ServiceInstances.AddRange(serviceInstances);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/services?limit=3";

            // Act
            var services = await GetJson<List<ServiceInstanceDto>>(url);

            // Assert

            Assert.Collection<ServiceInstanceDto>(
                services,
                s => AssertInstance(serviceInstances[0], s),
                s => AssertInstance(serviceInstances[1], s),
                s => AssertInstance(serviceInstances[2], s)
                );

            static void AssertInstance(ServiceInstance expect, ServiceInstanceDto actual)
            {
                Assert.Equal(expect.Id, actual.Id);
                Assert.Equal(expect.Template.Id, actual.ServiceTypeId);
            }
        }

        [Fact]
        public async Task NoServiceInstances_AddService_ServiceAdded()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
            {
                StartDateUtc = DateTime.UtcNow,
                EndDateUtc = DateTime.UtcNow.AddDays(1)
            };
            var childTask = new JfdiTask(
                 "child task",
                 Guid.NewGuid(),
                 DateTime.UtcNow,
                 default)
            {
                StartDateUtc = DateTime.UtcNow,
                EndDateUtc = DateTime.UtcNow.AddDays(1)
            };

            Database(db =>
            {
                task.Children.Add(childTask);
                childTask.Parent = task;
                db.Clients.Add(client);
                service.ServiceType = db.ServiceTypes.Find(IServiceTypeService.AccountsPayablesId);

                db.Services.Add(service);
                service.Tasks.Add(task);

                db.Tasks.Add(task);

                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/services/{service.Id}";

            // Act
            var response = await PostJson<ServiceInstanceDto>(url, default);

            // Assert
            Database(db =>
                {
                    var actualClient = db.Clients
                        .Include(c => c.Services)
                        .ThenInclude(si => si.Template)
                        .Include(c => c.Services)
                        .ThenInclude(si => si.Tasks.Where(t=>t.Parent == null))
                        .ThenInclude(si => si.Template)
                        .Include(c => c.Services)
                        .ThenInclude(si => si.Tasks.Where(t => t.Parent == null))
                        .ThenInclude(si => si.Children)
                        .First(c => c.CompanyName == "client");

                    Assert.Collection<ServiceInstance>(
                        actualClient.Services,
                        s => Assert.Equal(service.Id, s.Template.Id));

                    Assert.Collection(actualClient.Services.SelectMany(s => s.Tasks).Where(t => t.Parent == null),
                        ti => AssertTask(task, ti));
                });

            static void AssertTask(JfdiTask expectTask, TaskInstance actualTask)
            {
                Assert.Equal(expectTask.Title, actualTask.Name);
                Assert.Equal(expectTask.Id, actualTask.Template.Id);
                Assert.Equal(expectTask.Children.First().Title, actualTask.Children.First().Name);
            }
        }

        [Fact]
        public async Task ExistServiceInstances_AddService_GetError()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);

            var serviceInstance = new ServiceInstance(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "test service");

            Database(db =>
            {
                db.Clients.Add(client);

                db.Services.Add(service);
                service.Tasks.Add(task);

                db.Tasks.Add(task);
                client.Services.Add(serviceInstance);

                serviceInstance.Template = service;
                serviceInstance.Client = client;
                serviceInstance.Template = service;
                db.ServiceInstances.Add(serviceInstance);

                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/services";
            var payload = new { serviceId = service.Id };

            // Act
            var response = await PostJson(url, payload);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
        }

        [Fact]
        public async Task MultipleTaskInstances_GetTask_TaskReturnWithBlockers()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                $"testEvent",
                 DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1),
                "RRULE:FREQ=DAILY;UNTIL=20210430T200000Z;INTERVAL=1;WKST=MO");
            var user = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "Doe",
                "Joe");
            client.TeamMemberAssignments.Add(user);
            var range = Enumerable.Range(0, 20);
            var taskInstances = new List<TaskEvent>();
            var blockers = new List<Blocker>();
            foreach (var seed in range)
            {
                var taskInstance = new TaskInstance(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "task");
                serviceInstance.Tasks.Add(taskInstance);
                taskInstance.Service = serviceInstance;
                taskInstance.Event = calendarEvent;
                var taskEvent = new TaskEvent(
                    Guid.NewGuid())
                {
                    Instance = taskInstance
                };
                taskInstances.Add(taskEvent);
                var blocker = new Blocker(
                    $"blocker_{seed}",
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default);
                blocker.Task = taskEvent;
                blocker.CalendarEvent = calendarEvent;
                blockers.Add(blocker);
                taskInstance.Assignees.Add(user);
            }
            Database(db =>
            {
                var todoState = db.TaskStates.Find(ITaskStateService.TodoStateId);
                taskInstances.ForEach(ti => ti.State = todoState);
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.Services.Add(service);
                service.Tasks.Add(task);
                serviceInstance.Template = service;

                db.Tasks.Add(task);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.Add(calendarEvent);
                db.TaskEvents.AddRange(taskInstances);
                db.AddRange(blockers);

                db.SaveChanges();
            });

            var expectedTask = taskInstances[4];
            var url = $"api/clients/{client.Id}/tasks/instances/{expectedTask.Id}?includeBlockers=true";

            // Act
            var actualTask = await GetJson<TaskEventDto>(url);

            // Assert
            Assert.Equal(expectedTask.Id, actualTask.Id);
            Assert.Equal(expectedTask.State.Id, actualTask.State.Id);
            Assert.Equal(expectedTask.State.Title, actualTask.State.Title);
            Assert.Collection(expectedTask.Instance.Assignees,
                x => Assert.Equal(x.Id, actualTask.AssignedTo.First().Id));
            Assert.True(actualTask.Blockers.Any());

            var expectBlocker = blockers.Single(b => b.Task.Id == expectedTask.Id);
            Assert.Collection(actualTask.Blockers,
                actualBlocker => BlockerAssertion(expectBlocker, actualBlocker));

            static void BlockerAssertion(Blocker expectBlocker, BlockerDto actualBlocker)
            {
                Assert.Equal(expectBlocker.Id, actualBlocker.Id);
                Assert.Equal(expectBlocker.Task.Id, actualBlocker.TaskId);
                Assert.Equal(expectBlocker.Title, actualBlocker.Title);
                Assert.Equal(expectBlocker.CalendarEvent.Id, actualBlocker.CalendarEventId);
            }
        }

        [Fact]
        public async Task MultipleTaskInstances_GetTask_TaskReturnWithSubtasks()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                $"testEvent",
                 DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1),
                "RRULE:FREQ=DAILY;UNTIL=20210430T200000Z;INTERVAL=1;WKST=MO");

            var range = Enumerable.Range(0, 20);
            var taskInstances = new List<TaskInstance>();
            foreach (var seed in range)
            {
                var taskInstance = new TaskInstance(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "task");
                var taskEvent = new TaskEvent(
                    Guid.NewGuid())
                {
                    Instance = taskInstance
                };
                taskInstance.TaskEvents.Add(taskEvent);
                serviceInstance.Tasks.Add(taskInstance);
                taskInstances.Add(taskInstance);
                taskInstance.Service = serviceInstance;
                taskInstance.Event = calendarEvent;
                var childTask = new TaskInstance(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"child_{seed}");
                taskInstance.Children.Add(childTask);

                var childTaskEvent = new TaskEvent(
                    Guid.NewGuid())
                {
                    Instance = childTask
                };
                taskEvent.Children.Add(childTaskEvent);
            }
            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.Services.Add(service);
                service.Tasks.Add(task);
                serviceInstance.Template = service;

                db.Tasks.Add(task);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.Add(calendarEvent);
                db.AddRange(taskInstances);
                db.SaveChanges();
            });

            var expectedTask = taskInstances[4].TaskEvents.First();
            var url = $"api/clients/{client.Id}/tasks/instances/{expectedTask.Id}?includeSubtasks=true";

            // Act
            var actualTask = await GetJson<TaskEventDto>(url);

            // Assert
            Assert.Equal(expectedTask.Id, actualTask.Id);
            Assert.True(actualTask.SubTasks.Any());

            var expectSubTask = expectedTask.Children.Single();
            Assert.Collection(actualTask.SubTasks,
                actualSubtask => BlockerAssertion(expectSubTask, actualSubtask));

            static void BlockerAssertion(TaskEvent expectSubTask, SubTaskEventDto actualSubtask)
            {
                Assert.Equal(expectSubTask.Id, actualSubtask.Id);
                Assert.Equal(expectSubTask.Instance.Name, actualSubtask.Title);
            }
        }


        [Fact]
        public async Task MultipleTaskInstances_GetTask_TaskReturnWithoutBlockers()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                $"testEvent",
                 DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1),
                "RRULE:FREQ=DAILY;UNTIL=20210430T200000Z;INTERVAL=1;WKST=MO");

            var range = Enumerable.Range(0, 20);
            var taskInstances = new List<TaskEvent>();
            var blockers = new List<Blocker>();
            foreach (var seed in range)
            {
                var taskInstance = new TaskInstance(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "task");
                serviceInstance.Tasks.Add(taskInstance);
                taskInstance.Service = serviceInstance;
                taskInstance.Event = calendarEvent;
                var blocker = new Blocker(
                    $"blocker_{seed}",
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default);
                var taskEvent = new TaskEvent(
                    Guid.NewGuid())
                {
                    Instance = taskInstance
                };
                taskInstances.Add(taskEvent);
                blocker.Task = taskEvent;
                blocker.CalendarEvent = calendarEvent;
                blockers.Add(blocker);
            }
            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.Services.Add(service);
                service.Tasks.Add(task);
                serviceInstance.Template = service;

                db.Tasks.Add(task);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.Add(calendarEvent);
                db.TaskEvents.AddRange(taskInstances);
                db.AddRange(blockers);
                db.SaveChanges();
            });

            var expectedTask = taskInstances[4];
            var url = $"api/clients/{client.Id}/tasks/instances/{expectedTask.Id}";

            // Act
            var actualTask = await GetJson<TaskEventDto>(url);

            // Assert
            Assert.Equal(expectedTask.Id, actualTask.Id);
            Assert.Empty(actualTask.Blockers);
        }

        [Fact]
        public async Task OneServiceInstance_RemoveService_Success()
        {
            // Arrange
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            client.Calendar = calendar;
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service event",
                DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1))
            {
                Calendar = calendar
            };
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service")
            {
                Event = serviceEvent
            };
            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "testEvent",
                DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1))
            {
                CalendarId = calendar.Id
            };
            var software = new Software(
                "soft",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            serviceInstance.Softwares.Add(software);
            calendar.CalendarEvents.Add(calendarEvent);
            Database(db =>
            {
                db.Clients.Add(client);

                db.Services.Add(service);
                service.Tasks.Add(task);
                serviceInstance.Template = service;

                db.Tasks.Add(task);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.TaskInstances.Add(taskInstance);
                serviceInstance.Tasks.Add(taskInstance);
                taskInstance.Service = serviceInstance;
                taskInstance.Event = calendarEvent;
                taskInstance.Template = task;

                db.Calendars.Add(calendar);
                db.CalendarEvents.Add(calendarEvent);

                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/services/{serviceInstance.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            AssertDatabaseHasNot<ServiceInstance>(t =>
                t.Id == serviceInstance.Id);

            AssertDatabaseHas<Software>(s =>
                s.Id == software.Id);

            AssertDatabaseHas<Client>(c =>
                c.Id == client.Id);
            AssertDatabaseHasNot<TaskInstance>(ti =>
               ti.Id == taskInstance.Id);
            AssertDatabaseHasNot<CalendarEvent>(c =>
               c.Id == calendarEvent.Id);

            Database(db =>
            {
                var actualClient = db.Clients
                    .Include(c => c.Services)
                    .ThenInclude(s => s.Template)
                    .First(c => c.Id == client.Id);

                Assert.False(actualClient.Services.Any());
            });
        }

        [Fact]
        public async Task OneTaskInstance_ChangeStateToDone_StateChanged()
        {
            // Arrange
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task instance",
                DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1),
                "RRULE:FREQ=DAILY;INTERVAL=1;WKST=MO");
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service instance");
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task instance")
            {
                Event = calendarEvent
            };
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance,
                StartDateUtc = DateTime.UtcNow,
                EndDateUtc = DateTime.UtcNow.AddDays(1),
            };

            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Client = client;

                serviceInstance.Tasks.Add(tasksInstance);
                taskEvent.State = db.TaskStates.Find(ITaskStateService.TodoStateId);
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/{taskEvent.Id}/states/{ITaskStateService.DoneStateId}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var actualTask = db.TaskEvents
                    .Include(c => c.State)
                    .First(c => c.Id == taskEvent.Id);

                Assert.Equal(ITaskStateService.DoneStateId, actualTask.State.Id);
            });
            Database(db =>
            {
                var newTask = db.TaskEvents
                    .Include(c => c.State)
                    .First(te =>
                        te.Id != taskEvent.Id
                        && te.StartDateUtc.DayOfYear == taskEvent.StartDateUtc.AddDays(1).DayOfYear
                        && te.EndDateUtc.DayOfYear == taskEvent.EndDateUtc.AddDays(1).DayOfYear);

                Assert.Equal(ITaskStateService.TodoStateId, newTask.State.Id);
            });
        }

        [Fact]
        public async Task TaskInstanceNotForReview_ChangeStateToReview_StateNotChanged()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service instance");
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task instance")
            {
                ForReview = false
            };
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Client = client;

                serviceInstance.Tasks.Add(tasksInstance);
                taskEvent.State = db.TaskStates.Find(ITaskStateService.TodoStateId);
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/{tasksInstance.Id}/states/{ITaskStateService.ForReviewStateId}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            Database(db =>
            {
                var actualTask = db.TaskEvents
                    .Include(c => c.State)
                    .First(c => c.Id == taskEvent.Id);

                Assert.Equal(ITaskStateService.TodoStateId, actualTask.State.Id);
            });
        }

        [Fact]
        public async Task TaskBlocked_ChangeStateToDone_StateNotChanged()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service instance");
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task instance");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Client = client;

                serviceInstance.Tasks.Add(tasksInstance);
                db.TaskEvents.Add(taskEvent);
                taskEvent.State = db.TaskStates.Find(ITaskStateService.BlockedStateId);

                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/{taskEvent.Id}/states/{ITaskStateService.DoneStateId}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.False(response.IsSuccessStatusCode);

            Database(db =>
            {
                var actualTask = db.TaskEvents
                    .Include(c => c.State)
                    .First(c => c.Id == taskEvent.Id);

                Assert.Equal(ITaskStateService.BlockedStateId, actualTask.State.Id);
            });
        }

        [Fact]
        public async Task OneTeamMemberInClient_RemoveMember_Success()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var user = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "John",
                "Doe");
            client.TeamMemberAssignments.Add(user);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service instance");
            serviceInstance.Client = client;
            client.Services.Add(serviceInstance);
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task instance");
            serviceInstance.Tasks.Add(tasksInstance);
            tasksInstance.Service = serviceInstance;
            tasksInstance.Assignees.Add(user);
            tasksInstance.Supervisors.Add(user);
            Database(db =>
            {
                db.Clients.Add(client);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/team/{user.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            AssertDatabaseHas<TeamMember>(t =>
                t.Id == user.Id);

            AssertDatabaseHas<Client>(c =>
                c.Id == client.Id);

            Database(db =>
            {
                var actualClient = db.Clients
                    .Include(c => c.TeamMemberAssignments)
                    .First(c => c.Id == client.Id);

                Assert.False(actualClient.TeamMemberAssignments.Any());
            });
            Database(db =>
            {
                var actualTask = db.TaskInstances
                    .Include(c => c.Assignees)
                    .Include(c => c.Supervisors)
                    .First(t => t.Id == tasksInstance.Id);

                Assert.False(actualTask.Assignees.Any());
                Assert.False(actualTask.Supervisors.Any());
            });
        }

        [Fact]
        public async Task TaskDone_ChangeStateToBlock_StateNotChanged()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var serviceInstance = new ServiceInstance(
                 Guid.NewGuid(),
                 DateTime.UtcNow,
                 default,
                 "service name");
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task instance");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            Database(db =>
                {
                    db.Clients.Add(client);
                    client.Services.Add(serviceInstance);

                    db.ServiceInstances.Add(serviceInstance);
                    serviceInstance.Client = client;

                    serviceInstance.Tasks.Add(tasksInstance);
                    db.TaskEvents.Add(taskEvent);
                    taskEvent.State = db.TaskStates.Find(ITaskStateService.DoneStateId);

                    db.SaveChanges();
                });

            var url = $"api/clients/{client.Id}/tasks/{taskEvent.Id}/states/{ITaskStateService.BlockedStateId}";
            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.False(response.IsSuccessStatusCode);

            Database(db =>
            {
                var actualTask = db.TaskEvents
                    .Include(c => c.State)
                    .First(c => c.Id == taskEvent.Id);

                Assert.Equal(ITaskStateService.DoneStateId, actualTask.State.Id);
            });
        }

        [Fact]
        public async Task TaskDone_ChangeStateToToDo_StateNotChanged()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var serviceInstance = new ServiceInstance(
                 Guid.NewGuid(),
                 DateTime.UtcNow,
                 default,
                 "service name");
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task instance");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Client = client;

                serviceInstance.Tasks.Add(tasksInstance);
                db.TaskEvents.Add(taskEvent);
                taskEvent.State = db.TaskStates.Find(ITaskStateService.DoneStateId);

                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/{taskEvent.Id}/states/{ITaskStateService.TodoStateId}";
            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.False(response.IsSuccessStatusCode);

            Database(db =>
            {
                var actualTask = db.TaskEvents
                    .Include(c => c.State)
                    .First(c => c.Id == taskEvent.Id);

                Assert.Equal(ITaskStateService.DoneStateId, actualTask.State.Id);
            });
        }

        [Fact]
        public async Task TaskNotAssigned_AssigneToUser_TaskAssigned()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            var user = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "Doe",
                "Joe");
            client.TeamMemberAssignments.Add(user);

            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Client = client;

                serviceInstance.Tasks.Add(tasksInstance);
                db.TaskEvents.Add(taskEvent);
                taskEvent.State = db.TaskStates.Find(ITaskStateService.DoneStateId);

                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/{taskEvent.Id}/assign/{user.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            Database(db =>
            {
                var actualTask = db.TaskInstances
                    .Include(ti => ti.Assignees)
                    .First(c => c.Id == tasksInstance.Id);

                Assert.Collection(actualTask.Assignees,
                    x => Assert.Equal(actualTask.Assignees.First().Id, user.Id));
            });
        }

        [Fact]
        public async Task TaskAssigned_RemoveAssignedUser_TaskUnassigned()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            var user = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "Doe",
                "Joe");
            client.TeamMemberAssignments.Add(user);
            tasksInstance.Assignees.Add(user);
            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Client = client;
                db.TaskEvents.Add(taskEvent);
                serviceInstance.Tasks.Add(tasksInstance);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/{taskEvent.Id}/deassign/{user.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var actualTask = db.TaskInstances
                    .Include(ti => ti.Assignees)
                    .First(c => c.Id == tasksInstance.Id);

                Assert.Empty(actualTask.Assignees);
            });
        }

        [Fact]
        public async Task TaskUnsupervised_AddSupervisor_TaskSupervised()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            var user = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "Doe",
                "Joe");
            client.TeamMemberAssignments.Add(user);

            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Client = client;
                db.TaskEvents.Add(taskEvent);
                serviceInstance.Tasks.Add(tasksInstance);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/{taskEvent.Id}/supervise/{user.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            Database(db =>
            {
                var actualTask = db.TaskInstances
                    .Include(ti => ti.Supervisors)
                    .First(c => c.Id == tasksInstance.Id);

                Assert.Collection(actualTask.Supervisors,
                    x => Assert.Equal(x.Id, user.Id));
                Assert.True(actualTask.ForReview);
            });
        }

        [Fact]
        public async Task TaskSupervise_UnsuperviseTask_TaskUnsupervised()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            var user = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "Doe",
                "Joe");
            client.TeamMemberAssignments.Add(user);
            tasksInstance.Supervisors.Add(user);
            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Client = client;

                serviceInstance.Tasks.Add(tasksInstance);
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/{taskEvent.Id}/supervise/{user.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var actualTask = db.TaskInstances
                    .Include(ti => ti.Supervisors)
                    .First(c => c.Id == tasksInstance.Id);

                Assert.Empty(actualTask.Supervisors);
                Assert.False(actualTask.ForReview);
            });
        }

        [Fact]
        public async Task TaskAssigned_AssignedToAnotherUser_TaskAssigned()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            var user = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "Doe",
                "Joe");
            client.TeamMemberAssignments.Add(user);
            tasksInstance.Assignees.Add(user);
            var anotherUser = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "Doe",
                "Jane");
            client.TeamMemberAssignments.Add(anotherUser);

            Database(db =>
            {
                db.Clients.Add(client);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Client = client;

                serviceInstance.Tasks.Add(tasksInstance);
                db.TeamMembers.Add(anotherUser);
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/tasks/{taskEvent.Id}/assign/{anotherUser.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            Database(db =>
            {
                var actualTask = db.TaskInstances
                    .Include(ti => ti.Assignees)
                    .First(c => c.Id == tasksInstance.Id);

                Assert.Collection(actualTask.Assignees,
                    x => Assert.Equal(user.Id, x.Id),
                    x => Assert.Equal(anotherUser.Id, x.Id));
            });
        }

        [Fact]
        public async Task NoTeamMemberInClient_AddMember_Success()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var teamMember = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "John",
                "Doe")
            {
                EmailConfirmed = true
            };

            Database(db =>
            {
                db.Clients.Add(client);
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var url = $"api/clients/{client.Id}/team/{teamMember.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            AssertDatabaseHas<TeamMember>(t =>
                t.Id == teamMember.Id);

            AssertDatabaseHas<Client>(c =>
                c.Id == client.Id);

            Database(db =>
            {
                var actualClient = db.Clients
                    .Include(c => c.TeamMemberAssignments)
                    .First(c => c.Id == client.Id);

                Assert.True(actualClient.TeamMemberAssignments.Any());
                Assert.Equal(teamMember.Id, actualClient.TeamMemberAssignments.First().Id);
            });
        }

        [Fact]
        public async Task ClientArchive_GetArchiveCount_Count1()
        {
            //Assert
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var archiveClient = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client")
            {
                ArchiveInfo = archive,
            };
            var notArchiveClient = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client"); ;
            Database(db =>
            {
                db.Clients.Add(archiveClient);
                db.Clients.Add(notArchiveClient);
                db.SaveChanges();
            });

            var url = $"api/clients/archive/count";

            // Act
            var count = await GetJson<ArchiveCountDto>(url);

            // Assert
            Assert.Equal(1, count.Count);
        }

        [Fact]
        public async Task Client_ArchiveClient_ClientArchived()
        {
            //Assert
            var notArchiveClient = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            Database(db =>
            {
                db.Clients.Add(notArchiveClient);
                db.SaveChanges();
            });

            var url = $"api/clients/archive/{notArchiveClient.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.Clients
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == notArchiveClient.Id);

                Assert.NotNull(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task ClientArchive_ReactivateClient_Success()
        {
            //Assert
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var archiveClient = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client")
            {
                ArchiveInfo = archive,
            };
            Database(db =>
            {
                db.Clients.Add(archiveClient);
                db.SaveChanges();
            });

            var url = $"api/clients/archive/{archiveClient.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.Clients
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == archiveClient.Id);

                Assert.Null(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task ClientNotArchive_ReactivateClient_Fail()
        {
            //Assert
            var notArchiveClient = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            Database(db =>
            {
                db.Clients.Add(notArchiveClient);
                db.SaveChanges();
            });

            var url = $"api/clients/archive/{notArchiveClient.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.Clients
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == notArchiveClient.Id);

                Assert.Null(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task ClientWithOpenTask_ArchiveClient_ClientNotArchived()
        {
            //Assert
            var notArchiveClient = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service")
            {
                Client = notArchiveClient
            };
            var task = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service")
            {
                Service = service
            };
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = task
            };

            Database(db =>
            {
                taskEvent.State = db.TaskStates.Find(ITaskStateService.InProgressStateId);

                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/clients/archive/{notArchiveClient.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.Clients
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == notArchiveClient.Id);

                Assert.Null(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task ClientWithDoneTask_ArchiveClient_ClientArchived()
        {
            //Assert
            var notArchiveClient = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service")
            {
                Client = notArchiveClient
            };
            var task = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service")
            {
                Service = service
            };
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = task
            };
            Database(db =>
            {
                taskEvent.State = db.TaskStates.Find(ITaskStateService.DoneStateId);

                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/clients/archive/{notArchiveClient.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.Clients
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == notArchiveClient.Id);

                Assert.NotNull(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task ClientArchive_GetArchiveClients_ReturnClients()
        {
            //Assert
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var archiveClient = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client")
            {
                ArchiveInfo = archive,
            };
            var notArchiveClient = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new ServiceInstance(
                 Guid.NewGuid(),
                 DateTime.UtcNow,
                 default,
                 "service")
            {
                Client = notArchiveClient
            };
            Database(db =>
            {
                db.ServiceInstances.Add(service);
                db.Clients.Add(archiveClient);
                db.Clients.Add(notArchiveClient);
                db.SaveChanges();
            });

            var url = $"api/clients/archive?limit=3";

            // Act
            var clients = await GetJson<IEnumerable<ArchiveClientDto>>(url);

            // Assert
            Assert.Collection(clients,
                t => AssertArchive(archiveClient, t));


            void AssertArchive(Client expectClient, ArchiveClientDto actualClient)
            {
                Assert.Equal(expectClient.Id, actualClient.Id);
            }
        }
    }
}
