﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Xunit;
using Xunit.Abstractions;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Tests.Controllers
{
    public class TaxClassificationsControllerTests : TestCase
    {
        public TaxClassificationsControllerTests(
            TestApplicationFactory<Startup> fixture, 
            ITestOutputHelper testOutput)
            : base(fixture, testOutput)
        {
        }

        [Fact]
        public async Task TaxesExist_GetTaxies_TaxiesReturn()
        {
            // Arrange

            var url = $"api/taxClassifications";

            // Act
            var response = await GetJson<List<TaxClassificationDto>>(url);

            // Assert

            Assert.Equal(4, response.Count);

            response.ForEach(actual =>
            {
                AssertDatabaseHas<TaxClassification>(expect =>
                {
                    return expect.Id == actual.Id
                        && expect.Title == actual.Title;
                });
            });
        }
    }
}
