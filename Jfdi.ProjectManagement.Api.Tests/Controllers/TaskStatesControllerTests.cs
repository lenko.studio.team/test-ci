﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Xunit;
using Xunit.Abstractions;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Tests.Controllers
{
    public class TaskStatesControllerTests : TestCase
    {
        public TaskStatesControllerTests(TestApplicationFactory<Startup> fixture, ITestOutputHelper testOutput)
            : base(fixture, testOutput)
        {
        }

        [Fact]
        public async Task StatesExist_GetStates_StatesReturn()
        {
            // Arrange

            var url = $"api/taskStates";

            // Act
            var response = await GetJson<List<TaskStateDto>>(url);

            // Assert

            response.ForEach(actualState =>
            {
                AssertDatabaseHas<TaskState>(expectState =>
                {
                    return expectState.Id == actualState.Id
                        && expectState.Title == actualState.Title;
                });
            });
        }
    }
}
