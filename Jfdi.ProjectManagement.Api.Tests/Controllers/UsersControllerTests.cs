﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Dtos.Clients;
using Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances;
using Jfdi.ProjectManagement.Api.Dtos.Users;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Jfdi.ProjectManagement.Api.Models.Notifications;
using Jfdi.ProjectManagement.Api.Models.Users;
using Jfdi.ProjectManagement.Api.Services;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

using JfdiTask = Jfdi.ProjectManagement.Api.Models.Task;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Tests.Controllers
{
    public class UsersControllerTests : TestCase
    {
        public UsersControllerTests(TestApplicationFactory<Startup> fixture, ITestOutputHelper testOutput)
            : base(fixture, testOutput)
        {
        }

        [Fact]
        public async Task ExistMember_DeleteMember_Success()
        {
            // Arrange

            var user = new User(
                 Guid.NewGuid(),
                 $"user@mail.com",
                 DateTime.UtcNow,
                 UserPermissions.Owner);
            var member = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "John",
                "Doe")
            {
                User = user
            };
            Database(db =>
            {
                db.TeamMembers.Add(member);
                db.SaveChanges();
            });
            var url = $"api/users/{member.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            response.EnsureSuccessStatusCode();

            AssertDatabaseHasNot<TeamMember>(tm =>
                tm.Id == member.Id);
        }

        [Fact]
        public async Task NoMember_CreateMember_ReturnMember()
        {
            // Arrange
            var payload = new
            {
                lastName = "last name",
                firstName = "first name",
                email = "test@jfdiaccountants.com",
                role = "Manager",
                phone = "(888)-415-5555",
            };

            var url = $"api/users";

            // Act
            var actualTeamMember = await PostJson<TeamMemberDto>(url, payload);

            // Assert
            Assert.Equal(payload.firstName, actualTeamMember.FirstName);
            Assert.Equal(payload.email, actualTeamMember.Email);
            Database(db =>
            {
                var notificationOptions = db.NotificationOptions.Where(no => no.TeamMember.Id == actualTeamMember.Id);
                Assert.Collection(notificationOptions.OrderBy(no => no.NotificationType).ToList(),
                    opt => AssertOption(NotificationType.TaskCompletion, opt),
                    opt => AssertOption(NotificationType.TaskDueDate, opt),
                    opt => AssertOption(NotificationType.TaskAssigning, opt),
                    opt => AssertOption(NotificationType.TaskOverdue, opt));

                void AssertOption(NotificationType expect, NotificationOption opt)
                {
                    Assert.Equal(expect, opt.NotificationType);
                    Assert.True(opt.IsOn);
                }
            });
        }

        [Fact]
        public async Task ExistClientWithMember_GetMember_ReturnMemberWithClient()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service");
            serviceInstance.Template = service;
            client.Services.Add(serviceInstance);
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            client.Calendar = calendar;
            var user = new User(
                 Guid.NewGuid(),
                 "user@mail.com",
                 DateTime.UtcNow,
                 UserPermissions.Owner);
            var teamMember = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "Alex",
                "Kush")
            {
                User = user
            };
            client.TeamMemberAssignments.Add(teamMember);

            Database(db =>
            {
                db.Clients.Add(client);
                service.ServiceType = db.ServiceTypes.Find(IServiceTypeService.ComplianceId);
                db.Services.Add(service);
                db.ServiceInstances.Add(serviceInstance);
                db.Calendars.Add(calendar);
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var url = $"api/users/{teamMember.Id}";

            // Act
            var actualTeamMember = await GetJson<TeamMemberDto>(url);

            // Assert
            Assert.Equal(teamMember.FirstName, actualTeamMember.FirstName);
            Assert.Equal(teamMember.LastName, actualTeamMember.LastName);
            Assert.NotEmpty(actualTeamMember.Clients);

            AssertClient(client, actualTeamMember.Clients.First());

            void AssertClient(Client expectClient, ClientShortDto actualClient)
            {
                Assert.Equal(expectClient.Id, actualClient.Id);
                Assert.Equal(expectClient.CompanyName, actualClient.CompanyName);
                AssertService(actualClient.Services.First());
            }

            void AssertService(ServiceShortDto actualService)
            {
                Assert.Equal(serviceInstance.Id, actualService.Id);
                Assert.Equal(serviceInstance.Name, actualService.Name);
                Assert.Equal(serviceInstance.Template.ServiceType.Color, actualService.Color);
                Assert.Equal(serviceInstance.Template.ServiceType.Icon, actualService.Icon);
                Assert.Equal(serviceInstance.Template.ServiceType.FillIcon, actualService.FillIcon);
            }
        }

        [Fact]
        public async Task ExistClientWithMember_GetMemberByUserId_ReturnMemberWithClient()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service");
            serviceInstance.Template = service;
            client.Services.Add(serviceInstance);
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            client.Calendar = calendar;
            var user = new User(
                Guid.NewGuid(),
                "alex@solvex.com",
                DateTime.UtcNow,
                 UserPermissions.Owner);
            var teamMember = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "Alex",
                "Kush")
            {
                User = user
            };
            client.TeamMemberAssignments.Add(teamMember);

            Database(db =>
            {
                db.Clients.Add(client);
                service.ServiceType = db.ServiceTypes.Find(IServiceTypeService.ComplianceId);
                db.Services.Add(service);
                db.ServiceInstances.Add(serviceInstance);
                db.Calendars.Add(calendar);
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var url = $"api/users/keycloak/{user.Id}";

            // Act
            var actualTeamMember = await GetJson<TeamMemberDto>(url);

            // Assert
            Assert.Equal(teamMember.FirstName, actualTeamMember.FirstName);
            Assert.Equal(teamMember.LastName, actualTeamMember.LastName);
            Assert.NotEmpty(actualTeamMember.Clients);

            AssertClient(client, actualTeamMember.Clients.First());

            void AssertClient(Client expectClient, ClientShortDto actualClient)
            {
                Assert.Equal(expectClient.Id, actualClient.Id);
                Assert.Equal(expectClient.CompanyName, actualClient.CompanyName);
                AssertService(actualClient.Services.First());
            }

            void AssertService(ServiceShortDto actualService)
            {
                Assert.Equal(serviceInstance.Id, actualService.Id);
                Assert.Equal(serviceInstance.Name, actualService.Name);
                Assert.Equal(serviceInstance.Template.ServiceType.Color, actualService.Color);
                Assert.Equal(serviceInstance.Template.ServiceType.Icon, actualService.Icon);
                Assert.Equal(serviceInstance.Template.ServiceType.FillIcon, actualService.FillIcon);
            }
        }

        [Fact]
        public async Task ExistClintsWithMembers_GetMembers_ReturnMembersWithClients()
        {
            // Arrange
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);

            var range = Enumerable.Range(0, 20);
            var clients = new List<Client>();
            var teamMembers = new List<TeamMember>();
            foreach (var seed in range)
            {
                var client = new Client(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"client_{seed}");
                var serviceInstance = new ServiceInstance(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "test service");
                serviceInstance.Template = service;
                client.Services.Add(serviceInstance);
                var calendar = new Calendar(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default);
                client.Calendar = calendar;
                clients.Add(client);
                var user = new User(
                     Guid.NewGuid(),
                     $"user@mail{seed}.com",
                     DateTime.UtcNow,
                     UserPermissions.Owner);
                var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex_{seed}",
                    "Kush")
                {
                    User = user
                };
                client.TeamMemberAssignments.Add(teamMember);
                teamMembers.Add(teamMember);
            }

            Database(db =>
            {
                db.AddRange(clients);
                service.ServiceType = db.ServiceTypes.Find(IServiceTypeService.ComplianceId);
                db.Services.Add(service);
                db.SaveChanges();
            });
            var expectMember1 = teamMembers[1];
            var expectMember2 = teamMembers[2];
            var url = $"api/users?limit=2&offset=1";

            // Act
            var actualPagination = await GetJson<PaginationDto<TeamMemberDto>>(url);

            // Assert
            Assert.Equal(20, actualPagination.TotalCount);
            Assert.Collection(
                actualPagination.Data,
                tm => AssertMember(expectMember1, tm),
                tm => AssertMember(expectMember2, tm));

            void AssertMember(TeamMember expectMember, TeamMemberDto actualTeamMember)
            {
                Assert.Equal(expectMember.Id, actualTeamMember.Id);
                Assert.Equal(expectMember.FirstName, actualTeamMember.FirstName);
                Assert.Equal(expectMember.LastName, actualTeamMember.LastName);
                Assert.Equal(expectMember.User.Email, actualTeamMember.Email);
                Assert.NotEmpty(actualTeamMember.Clients);
                var expectClient = clients.First(c => c.TeamMemberAssignments.Any(tm => tm.Id == expectMember.Id));
                AssertClient(expectClient, actualTeamMember.Clients.First());
            }

            void AssertClient(Client expectClient, ClientShortDto actualClient)
            {
                Assert.Equal(expectClient.Id, actualClient.Id);
                Assert.Equal(expectClient.CompanyName, actualClient.CompanyName);
                var expectService = expectClient.Services.First();
                AssertService(expectService, actualClient.Services.First());
            }

            void AssertService(ServiceInstance expectService, ServiceShortDto actualService)
            {
                Assert.Equal(expectService.Id, actualService.Id);
                Assert.Equal(expectService.Name, actualService.Name);
                Assert.Equal(expectService.Template.ServiceType.Color, actualService.Color);
                Assert.Equal(expectService.Template.ServiceType.Icon, actualService.Icon);
                Assert.Equal(expectService.Template.ServiceType.FillIcon, actualService.FillIcon);
            }
        }

        [Fact]
        public async Task ExistClintsWithMembers_GetMembersWithName_ReturnMembers()
        {
            // Arrange
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);

            var range = Enumerable.Range(0, 20);
            var clients = new List<Client>();
            var teamMembers = new List<TeamMember>();
            foreach (var seed in range)
            {
                var client = new Client(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"client_{seed}");
                var serviceInstance = new ServiceInstance(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "test service");
                serviceInstance.Template = service;
                client.Services.Add(serviceInstance);
                var calendar = new Calendar(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default);
                client.Calendar = calendar;
                clients.Add(client);
                var user = new User(
                     Guid.NewGuid(),
                     $"user@mail{seed}.com",
                     DateTime.UtcNow,
                     UserPermissions.Owner);
                var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex_{seed}",
                    "Kush")
                {
                    User = user
                };
                client.TeamMemberAssignments.Add(teamMember);
                teamMembers.Add(teamMember);
            }

            Database(db =>
            {
                db.AddRange(clients);
                service.ServiceType = db.ServiceTypes.Find(IServiceTypeService.ComplianceId);
                db.Services.Add(service);
                db.SaveChanges();
            });
            var expectMember1 = teamMembers[7];
            var expectMember2 = teamMembers[17];
            var url = $"api/users?name=7";

            // Act
            var actualPagination = await GetJson<PaginationDto<TeamMemberDto>>(url);

            // Assert
            Assert.Equal(2, actualPagination.TotalCount);
            Assert.Collection(
                actualPagination.Data,
                tm => AssertMember(expectMember1, tm),
                tm => AssertMember(expectMember2, tm));

            void AssertMember(TeamMember expectMember, TeamMemberDto actualTeamMember)
            {
                Assert.Equal(expectMember.Id, actualTeamMember.Id);
                Assert.Equal(expectMember.FirstName, actualTeamMember.FirstName);
                Assert.Equal(expectMember.LastName, actualTeamMember.LastName);
                Assert.Equal(expectMember.User.Email, actualTeamMember.Email);
                Assert.NotEmpty(actualTeamMember.Clients);
                var expectClient = clients.First(c => c.TeamMemberAssignments.Any(tm => tm.Id == expectMember.Id));
                AssertClient(expectClient, actualTeamMember.Clients.First());
            }

            void AssertClient(Client expectClient, ClientShortDto actualClient)
            {
                Assert.Equal(expectClient.Id, actualClient.Id);
                Assert.Equal(expectClient.CompanyName, actualClient.CompanyName);
                var expectService = expectClient.Services.First();
                AssertService(expectService, actualClient.Services.First());
            }

            void AssertService(ServiceInstance expectService, ServiceShortDto actualService)
            {
                Assert.Equal(expectService.Id, actualService.Id);
                Assert.Equal(expectService.Name, actualService.Name);
                Assert.Equal(expectService.Template.ServiceType.Color, actualService.Color);
                Assert.Equal(expectService.Template.ServiceType.Icon, actualService.Icon);
                Assert.Equal(expectService.Template.ServiceType.FillIcon, actualService.FillIcon);
            }
        }

        [Fact]
        public async Task ExistMember_ConfirmMembers_Success()
        {
            // Arrange

            var user = new User(
                 Guid.NewGuid(),
                 $"user@mail.com",
                 DateTime.UtcNow,
                 UserPermissions.Owner);
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush")
            {
                User = user
            };

            Database(db =>
            {
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var payload = new
            {
                userName = "new_username",
                password = "Q1w2e3r4t5y6U7i8o9p0&",
            };
            var url = $"api/users/{teamMember.Id}/confirm";

            // Act
            var response = await PutJson(url, payload);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            AssertDatabaseHas<TeamMember>(t =>
                t.Id == teamMember.Id
                && t.EmailConfirmed
            );
        }

        [Fact]
        public async Task ExistMember_ConfirmMembersWithPassWithoutNumbers_Fail()
        {
            // Arrange
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush");

            Database(db =>
            {
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var payload = new
            {
                userName = "new_username",
                password = "QWErtyQWErty&&",
            };
            var url = $"api/users/{teamMember.Id}/confirm";

            // Act
            var response = await PutJson(url, payload);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            AssertDatabaseHasNot<TeamMember>(t =>
                t.Id == teamMember.Id
                && t.EmailConfirmed
            );
        }

        [Fact]
        public async Task ExistMember_ConfirmMembersWithPassWithoutSpecialCharacter_Fail()
        {
            // Arrange
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush");

            Database(db =>
            {
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var payload = new
            {
                userName = "new_username",
                password = "QWErtyQWErty11",
            };
            var url = $"api/users/{teamMember.Id}/confirm";

            // Act
            var response = await PutJson(url, payload);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            AssertDatabaseHasNot<TeamMember>(t =>
                t.Id == teamMember.Id
                && t.EmailConfirmed
            );
        }

        [Fact]
        public async Task ExistMember_ConfirmMembersWithPassWithoutUpperCase_Fail()
        {
            // Arrange
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush");

            Database(db =>
            {
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var payload = new
            {
                userName = "new_username",
                password = "qwertyqwerty1&",
            };
            var url = $"api/users/{teamMember.Id}/confirm";

            // Act
            var response = await PutJson(url, payload);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            AssertDatabaseHasNot<TeamMember>(t =>
                t.Id == teamMember.Id
                && t.EmailConfirmed
            );
        }

        [Fact]
        public async Task ExistMember_ConfirmMembersWithPassWithoutLowCase_Fail()
        {
            // Arrange
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush");

            Database(db =>
            {
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var payload = new
            {
                userName = "new_username",
                password = "QWERTYQWERTY1&",
            };
            var url = $"api/users/{teamMember.Id}/confirm";

            // Act
            var response = await PutJson(url, payload);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            AssertDatabaseHasNot<TeamMember>(t =>
                t.Id == teamMember.Id
                && t.EmailConfirmed
            );
        }

        [Fact]
        public async Task ExistMember_ConfirmMembersWithPass11Characters_Fail()
        {
            // Arrange
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush");

            Database(db =>
            {
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var payload = new
            {
                userName = "new_username",
                password = "Q1W2E3r4t5&",
            };
            var url = $"api/users/{teamMember.Id}/confirm";

            // Act
            var response = await PutJson(url, payload);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            AssertDatabaseHasNot<TeamMember>(t =>
                t.Id == teamMember.Id
                && t.EmailConfirmed
            );
        }

        [Fact]
        public async Task ExistMember_UpdateMember_Success()
        {
            // Arrange
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush");
            Database(db =>
            {
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var payload = new
            {
                lastName = "Doe",
                firstName = "John",
                physicalAddress = "Palm Beach",
                socialSecurityNumber = 1234,
                birthDate = DateTime.UtcNow,
                companyMailingAddress = "NY",
                cellPhone = "123456",
                fullName = "John Doe",
                address = "LA",
                phone = "098654",
                supervisorId = teamMember.Id,
                salary = "123456",
                startDate = DateTime.UtcNow,
                nameOfCollege = "Jonny",
                degree = "master"
            };

            var url = $"api/users/{teamMember.Id}";

            // Act
            var response = await PutJson(url, payload);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var actualTeamMember = db.TeamMembers
                    .Include(tm => tm.Supervisor)
                    .Single(tm => tm.Id == teamMember.Id);
                Assert.Equal(payload.firstName, actualTeamMember.FirstName);
                Assert.Equal(payload.lastName, actualTeamMember.LastName);
                Assert.Equal(payload.physicalAddress, actualTeamMember.PhysicalAddress);
                Assert.Equal(payload.firstName, actualTeamMember.FirstName);
                Assert.Equal(payload.socialSecurityNumber, actualTeamMember.SocialSecurityNumber);
                Assert.Equal(payload.birthDate, actualTeamMember.BirthDate);
                Assert.Equal(payload.companyMailingAddress, actualTeamMember.CompanyMailingAddress);
                Assert.Equal(payload.cellPhone, actualTeamMember.CellPhone);
                Assert.Equal(payload.fullName, actualTeamMember.FullName);
                Assert.Equal(payload.address, actualTeamMember.Address);
                Assert.Equal(payload.phone, actualTeamMember.Phone);
                Assert.Equal(payload.supervisorId, actualTeamMember.Supervisor.Id);
                Assert.Equal(payload.salary, actualTeamMember.Salary);
                Assert.Equal(payload.nameOfCollege, actualTeamMember.NameOfCollege);
                Assert.Equal(payload.degree, actualTeamMember.Degree);
            });
        }

        [Fact]
        public async Task TeamMember_ArchiveTeamMember_TeamMemberArchived()
        {
            //Assert
            var user = new User(
                Guid.NewGuid(),
                "joe@doe.com",
                DateTime.UtcNow,
                UserPermissions.Owner);
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush")
            {
                User = user
            };
            Database(db =>
            {
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var url = $"api/users/archive/{teamMember.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.TeamMembers
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == teamMember.Id);

                Assert.NotNull(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task TeamMemberAssigned_ArchiveTeamMember_TeamMemberArchived()
        {
            //Assert
            var user = new User(
                Guid.NewGuid(),
                "joe@doe.com",
                DateTime.UtcNow,
                UserPermissions.Owner);
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush")
            {
                User = user
            };
            var service = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var task = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task")
            {
                Service = service
            };
            task.Assignees.Add(teamMember);
            task.Supervisors.Add(teamMember);
            Database(db =>
            {
                db.TaskInstances.Add(task);
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var url = $"api/users/archive/{teamMember.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.TeamMembers
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == teamMember.Id);

                Assert.NotNull(data.ArchiveInfo);
            });
            Database(db =>
            {
                var actualTask = db.TaskInstances
                    .Include(ti => ti.Assignees)
                    .Include(ti => ti.Supervisors)
                    .First(ti => ti.Id == task.Id);
                Assert.Empty(actualTask.Assignees);
                Assert.Empty(actualTask.Supervisors);
            });
        }

        [Fact]
        public async Task TeamMemberArchive_ReactivateTeamMember_Success()
        {
            //Assert
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush")
            {
                ArchiveInfo = archive
            };
            var service = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var task = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task")
            {
                Service = service
            };
            task.Assignees.Add(teamMember);
            task.Supervisors.Add(teamMember);
            Database(db =>
            {
                db.TaskInstances.Add(task);
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var url = $"api/users/archive/{teamMember.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.TeamMembers
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == teamMember.Id);

                Assert.Null(data.ArchiveInfo);
            });
            Database(db =>
            {
                var actualTask = db.TaskInstances
                    .Include(ti => ti.Assignees)
                    .Include(ti => ti.Supervisors)
                    .First(ti => ti.Id == task.Id);
                Assert.NotEmpty(actualTask.Assignees);
                Assert.NotEmpty(actualTask.Supervisors);
            });
        }

        [Fact]
        public async Task TeamMemberNotArchive_ReactivateTeamMember_Fail()
        {
            //Assert
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush");
            var service = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var task = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task")
            {
                Service = service
            };
            task.Assignees.Add(teamMember);
            task.Supervisors.Add(teamMember);
            Database(db =>
            {
                db.TaskInstances.Add(task);
                db.TeamMembers.Add(teamMember);
                db.SaveChanges();
            });

            var url = $"api/users/archive/{teamMember.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.TeamMembers
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == teamMember.Id);

                Assert.Null(data.ArchiveInfo);
            });
            Database(db =>
            {
                var actualTask = db.TaskInstances
                    .Include(ti => ti.Assignees)
                    .Include(ti => ti.Supervisors)
                    .First(ti => ti.Id == task.Id);
                Assert.NotEmpty(actualTask.Assignees);
                Assert.NotEmpty(actualTask.Supervisors);
            });
        }

        [Fact]
        public async Task TeamMemberArchive_GetArchiveCount_Count1()
        {
            //Assert
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var archiveTeamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush")
            {
                ArchiveInfo = archive
            };
            var notArchiveTeamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush");
            Database(db =>
            {
                db.TeamMembers.AddRange(archiveTeamMember, notArchiveTeamMember);
                db.SaveChanges();
            });

            var url = $"api/users/archive/count";

            // Act
            var count = await GetJson<ArchiveCountDto>(url);

            // Assert
            Assert.Equal(1, count.Count);
        }

        [Fact]
        public async Task TeamMemberArchive_GetArchiveTeamMembers_ReturnTeamMembers()
        {
            //Assert
            var client1 = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var client2 = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var archiveTeamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush")
            {
                ArchiveInfo = archive
            };
            var notArchiveTeamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush");
            client1.TeamMemberAssignments.Add(archiveTeamMember);
            client2.TeamMemberAssignments.Add(archiveTeamMember);
            Database(db =>
            {
                db.Clients.AddRange(client1, client2);
                db.TeamMembers.AddRange(archiveTeamMember, notArchiveTeamMember);
                db.SaveChanges();
            });

            var url = $"api/users/archive?limit=3";

            // Act
            var users = await GetJson<IEnumerable<ArchiveTeamMemberDto>>(url);

            // Assert
            Assert.Collection(users,
                t => AssertArchive(archiveTeamMember, t));


            void AssertArchive(TeamMember expectTeamMember, ArchiveTeamMemberDto actualTeamMember)
            {
                Assert.Equal(expectTeamMember.Id, actualTeamMember.Id);
                Assert.Equal(2, actualTeamMember.ClientsCount);
            }
        }

        [Fact]
        public async Task ExistMember_TurnOffNotification_Success()
        {
            // Arrange
            var teamMember = new TeamMember(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"Alex",
                    "Kush");
            var notificationOption = new NotificationOption(Guid.NewGuid())
            {
                IsOn = true,
                NotificationType = NotificationType.TaskDueDate,
                TeamMember = teamMember
            };

            Database(db =>
            {
                db.NotificationOptions.Add(notificationOption);
                db.SaveChanges();
            });

            var payload = new
            {
                notificationType = "TaskDueDate",
                isOn = false
            };

            var url = $"api/users/{teamMember.Id}/notifications";

            // Act
            var request = await PutJson(url, payload);

            // Assert
            request.EnsureSuccessStatusCode();
            Database(db =>
            {
                var notificationOption = db.NotificationOptions
                    .Single(no => no.TeamMember.Id == teamMember.Id && no.NotificationType == NotificationType.TaskDueDate);
                Assert.False(notificationOption.IsOn);
            });
        }
    }
}
