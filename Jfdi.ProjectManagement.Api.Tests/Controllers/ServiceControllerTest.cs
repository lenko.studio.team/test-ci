﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;
using Task = System.Threading.Tasks.Task;
using JfdiTask = Jfdi.ProjectManagement.Api.Models.Task;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;
using Jfdi.ProjectManagement.Api.Services;
using Microsoft.EntityFrameworkCore;
using Jfdi.ProjectManagement.Api.Models.Attachments;
using Jfdi.ProjectManagement.Api.Dtos.Tasks.Instances;
using Jfdi.ProjectManagement.Api.Dtos.Services;
using Jfdi.ProjectManagement.Api.Models.Archives;
using Jfdi.ProjectManagement.Api.Dtos.Clients;

namespace Jfdi.ProjectManagement.Api.Tests.Controllers
{
    public class ServiceControllerTest : TestCase
    {
        public ServiceControllerTest(TestApplicationFactory<Startup> fixture, ITestOutputHelper testOutput)
            : base(fixture, testOutput)
        {
        }

        [Fact]
        public async Task MultipleServiceExist_GetServicesWithLimit_ReturnsLimitedServeses()
        {
            // Arrange
            var range = Enumerable.Range(0, 20);
            var services = new List<Service>();
            foreach (var seed in range)
            {
                var service = new Service(
                $"service_{seed}",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
                {
                    Links = new [] { "facebook.com" }
                };
                var attachment = new GDriveAttachment(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "http://www.google.com")
                {
                    Name = "test"
                };
                service.Attachments.Add(attachment);
                services.Add(service);
            }
            Database(db =>
            {
                var serviceType = db.ServiceTypes.Find(IServiceTypeService.BookkeepingId);
                services.ForEach(s => s.ServiceType = serviceType);
                db.Services.AddRange(services);
                db.SaveChanges();
            });

            var removedTask = services[7];
            var url = $"api/services?limit=3";

            // Act
            var events = await GetJson<List<ServiceDto>>(url);

            // Assert
            Assert.Collection(
                events,
                x => AssertService(services[0], x),
                x => AssertService(services[1], x),
                x => AssertService(services[2], x));

            static void AssertService(Service expectService, ServiceDto actualService)
            {
                Assert.Equal(expectService.Id, actualService.Id);
                Assert.Equal(expectService.Name, actualService.Name);
                Assert.Equal(expectService.ServiceType.Id, actualService.ServiceType.Id);
                Assert.Equal(expectService.Links.First(), actualService.Links.First());
            }
        }
        [Fact]
        public async Task MultipleServiceExist_GetServicesByServiceType_ReturnsLimitedServeses()
        {
            // Arrange
            var range = Enumerable.Range(0, 20);
            var services = new List<Service>();
            foreach (var seed in range)
            {
                var service = new Service(
                $"service_{seed}",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
                {
                    Links = new[] { "facebook.com" }
                };
                var attachment = new GDriveAttachment(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "http://www.google.com")
                {
                    Name = "test"
                };
                service.Attachments.Add(attachment);
                services.Add(service);
            }
            Database(db =>
            {
                var serviceType = db.ServiceTypes.Find(IServiceTypeService.BookkeepingId);
                services.ForEach(s => s.ServiceType = serviceType);
                serviceType = db.ServiceTypes.Find(IServiceTypeService.MonthEndCloseId);
                services.Take(2).ToList().ForEach(s => s.ServiceType = serviceType);
                db.Services.AddRange(services);
                db.SaveChanges();
            });

            var url = $"api/services?limit=3&serviceTypeIds={IServiceTypeService.MonthEndCloseId}";

            // Act
            var events = await GetJson<List<ServiceDto>>(url);

            // Assert
            Assert.Collection(
                events,
                x => AssertService(services[0], x),
                x => AssertService(services[1], x));

            static void AssertService(Service expectService, ServiceDto actualService)
            {
                Assert.Equal(expectService.Id, actualService.Id);
                Assert.Equal(expectService.Name, actualService.Name);
                Assert.Equal(expectService.ServiceType.Id, actualService.ServiceType.Id);
                Assert.Equal(expectService.Links.First(), actualService.Links.First());
            }
        }

        [Fact]
        public async Task MultipleServices_GetService_ReturnService()
        {
            // Arrange
            var range = Enumerable.Range(0, 20);
            var services = new List<Service>();
            foreach (var seed in range)
            {
                var service = new Service(
                $"service_{seed}",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
                {
                    Description = $"description_{seed}",
                    RecurrenceRule = "RRULE",
                    StartDateUtc = DateTime.UtcNow.AddDays(seed),
                    EndDateUtc = DateTime.UtcNow.AddDays(seed)
                };
                var attachment = new GDriveAttachment(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "http://www.google.com")
                {
                    Name = "test"
                };
                service.Attachments.Add(attachment);
                services.Add(service);
            }
            Database(db =>
            {
                var serviceType = db.ServiceTypes.Find(IServiceTypeService.BookkeepingId);
                services.ForEach(s => s.ServiceType = serviceType);
                db.Services.AddRange(services);
                db.SaveChanges();
            });

            var expectService = services[5];
            var url = $"api/services/{expectService.Id}";

            // Act
            var actualService = await GetJson<ServiceDto>(url);

            // Assert
            Assert.Equal(expectService.Id, actualService.Id);
            Assert.Equal(expectService.Name, actualService.Name);
            Assert.Equal(expectService.ServiceType.Color, actualService.Color);
            Assert.Equal(expectService.Description, actualService.Description);
            Assert.Equal(expectService.RecurrenceRule, actualService.RecurrenceRule);
            Assert.Equal(expectService.StartDateUtc, actualService.StartDateUtc);
            Assert.Equal(expectService.EndDateUtc, actualService.EndDateUtc);
            Assert.Equal(expectService.ServiceType.Id, actualService.ServiceType.Id);
            Assert.Equal(expectService.IsHighPriority, actualService.IsHighPriority);
            Assert.Equal(expectService.Attachments.First().Id, actualService.Attachments.First().Id);
            Assert.Equal(0, actualService.ClientsCount);
        }

        [Fact]
        public async Task ServiceWithMultipleClients_GetService_ReturnService()
        {
            // Arrange
            var service = new Service(
                $"service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
            {
                Description = $"description",
                RecurrenceRule = "RRULE",
                StartDateUtc = DateTime.UtcNow,
                EndDateUtc = DateTime.UtcNow.AddDays(1)
            };
            var range = Enumerable.Range(0, 20);
            foreach (var seed in range)
            {
                var client = new Client(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"client_{seed}");
                var serviceInstance = new ServiceInstance(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    $"service instance_{seed}")
                {
                    Client = client
                };
                service.Instancies.Add(serviceInstance);
            }
            Database(db =>
            {
                var serviceType = db.ServiceTypes.Find(IServiceTypeService.BookkeepingId);
                service.ServiceType = serviceType;
                db.Services.Add(service);
                db.SaveChanges();
            });

            var url = $"api/services/{service.Id}";

            // Act
            var actualService = await GetJson<ServiceDto>(url);

            // Assert
            Assert.Equal(service.Id, actualService.Id);
            Assert.Equal(service.Name, actualService.Name);
            Assert.Equal(service.ServiceType.Color, actualService.Color);
            Assert.Equal(service.Description, actualService.Description);
            Assert.Equal(service.RecurrenceRule, actualService.RecurrenceRule);
            Assert.Equal(service.StartDateUtc, actualService.StartDateUtc);
            Assert.Equal(service.EndDateUtc, actualService.EndDateUtc);
            Assert.Equal(service.ServiceType.Id, actualService.ServiceType.Id);
            Assert.Equal(service.IsHighPriority, actualService.IsHighPriority);
            Assert.Equal(20, actualService.ClientsCount);
        }

        [Fact]
        public async Task MultipleServices_GetService_Return404()
        {
            // Arrange
            var range = Enumerable.Range(0, 20);
            var services = new List<Service>();
            foreach (var seed in range)
            {
                var service = new Service(
                $"service_{seed}",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
                services.Add(service);
            }
            Database(db =>
            {
                var serviceType = db.ServiceTypes.Find(IServiceTypeService.BookkeepingId);
                services.ForEach(s => s.ServiceType = serviceType);
                db.Services.AddRange(services);
                db.SaveChanges();
            });

            var expectServiceId = Guid.NewGuid();
            var url = $"api/services/{expectServiceId}";

            // Act
            var request = GetJson<TaskDto>(url);

            // Assert
            await Assert.ThrowsAsync<NotNullException>(async () => await request);
        }

        [Fact]
        public async Task NewService_PostService_Success()
        {
            // Arrange
            var attachment = new GDriveAttachment(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "http://www.google.com")
            {
                Name = "test"
            };
            var user = new TeamMember(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "John",
                "Doe");
            var software = new Software(
                "software",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
            {
                Description = "description",
                Links = new List<string> { "facebook.com" }
            };
            Database(db =>
            {
                db.TeamMembers.Add(user);
                db.Attachments.Add(attachment);
                db.Softwares.Add(software);
                db.SaveChanges();
            });
            var task = new
            {
                title = "test",
                recurrenceRule = "FREQ=DAILY;INTERVAL=1;COUNT=2",
                dueDateUtc = DateTime.UtcNow,
                forReview = true,
                supervisorId = user.Id,
                role = TeamMemberRole.Manager.ToString(),
                info = "some info"
            };
            var payload = new
            {
                name = "test",
                serviceTypeId = IServiceTypeService.AccountsPayablesId,
                recurrenceRule = "FREQ=DAILY;INTERVAL=1;COUNT=2",
                startDateUtc = DateTime.UtcNow,
                endDateUtc = DateTime.UtcNow,
                isHighPriority = true,
                description = "description",
                links = new[] { "google.com" },
                softwareIds = new[] { software.Id },
                tasks = new[] { task },
                attachments = new[] { attachment.Id }
            };
            var url = $"api/services";

            // Act
            var response = await PostJson(url, payload);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            Database(db =>
            {
                var actualService = db.Services
                    .Include(s => s.ServiceType)
                    .Include(s => s.Softwares)
                    .Include(s => s.Tasks)
                    .Include(s => s.Attachments)
                    .Single(x => x.Name == payload.name);
                Assert.Equal(payload.serviceTypeId, actualService.ServiceType.Id);
                Assert.Equal(payload.isHighPriority, actualService.IsHighPriority);
                Assert.Equal(payload.description, actualService.Description);
                Assert.Equal(payload.recurrenceRule, actualService.RecurrenceRule);
                Assert.Equal(payload.startDateUtc, actualService.StartDateUtc);
                Assert.Equal(payload.endDateUtc, actualService.EndDateUtc);
                Assert.Collection(actualService.Links,
                    s=>Assert.Equal(payload.links[0], s));
                Assert.NotEmpty(actualService.Softwares);
                Assert.NotEmpty(actualService.Tasks);
                Assert.NotEmpty(actualService.Attachments);
                Assert.Equal(attachment.Id, actualService.Attachments.First().Id);
            });
        }

        [Fact]
        public async Task NewServiceWrongSoftware_PostService_Return404()
        {
            // Arrange
            var softwareId = Guid.NewGuid();
            var userId = Guid.NewGuid();
            var task = new
            {
                title = "test",
                recurrenceRule = "FREQ=DAILY;INTERVAL=1;COUNT=2",
                dueDateUtc = DateTime.UtcNow,
                forReview = true,
                supervisorId = userId,
                role = TeamMemberRole.Manager.ToString(),
                info = "some info"
            };
            var payload = new
            {
                name = "test",
                serviceTypeId = IServiceTypeService.AccountsPayablesId,
                recurring = "FREQ=DAILY;INTERVAL=1;COUNT=2",
                isHighPriority = true,
                description = "description",
                links = new[] { "google.com" },
                softwareIds = new[] { softwareId },
                tasks = new[] { task },
            };
            var url = $"api/services";

            // Act
            var response = await PostJson(url, payload);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
            AssertDatabaseHasNot<Service>(s =>
                s.Name == payload.name);
        }

        [Fact]
        public async Task MultipleServices_UpdateService_ServiceUpdated()
        {
            // Arrange
            var range = Enumerable.Range(0, 20);
            var services = new List<Service>();
            foreach (var seed in range)
            {
                var service = new Service(
                    $"service_{seed}",
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default);
                var attachment = new GDriveAttachment(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "http://www.google.com")
                {
                    Name = "test"
                };
                service.Attachments.Add(attachment);
                services.Add(service);
            }
            var newAttachment = new GDriveAttachment(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "http://www.google.com")
            {
                Name = "test_1"
            };
            Database(db =>
            {
                var serviceType = db.ServiceTypes.Find(IServiceTypeService.BookkeepingId);
                services.ForEach(s => s.ServiceType = serviceType);
                db.Services.AddRange(services);
                db.Attachments.Add(newAttachment);
                db.SaveChanges();
            });

            var updatedService = services[2];
            var url = $"api/services/{updatedService.Id}";
            var updatedValue = new
            {
                name = "new_updated_test",
                recurrenceRule = "RRULE:FREQ=WEEKLY;INTERVAL=1;WKST=MO",
                attachments = new [] { newAttachment.Id }
            };

            // Act
            var response = await PutJson(url, updatedValue);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            AssertDatabaseHas<Service>(x =>
                x.Id == updatedService.Id &&
                x.Name == updatedValue.name);
            Database(db =>
            {
                var expectService = db.Services
                    .Include(s => s.Attachments)
                    .Single(s => s.Id == updatedService.Id);
                Assert.Equal(1, expectService.Attachments.Count);
                Assert.Equal(newAttachment.Id, expectService.Attachments.First().Id);
            });
        }

        [Fact]
        public async Task MultipleTasksInService_RemoveService_ReturnsError()
        {
            // Arrange
            var range = Enumerable.Range(0, 20);
            var services = new List<Service>();
            foreach (var seed in range)
            {
                var service = new Service(
                    $"service_{seed}",
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default);
                services.Add(service);

                var serviceTask = new Models.Task(
                    $"task_{seed}",
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default);
                service.Tasks.Add(serviceTask);
            }
            Database(db =>
            {
                db.Services.AddRange(services);
                db.SaveChanges();
            });

            var deletedService = services[2];
            var url = $"api/services/{deletedService.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.False(response.IsSuccessStatusCode);

            AssertDatabaseHas<Service>(x =>
                x.Id == deletedService.Id);
        }

        [Fact]
        public async Task NoTaskInService_RemoveService_ServiceRemoved()
        {
            // Arrange
            var software = new Software(
                "soft",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var service = new Service(
                $"service_1",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            service.Softwares.Add(software);
            Database(db =>
            {
                var serviceType = db.ServiceTypes.Find(IServiceTypeService.BookkeepingId);
                service.ServiceType = serviceType;
                db.Services.Add(service);
                db.Softwares.Add(software);
                db.SaveChanges();
            });

            var url = $"api/services/{service.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            AssertDatabaseHasNot<Service>(x =>
                x.Id == service.Id);
            AssertDatabaseHasNot<Software>(x =>
                x.Id == software.Id);
        }

        [Fact]
        public async Task NewServiceTask_AddTask_TaskAdded()
        {
            // Arrange
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client")
            {
                Calendar = calendar
            };
            var service = new Service(
                "service_8",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service instance")
            {
                Client = client,
                Template = service
            };
            var serviceTask = new JfdiTask(
                "test_1",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
            {
                StartDateUtc = DateTime.UtcNow,
                EndDateUtc = DateTime.UtcNow.AddDays(1)
            };
            Database(db =>
            {
                db.ServiceInstances.Add(serviceInstance);
                db.Tasks.Add(serviceTask);
                db.SaveChanges();
            });
            var url = $"api/services/{service.Id}/tasks";

            // Act
            var response = await PostJson(url, serviceTask.Id);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            AssertDatabaseHas<Service>(x =>
                x.Id == service.Id);
            Database(db =>
            {
                var taskEvent = db.TaskEvents.Single(te =>
                    te.Instance.Template.Id == serviceTask.Id);
            });
        }

        [Fact]
        public async Task MultipleTasksExist_GetForService_ReturnsLimitedTasks()
        {
            // Arrange
            var service = new Service(
                "service_5",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var range = Enumerable.Range(0, 20);
            var serviceTasks = new List<Models.Task>();
            foreach (var seed in range)
            {
                var serviceTask = new Models.Task(
                    $"test_{seed}",
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default)
                {
                    RecurrenceRule = "test recurring",
                    Info = $"info_{seed}"
                };
                serviceTasks.Add(serviceTask);
                service.Tasks.Add(serviceTask);
            }
            Database(db =>
            {
                db.Services.Add(service);
                db.SaveChanges();
            });

            var removedTask = serviceTasks[7];
            var url = $"api/services/{service.Id}/tasks?limit=3";

            // Act
            var events = await GetJson<List<TaskDto>>(url);

            // Assert
            Assert.Collection(
                events,
                x => AssertTask(serviceTasks[0], x),
                x => AssertTask(serviceTasks[1], x),
                x => AssertTask(serviceTasks[2], x));

            static void AssertTask(JfdiTask expectTask, TaskDto actualTask)
            {
                Assert.Equal(expectTask.Id, actualTask.Id);
                Assert.Equal(expectTask.Info, actualTask.Info);
                Assert.Equal(expectTask.RecurrenceRule, actualTask.RecurrenceRule);
                Assert.Equal(expectTask.Title, actualTask.Title);
            }
        }

        [Fact]
        public async Task MultipleTasksExist_DeleteTaskFromService_RemoveTask()
        {
            // Arrange
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var service = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var task = new JfdiTask(
                "task",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "test service");
            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            client.Calendar = calendar;
            var calendarEvent = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "testEvent",
                DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1))
            {
                CalendarId = calendar.Id
            };
            calendar.CalendarEvents.Add(calendarEvent);
            Database(db =>
            {
                db.Clients.Add(client);

                db.Services.Add(service);
                service.Tasks.Add(task);
                serviceInstance.Template = service;

                db.Tasks.Add(task);
                client.Services.Add(serviceInstance);

                db.ServiceInstances.Add(serviceInstance);
                serviceInstance.Template = service;
                serviceInstance.Client = client;

                db.TaskInstances.Add(taskInstance);
                serviceInstance.Tasks.Add(taskInstance);
                taskInstance.Service = serviceInstance;
                taskInstance.Event = calendarEvent;
                taskInstance.Template = task;

                db.Calendars.Add(calendar);
                db.CalendarEvents.Add(calendarEvent);

                db.SaveChanges();
            });

            var url = $"api/services/{service.Id}/tasks/{task.Id}";

            // Act
            var events = await DeleteJson(url);

            // Assert
            Assert.True(events.IsSuccessStatusCode);

            AssertDatabaseHas((Models.Task x) =>
                x.Id == task.Id);

            AssertDatabaseHas<Service>(x =>
                x.Id == service.Id);

            AssertDatabaseHasNot<TaskInstance>(x =>
                x.Id == taskInstance.Id);

            AssertDatabaseHasNot<CalendarEvent>(x =>
                x.Id == calendarEvent.Id);
        }

        [Fact]
        public async Task Service_ArchiveService_ServiceArchived()
        {
            //Assert
            var notArchiveService = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            Database(db =>
            {
                db.Services.Add(notArchiveService);
                db.SaveChanges();
            });

            var url = $"api/services/archive/{notArchiveService.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.Services
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == notArchiveService.Id);

                Assert.NotNull(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task ArchivedService_ArchiveService_ServiceNotArchived()
        {
            //Assert
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var archiveService = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
            {
                ArchiveInfo = archive,
            };
            Database(db =>
            {
                db.Services.Add(archiveService);
                db.SaveChanges();
            });

            var url = $"api/services/archive/{archiveService.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.Services
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == archiveService.Id);

                Assert.NotNull(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task ServiceInClient_ArchiveService_ServiceNotArchived()
        {
            //Assert
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var notArchiveService = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var taskEvent = new TaskEvent(
                Guid.NewGuid());
            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            taskInstance.TaskEvents.Add(taskEvent);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service")
            {
                Client = client,
                Template = notArchiveService
            };
            serviceInstance.Tasks.Add(taskInstance);
            Database(db =>
            {
                taskEvent.State = db.TaskStates.Find(ITaskStateService.TodoStateId);
                db.ServiceInstances.Add(serviceInstance);
                db.SaveChanges();
            });

            var url = $"api/services/archive/{notArchiveService.Id}";

            // Act
            var response = await PutJson(url, default);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.Services
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == notArchiveService.Id);

                Assert.Null(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task ServiceArchive_ReactivateService_Success()
        {
            //Assert
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var archiveService = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
            {
                ArchiveInfo = archive,
            };
            Database(db =>
            {
                db.Services.Add(archiveService);
                db.SaveChanges();
            });

            var url = $"api/services/archive/{archiveService.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.Services
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == archiveService.Id);

                Assert.Null(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task ServiceNotArchive_ReactivateService_Fail()
        {
            //Assert
            var notArchiveService = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            Database(db =>
            {
                db.Services.Add(notArchiveService);
                db.SaveChanges();
            });

            var url = $"api/services/archive/{notArchiveService.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
            Database(db =>
            {
                var data = db.Services
                    .Include(d => d.ArchiveInfo)
                    .IgnoreQueryFilters()
                    .Single(d => d.Id == notArchiveService.Id);

                Assert.Null(data.ArchiveInfo);
            });
        }

        [Fact]
        public async Task ServiceArchive_GetArchiveCount_Count1()
        {
            //Assert
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var archiveService = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
            {
                ArchiveInfo = archive,
            };
            var notArchiveService = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            Database(db =>
            {
                db.Services.Add(archiveService);
                db.Services.Add(notArchiveService);
                db.SaveChanges();
            });

            var url = $"api/services/archive/count";

            // Act
            var count = await GetJson<ArchiveCountDto>(url);

            // Assert
            Assert.Equal(1, count.Count);
        }

        [Fact]
        public async Task ServiceArchive_GetArchiveServicies_ReturnServicies()
        {
            //Assert
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client");
            var archive = new ArchiveData(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var archiveService = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default)
            {
                ArchiveInfo = archive,
            };
            var notArchiveService = new Service(
                "service",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service")
            {
                Client = client,
                Template = notArchiveService
            };
            
            Database(db =>
            {
                db.Services.Add(archiveService);
                db.ServiceInstances.Add(serviceInstance);
                db.SaveChanges();
            });

            var url = $"api/services/archive?limit=3";

            // Act
            var servicies = await GetJson<IEnumerable<ArchivedServiceDto>>(url);

            // Assert
            Assert.Collection(servicies,
                s => AssertArchive(archiveService, s));


            void AssertArchive(Service expectService, ArchivedServiceDto actualService)
            {
                Assert.Equal(expectService.Id, actualService.Id);
            }
        }

        [Fact]
        public async Task MultipleServiceExist_GetClients_ReturnsClients()
        {
            // Arrange
            var existService = new Service(
                "service 1",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var notExistService = new Service(
                "service 2",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var client = new Client(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "client 1");
            var serviceInstance = new ServiceInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service instance 1")
            {
                Template = existService,
                Client = client
            };
            Database(db =>
            {
                db.ServiceInstances.Add(serviceInstance);
                db.Services.Add(notExistService);
                db.SaveChanges();
            });

            var url = $"api/services/{existService.Id}/clients";

            // Act
            var clients = await GetJson<List<ClientShortDto>>(url);

            // Assert
            Assert.Collection(
                clients,
                x => AssertClient(client, x));

            static void AssertClient(Client expectClient, ClientShortDto actualClient)
            {
                Assert.Equal(expectClient.Id, actualClient.Id);
                Assert.Equal(expectClient.CompanyName, actualClient.CompanyName);
            }
        }
    }
}
