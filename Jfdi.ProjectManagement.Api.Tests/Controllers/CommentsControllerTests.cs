﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos.Comments;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Tests.Controllers
{
    public class CommentsControllerTests : TestCase
    {
        public CommentsControllerTests(TestApplicationFactory<Startup> fixture, ITestOutputHelper testOutput)
            : base(fixture, testOutput)
        {
        }

        [Fact()]
        public async Task NewComment_PostCommentToTask_CommentIsCreated()
        {
            // Arrange
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            Database(db =>
            {
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var payload = new
            {
                content = "test"
            };
            var url = $"api/comments/tasks/{taskEvent.Id}";

            // Act
            var actualComment = await PostJson<CommentDto>(url, payload);

            // Assert
            Fixture.Database(db =>
            {
                var actualTask = db.TaskEvents
                    .Include(ti => ti.Comments)
                    .Single(ti => ti.Id == taskEvent.Id);

                Assert.Equal(actualTask.Comments.First().Id, actualComment.Id);
            });
            AssertDatabaseHas<Comment>(com =>
                com.Id == actualComment.Id);
        }

        [Fact()]
        public async Task NewComment_DeleteCommentFromTask_Success()
        {
            // Arrange
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var comment = new Comment(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "payload");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            taskEvent.Comments.Add(comment);

            Database(db =>
            {
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/comments/{comment.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            Fixture.Database(db =>
            {
                var actualTask = db.TaskEvents
                    .Include(ti => ti.Comments)
                    .Single(ti => ti.Id == taskEvent.Id);

                Assert.Empty(actualTask.Comments);
            });

            AssertDatabaseHasNot<Comment>(com =>
                com.Id == comment.Id);
        }

        [Fact()]
        public async Task ExistComments_GetCommentsFromTask_ReturnComments()
        {
            // Arrange
            var user = new User(default, "", DateTime.UtcNow, Models.Users.UserPermissions.Owner);
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                user.Id,
                "service");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            var range = Enumerable.Range(0, 20);
            foreach(var seed in range)
            {
                var comment = new Comment(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    user.Id,
                    $"payload_{seed}");
                taskEvent.Comments.Add(comment);
            }

            Database(db =>
            {
                db.Users.Add(user);
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var url = $"api/comments/tasks/{taskEvent.Id}";

            // Act
            var actualComments = await GetJson<IEnumerable<CommentDto>>(url);

            // Assert
            foreach(var seed in range)
            {
                var expectComment = taskEvent.Comments.ToArray()[seed];
                var actualComment = actualComments.ToArray()[seed];
                Assert.Equal(expectComment.Id, actualComment.Id);
                Assert.Equal(expectComment.Data, actualComment.Content);
            }
        }

        [Fact()]
        public async Task ExistComment_UpdateComment_Success()
        {
            // Arrange
            var tasksInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "service");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = tasksInstance
            };
            var comment = new Comment(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "payload");
            taskEvent.Comments.Add(comment);

            Database(db =>
            {
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var payload = new
            {
                content = "test"
            };
            var url = $"api/comments/{comment.Id}";

            // Act
            var response = await PutJson(url, payload);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            AssertDatabaseHas<Comment>(com =>
                com.Id == comment.Id
                && com.Data == payload.content);
        }
    }
}
