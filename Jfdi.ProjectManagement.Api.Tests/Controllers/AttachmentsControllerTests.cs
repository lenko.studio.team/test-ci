﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Dtos.Attachments;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Models.Attachments;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Microsoft.AspNetCore.Http;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Tests.Controllers
{
    public class AttachmentsControllerTests : TestCase
    {
        public AttachmentsControllerTests(TestApplicationFactory<Startup> fixture, ITestOutputHelper testOutput)
            : base(fixture, testOutput)
        {
        }

        [Fact()]
        public async Task NewGDriveAttachment_PostAttachment_Success()
        {
            // Arrange
            var payload = new
            {
                name = "image.png",
                link = "http://www.google.com"
            };

            // Act
            var attachmentId = await PostJson<Guid>("api/attachments/gdrive", payload);

            // Assert
            Fixture.Database(db =>
            {
                var actualAttachment = db.Attachments.Find(attachmentId);
                Assert.Equal(payload.name, actualAttachment.Name);
                Assert.Equal(AttachmentType.GoogleDrive, actualAttachment.Type);
                var actualGdriveAttachment = actualAttachment as GDriveAttachment;
                Assert.NotNull(actualGdriveAttachment);
                Assert.Equal(payload.link, actualGdriveAttachment.Link);
            });
        }

        [Fact()]
        public async Task GDriveAttachment_GetGdriveAttachment_ReturnGdriveAttachment()
        {
            // Arrange
            var attachment = new GDriveAttachment(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "http://www.google.com")
            {
                Name = "test"
            };

            Database(db =>
            {
                db.Attachments.Add(attachment);
                db.SaveChanges();
            });

            var url = $"api/attachments/{attachment.Id}/gdrive";
            // Act

            var actualAttachment = await GetJson<GDriveAttachmentDto>(url);

            // Assert
            Assert.Equal(attachment.Link, actualAttachment.Link);
            Assert.Equal(attachment.Name, actualAttachment.Name);
        }

        [Fact()]
        public async Task DirecteAttachment_GetGdriveAttachment_ReturnError()
        {
            // Arrange
            var bag = Encoding.UTF8.GetBytes("image");
            var attachment = new DirectAttachment(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                bag)
            {
                Name = "test"
            };
            Database(db =>
            {
                db.Attachments.Add(attachment);
                db.SaveChanges();
            });

            var url = $"api/attachments/{attachment.Id}/gdrive";
            // Act

            var act = GetJson<GDriveAttachmentDto>(url);

            // Assert
            await Assert.ThrowsAsync<NotNullException>(async () => await act);
        }

        [Fact()]
        public async Task DirectAttachment_removeAttachment_Success()
        {
            // Arrange
            var bag = Encoding.UTF8.GetBytes("image");
            var attachment = new DirectAttachment(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                bag)
            {
                Name = "test"
            };
            Database(db =>
            {
                db.Attachments.Add(attachment);
                db.SaveChanges();
            });

            var url = $"api/attachments/{attachment.Id}";
            // Act

            var act = await DeleteJson(url);

            // Assert
            Assert.True(act.IsSuccessStatusCode);

            AssertDatabaseHasNot<Attachment>(a =>
                a.Id == attachment.Id);
        }
    }
}
