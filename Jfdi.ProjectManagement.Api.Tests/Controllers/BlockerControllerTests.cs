﻿using System;
using System.Linq;
using Jfdi.ProjectManagement.Api.Dtos;
using Jfdi.ProjectManagement.Api.Models;
using Jfdi.ProjectManagement.Api.Services;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;
using Task = System.Threading.Tasks.Task;

namespace Jfdi.ProjectManagement.Api.Tests.Controllers
{
    public class BlockerControllerTests : TestCase
    {
        public BlockerControllerTests(TestApplicationFactory<Startup> fixture, ITestOutputHelper testOutput)
            : base(fixture, testOutput)
        {
        }

        [Fact]
        public async Task NewEvent_PostBlocker_BlockerIsCreated()
        {
            // Arrange
            var calendar = new Calendar(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var eventTask = new CalendarEvent(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "event",
                DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1))
            {
                Calendar = calendar
            };
            var user = new User(Guid.NewGuid(), "", DateTime.UtcNow, Models.Users.UserPermissions.Owner);
            var member = new TeamMember(Guid.NewGuid(), DateTime.UtcNow, user.Id, "first", "last");
            var client = new Client(Guid.NewGuid(), DateTime.UtcNow, Guid.NewGuid(), "client");
            client.TeamMemberAssignments.Add(member);
            var service = new ServiceInstance(Guid.NewGuid(), DateTime.UtcNow, Guid.NewGuid(), "service")
            {
                Client = client,
            };

            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task")
            {
                Event = eventTask,
                Service = service
            };
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = taskInstance
            };
            Database(db =>
            {
                db.TaskEvents.Add(taskEvent);
                db.SaveChanges();
            });

            var payload = new
            {
                title = "test",
                taskId = taskEvent.Id,
                startDate = DateTime.UtcNow,
                endDate = DateTime.UtcNow.AddDays(1)
            };
            var url = $"api/blockers";

            // Act
            var response = await PostJson<BlockerDto>(url, payload);

            // Assert
            Assert.Equal(taskEvent.Id, response.TaskId);
            Assert.Equal(payload.title, response.Title);
            Database(db =>
                {
                    var actualBlocker = db.Blockers
                    .Include(b => b.CalendarEvent)
                    .Include(b => b.Task)
                    .ThenInclude(ti => ti.State)
                    .First(b => b.Id == response.Id);

                    Assert.Equal(taskEvent.Id, actualBlocker.Task.Id);
                    Assert.Equal(payload.title, actualBlocker.Title);
                    Assert.Equal(ITaskStateService.BlockedStateId, actualBlocker.Task.State.Id);
                });
        }

        [Fact]
        public async Task NewBlocker_UpdateBlocker_BlockerUpdated()
        {
            // Arrange
            var blocker = new Blocker(
                "old_blocker",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var calendarEvent = new CalendarEvent(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "event",
                    DateTime.UtcNow,
                    DateTime.UtcNow.AddDays(1),
                    default);
            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = taskInstance
            };
            blocker.Task = taskEvent;
            blocker.CalendarEvent = calendarEvent;
            Database(db =>
            {
                db.Blockers.Add(blocker);
                db.TaskEvents.Add(taskEvent);
                db.CalendarEvents.Add(calendarEvent);
                db.SaveChanges();
            });

            var payload = new { title = "new_blocker" };
            var url = $"api/blockers/{blocker.Id}";

            // Act
            var response = await PutJson(url, payload);

            // Assert
            Assert.True(response.IsSuccessStatusCode);

            Database(db =>
            {
                var actualBlocker = db.Blockers
                    .Include(b => b.Task)
                    .Include(b => b.CalendarEvent)
                    .First(b => b.Id == blocker.Id);

                Assert.Equal(taskEvent.Id, actualBlocker.Task.Id);
                Assert.Equal(calendarEvent.Id, actualBlocker.CalendarEvent.Id);
                Assert.Equal(payload.title, actualBlocker.Title);
            });
        }

        [Fact]
        public async Task NewBlocker_ReadBlocker_ReturnBlocker()
        {
            // Arrange
            var blocker = new Blocker(
                "blocker",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var calendarEvent = new CalendarEvent(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "event",
                    DateTime.UtcNow,
                    DateTime.UtcNow.AddDays(1),
                    default);
            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = taskInstance
            };
            blocker.Task = taskEvent;
            blocker.CalendarEvent = calendarEvent;
            Database(db =>
            {
                db.Blockers.Add(blocker);
                db.TaskEvents.Add(taskEvent);
                db.CalendarEvents.Add(calendarEvent);
                db.SaveChanges();
            });

            var url = $"api/blockers/{blocker.Id}";

            // Act
            var response = await GetJson<BlockerDto>(url);

            // Assert

            Assert.Equal(taskEvent.Id, response.TaskId);
            Assert.Equal(calendarEvent.Id, response.CalendarEventId);
            Assert.Equal(blocker.Title, response.Title);
            Assert.Equal(blocker.Id, response.Id);
        }

        [Fact]
        public async Task NewBlocker_DeleteBlocker_BlockerDeleted()
        {
            // Arrange
            var blocker = new Blocker(
                "blocker",
                Guid.NewGuid(),
                DateTime.UtcNow,
                default);
            var calendarEvent = new CalendarEvent(
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    default,
                    "event",
                    DateTime.UtcNow,
                    DateTime.UtcNow.AddDays(1),
                    default);
            var taskInstance = new TaskInstance(
                Guid.NewGuid(),
                DateTime.UtcNow,
                default,
                "task");
            var taskEvent = new TaskEvent(
                Guid.NewGuid())
            {
                Instance = taskInstance
            };
            blocker.Task = taskEvent;
            blocker.CalendarEvent = calendarEvent;
            Database(db =>
            {
                taskEvent.State = db.TaskStates.Find(ITaskStateService.BlockedStateId);
                db.Blockers.Add(blocker);
                db.TaskEvents.Add(taskEvent);
                db.CalendarEvents.Add(calendarEvent);
                db.SaveChanges();
            });

            var url = $"api/blockers/{blocker.Id}/tasks/{taskEvent.Id}";

            // Act
            var response = await DeleteJson(url);

            // Assert

            Assert.True(response.IsSuccessStatusCode);

            AssertDatabaseHasNot<Blocker>(b =>
                b.Id == blocker.Id);

            AssertDatabaseHas<TaskInstance>(ti =>
                ti.Id == taskInstance.Id);

            AssertDatabaseHas<CalendarEvent>(ce =>
                ce.Id == calendarEvent.Id);

            Database(db =>
            {
                var actualTaskInstance = db.TaskEvents
                    .Include(ti => ti.State).Single(ti => ti.Id == taskEvent.Id);
                Assert.Equal(actualTaskInstance.State.Id, ITaskStateService.TodoStateId);
            });
        }
    }
}
