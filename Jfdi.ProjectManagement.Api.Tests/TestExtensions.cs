﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Auth;
using Jfdi.ProjectManagement.Api.Services;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Jfdi.ProjectManagement.Api.Tests.Mocks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Jfdi.ProjectManagement.Api.Tests
{
    public static class TestExtensions
    {
        public static void DatabaseClear<TStartup>(
              this TestApplicationFactory<TStartup> factory
          ) where TStartup : class
        {
            using var scope = factory.Services.CreateScope();
            var db = scope.ServiceProvider.GetService<JfdiDbContext>();
            db.Database.EnsureDeleted();
            db.Database.EnsureCreated();
        }

        public static void Database<TStartup>(
            this TestApplicationFactory<TStartup> factory,
            Action<JfdiDbContext> action
        ) where TStartup : class
        {
            using var scope = factory.Services.CreateScope();
            var db = scope.ServiceProvider.GetService<JfdiDbContext>();
            action(db);
        }

        public static void DatabaseHas<TStartup, TEntity>(
            this TestApplicationFactory<TStartup> factory,
            Func<TEntity, bool> query
        ) where TStartup : class where TEntity : class
        {
            using var scope = factory.Services.CreateScope();
            var db = scope.ServiceProvider.GetService<JfdiDbContext>();
            var res = db.Set<TEntity>().IgnoreQueryFilters().FirstOrDefault(query);
            Assert.NotNull(res);
        }

        public static void DatabaseHasNot<TStartup, TEntity>(
            this TestApplicationFactory<TStartup> factory,
            Func<TEntity, bool> query
        ) where TStartup : class where TEntity : class
        {
            using var scope = factory.Services.CreateScope();
            var db = scope.ServiceProvider.GetService<JfdiDbContext>();
            var res = db.Set<TEntity>().IgnoreQueryFilters().FirstOrDefault(query);
            Assert.Null(res);
        }

        public static Task<HttpResponseMessage> PostJson(
            this HttpClient client,
            string url,
            object payload)
        {
            var json = JsonSerializer.Serialize(payload);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            return client.PostAsync(url, content);
        }

        public static Task<HttpResponseMessage> PostForm(
            this HttpClient client,
            string url,
            Stream payload)
        {
            var content = new MultipartFormDataContent();
            content.Add(new StreamContent(payload), "data");
            return client.PostAsync(url, content);
        }

        public static Task<HttpResponseMessage> PutJson(
            this HttpClient client,
            string url,
            object payload)
        {
            var json = JsonSerializer.Serialize(payload);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            return client.PutAsync(url, content);
        }

        public static IServiceCollection AddTestAuth(this IServiceCollection services)
        {
            services
                .AddAuthentication("Test")
                .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>(
                    "Test",
                    options => { }
                );

            services.Replace<ICurrentUserProvider>(new TestCurrentUserProvider());

            return services;
        }

        public static IServiceCollection AddKeycloakMock(this IServiceCollection services)
        {
            services.Replace<IUserService>(new UserServiceMock());
            return services;
        }

        public static IServiceCollection Replace<TReplacement>(
            this IServiceCollection services,
            TReplacement replacement,
            ServiceLifetime lifetime = ServiceLifetime.Singleton
        ) where TReplacement : class
        {
            var descriptor = services.SingleOrDefault(x =>
                x.ServiceType == typeof(TReplacement)
            );
            if (descriptor != null)
            {
                services.Remove(descriptor);
            }

            descriptor = new ServiceDescriptor(
                typeof(TReplacement),
                (_) => replacement,
                lifetime
            );
            services.Add(descriptor);
            return services;
        }

        public static IServiceCollection Replace<TReplacement, TReplacementService>(
            this IServiceCollection services,
            TReplacement replacement,
            ServiceLifetime lifetime = ServiceLifetime.Singleton
        ) where TReplacement : class
        {
            var descriptor = services.SingleOrDefault(x =>
                x.ServiceType == typeof(TReplacement)
                && x.ImplementationType == typeof(TReplacementService)
            );
            if (descriptor != null)
            {
                services.Remove(descriptor);
            }

            descriptor = new ServiceDescriptor(
                typeof(TReplacement),
                (_) => replacement,
                lifetime
            );
            services.Add(descriptor);
            return services;
        }
    }
}
