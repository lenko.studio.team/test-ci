﻿using System;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Jfdi.ProjectManagement.Api.Tests.Fixture;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Jfdi.ProjectManagement.Api.Tests
{
    public abstract class TestCase : IClassFixture<TestApplicationFactory<Startup>>
    {
        protected TestCase(TestApplicationFactory<Startup> fixture, ITestOutputHelper testOutput)
        {
            Fixture = fixture;
            TestOutput = testOutput;
            Fixture.DatabaseClear();
        }

        public TestApplicationFactory<Startup> Fixture { get; }

        public ITestOutputHelper TestOutput { get; }

        protected void Database(Action<JfdiDbContext> action)
        {
            Fixture.Database(action);
        }

        protected async Task<TResult> GetJson<TResult>(string url) where TResult : class
        {
            var res = await Fixture.CreateClient().GetAsync(url);
            if(!res.IsSuccessStatusCode)
            {
                switch(res.StatusCode)
                {
                    case System.Net.HttpStatusCode.NotFound:
                        throw new NotNullException();
                }
            }

            var json = await res.Content.ReadAsStringAsync();
            try
            {
                var serializerOptions = new JsonSerializerOptions { PropertyNameCaseInsensitive = true, };
                serializerOptions.Converters.Add(new JsonStringEnumConverter());
                return JsonSerializer.Deserialize<TResult>(
                    json,
                    serializerOptions);
            }
            catch (JsonException)
            {
                TestOutput.WriteLine("Failed to parse response as JSON");
                TestOutput.WriteLine("Status: " + res.StatusCode);
                TestOutput.WriteLine("Output: " + json);
                return null;
            }
        }

        protected Task<HttpResponseMessage> DeleteJson(string url)
        {
            return Fixture.CreateClient().DeleteAsync(url);
        }

        protected Task<HttpResponseMessage> PostJson(string url, object payload)
        {
            return Fixture.CreateClient().PostJson(url, payload);
        }

        protected Task<HttpResponseMessage> PostForm(string url, Stream payload)
        {
            return Fixture.CreateClient().PostForm(url, payload);
        }

        protected async Task<TResult> PostJson<TResult>(string url, object payload)
        {
            var res = await Fixture.CreateClient().PostJson(url, payload);
            if (!res.IsSuccessStatusCode)
            {
                switch (res.StatusCode)
                {
                    case System.Net.HttpStatusCode.NotFound:
                        throw new NotNullException();
                }
            }

            var json = await res.Content.ReadAsStringAsync();
            try
            {
                var serializerOptions = new JsonSerializerOptions { PropertyNameCaseInsensitive = true, };
                serializerOptions.Converters.Add(new JsonStringEnumConverter());
                return JsonSerializer.Deserialize<TResult>(
                    json,
                    serializerOptions);
            }
            catch (JsonException)
            {
                TestOutput.WriteLine("Failed to parse response as JSON");
                TestOutput.WriteLine("Status: " + res.StatusCode);
                TestOutput.WriteLine("Output: " + json);
                return default;
            }
        }

        protected void AssertDatabaseHas<TEntity>(Func<TEntity, bool> assertion) where TEntity : class
        {
            Fixture.DatabaseHas(assertion);
        }

        protected Task<HttpResponseMessage> PutJson(string url, object payload)
        {
            return Fixture.CreateClient().PutJson(url, payload);
        }

        protected void AssertDatabaseHasNot<TEntity>(Func<TEntity, bool> assertion) where TEntity : class
        {
            Fixture.DatabaseHasNot(assertion);
        }
    }
}
